{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveFunctor #-}
module Data.Schema.SemanticError where

import Internal.Interlude hiding (id)

import qualified Data.Schema.Types.Schema as Semantic
import qualified Data.Schema.Types.AST as AST
import Data.Schema.Semantic.Select
import Text.Megaparsec.Pos
import Data.Schema.Types.AST
import qualified Data.Schema.RenderSQL as RenderSQL
import qualified Data.Text as T

data SemanticErrorContext = SemanticErrorContext SemanticError AST.StatementContext
    deriving (Eq)

displayError :: AST.StatementContext -> Text -> Text
displayError (AST.StatementContext _ raw offset sourcePos) err =
    err' <> "\n" <> errLine <> "\n" <> "C: " <> (toS . sourcePosPretty) sourcePos
    where
       err' = T.drop offset raw
       column = (unPos . sourceColumn) sourcePos - offset
       errLine = T.replicate column "^" <> "\n    " <> err
-- @TODO maybe use megaparse error handling, overkill for now
--    toS $ errorBundlePretty errBundle :: Text
--    where
--      errBundle = ParseErrorBundle errors errState
--      errors = pure (TrivialError offset Nothing mempty :: ParseError Text Void)
--      errState = PosState raw offset sourcePos (mkPos 4) (">")


instance Show SemanticErrorContext where
    show (SemanticErrorContext (TableAlreadyExists (CreateTable name _ _)) ctx) =
      toS $ displayError ctx ("Table already exists" <> (toS . show) name)
    show (SemanticErrorContext (TableDoesNotExists tname) ctx) =
      toS $ displayError ctx ("Table " <> (toS . RenderSQL.renderToSQL) tname <> " does not exist")
    show (SemanticErrorContext (ColumnAlreadyExists name _ _) ctx) =
      toS $ displayError ctx ("Column already exists" <> (toS . show) name)
    show (SemanticErrorContext (ColumnNameMissmatch _ _ _ _) ctx) =
      toS $ displayError ctx ("Column name missmatch")
    show (SemanticErrorContext (ViewColumnNamesDuplicate names) ctx) =
      toS $ displayError ctx ("Duplicate column names " <> (toS . show) names)
    show (SemanticErrorContext (TableAlternationError _consts fromTable fromColsFound fromCols) _ctx) =
      "Cannot find all columns in table" <> show fromTable <> ". Found " <> show fromColsFound <> " out of " <> show fromCols
    show (SemanticErrorContext err _) = show err

type Context = Text
type AlternationType = Text
data SemanticError =
    TableAlreadyExists AST.CreateTable |
    TableDoesNotExists AST.TableName |
    ColumnAlreadyExists Text Semantic.SqlType [AST.NamedConstraint AST.ColumnConstraint] |
    ColumnNameMissmatch Context Semantic.Table (NonEmpty Text) [Semantic.Column] |
    TableAlternationError AST.TableConstraint Semantic.Table [Semantic.Column] (NonEmpty Text) |
    UnknownAlteration Semantic.TableId AST.Alteration |
    TypeCheckError TypeCheckError |
    ViewColumnNamesAmbiguous [(Maybe Text, InferredType)] |
    ViewColumnNamesDuplicate [Text] |
    ViewColumnIsUnknownRowType [(Maybe Text, InferredType)]
    deriving (Show, Eq)
