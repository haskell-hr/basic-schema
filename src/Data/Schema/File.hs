{-# LANGUAGE ScopedTypeVariables #-}
module Data.Schema.File where

import Internal.Interlude hiding (id)
import Data.Schema.Parser.Statement
import Data.Schema.Parser.Utils
import qualified Data.Schema.Types.AST as AST

import qualified Data.Text.IO as T
import Text.Megaparsec.Error
import qualified Data.Schema.Types.Schema as Semantic
import qualified Data.Schema.Semantic as Semantic
import Text.Megaparsec
import Data.Schema.SemanticError
import Data.Functor.Identity

file :: Parser [AST.StatementContext]
file = do
    raw <- getInput
    whitespace *> statement raw `endBy` semicolon

data SemanticParseError = ParseError Text | SemanticError SemanticErrorContext
    deriving (Eq, Show)


parseStatements :: Text -> IO (Either SemanticParseError [AST.StatementContext])
parseStatements input =
     case runParser file "" input of
        Left a -> return $ Left $ ParseError (toS $ errorBundlePretty a)
        Right statements -> do
            let errorsShown = Semantic.semanticCompile statements
                    & handleException @SemanticErrorContext throwSignal
            return $ case errorsShown & runExceptT & runIdentity of
                Left err -> Left $ SemanticError err
                Right _ -> return statements

semanticParse :: Text -> IO (Either SemanticParseError ([AST.StatementContext], Semantic.Schema))
semanticParse input =
    case runParser file "" input of
        Left a -> return $ Left $ ParseError (toS $ errorBundlePretty a)
        Right statements -> do
            let errorsShown = Semantic.semanticCompile statements
                    & handleException @SemanticErrorContext throwSignal
            return $ case errorsShown & runExceptT & runIdentity of
                Left err -> Left $ SemanticError err
                Right s -> return (statements, s)

semanticParseFile :: FilePath -> IO (Either SemanticParseError ([AST.StatementContext], Semantic.Schema))
semanticParseFile filename = do
    sql <- T.readFile filename
    semanticParse sql


semanticParseFilePrintError :: FilePath -> IO ([AST.StatementContext], Semantic.Schema)
semanticParseFilePrintError filename = do
    res <- semanticParseFile filename
    case res of
        Left (ParseError err) -> putStrLn (toS err) >> error "failed parse"
        Left (SemanticError err) -> putStrLn ((toS . show) err) >> error "failed compile"
        Right a -> return a
