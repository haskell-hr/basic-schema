{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedLists   #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.Schema.RenderSQL
    ( module Data.Schema.RenderSQL
    , module Data.Schema.Types.AST
    ) where

import           Data.Schema.Types.AST
import           qualified Data.Text             as T
import           Internal.Interlude
import           qualified Data.List.NonEmpty as NE
import Data.String (IsString(..))


{-
We need an statically typed way to generate SQL code. Basic-schema provides AST for a part of the needed SQL code.
This file creates the RenderSQL interface and the corresponding SQLQuery datatype. SQLQuery uses basic-schema for stuff that's already there and fills in the corresponding blanks.
The idea is to implement all of it in basic-schema, eventually.

-}

data Token =
    Unspaced { tokenText :: Text }
    | Spaced { tokenText :: Text }
    | UnspacedLeft { tokenText :: Text }
    | UnspacedRight { tokenText :: Text }

newtype Tokens = Tokens [Token]
    deriving (Semigroup, Monoid)

instance IsString Tokens where
    fromString str
        | elem @[] str ["(", "[", "{"] = Tokens [UnspacedRight (T.pack str)]
        | elem @[] str [")", "]", "}", ",", ";", ":"] = Tokens [UnspacedLeft (T.pack str)]
        | elem @[] str ["."] = Tokens [Unspaced (T.pack str)]
        | str == "" = mempty
        | otherwise = Tokens [Spaced (T.pack str)]

tokensToText :: Tokens -> Text
tokensToText (Tokens []) = ""
tokensToText (Tokens [x]) = tokenText x
tokensToText (Tokens (Unspaced x : xs)) = x <> tokensToText (Tokens xs)
tokensToText (Tokens (UnspacedRight x : xs)) = x <> tokensToText (Tokens xs)
tokensToText (Tokens (x : Unspaced y : xs)) =
    tokenText x <> tokensToText (Tokens (Unspaced y : xs))
tokensToText (Tokens (x : UnspacedLeft y : xs)) =
    tokenText x <> tokensToText (Tokens (UnspacedLeft y : xs))
tokensToText (Tokens (x : xs)) = tokenText x <> " " <> tokensToText (Tokens xs)

mintercalate :: (Foldable f, Monoid m) => m -> f m -> m
mintercalate sep f
    | null f = mempty
    | otherwise = foldl1 (\a b -> a <> sep <> b) f

spaced :: Text -> Tokens
spaced x = Tokens [Spaced x]

class RenderSQL a where
    renderToTokens :: a -> Tokens

renderToSQL :: RenderSQL a => a -> Text
renderToSQL = tokensToText . renderToTokens

optionalAlias :: Maybe Text -> Tokens
optionalAlias = maybe "" (\s -> "as" <> spaced s)

integerLiteral :: Integral a => a -> Tokens
integerLiteral = fromString . show @Integer . fromIntegral
------------------------------------


instance RenderSQL SqlType where
    renderToTokens SqlInt = "int"
    renderToTokens SmallSerial = "smallserial"
    renderToTokens Serial = "serial"
    renderToTokens BigSerial = "bigserial"
    renderToTokens SmallInt = "smallint"
    renderToTokens BigInt = "bigint"
    renderToTokens SqlInteger = "integer"
    renderToTokens Int4 = "int4"
    renderToTokens DoublePrecision = "double"
    renderToTokens SqlDouble = "double"
    renderToTokens Real = "real"
    renderToTokens Numeric = "numeric"
    renderToTokens (NumericPS a b) = "numeric(" <> integerLiteral a <> "," <> integerLiteral b <> ")"
    renderToTokens (NumericP a) = "numeric(" <> integerLiteral a  <> ")"
    renderToTokens CharacterVarying = "character"
    renderToTokens SqlText = "text"
    renderToTokens Timestamp = "timestamp"
    renderToTokens TimestampWithTimeZone = "timestamptz"
    renderToTokens Time = "time"
    renderToTokens TimeWithTimeZone = "time"
    renderToTokens Interval = "interval"
    renderToTokens Date = "date"
    renderToTokens Boolean = "boolean"
    renderToTokens Point = "point"
    renderToTokens Line = "line"
    renderToTokens LSeg = "lseg"
    renderToTokens Box = "box"
    renderToTokens Path = "path"
    renderToTokens Polygon = "polygon"
    renderToTokens Circle = "circl"
    renderToTokens Cidr = "cidr"
    renderToTokens Inet = "inet"
    renderToTokens MacAddr = "macaddr"
    renderToTokens MacAddr8 = "macaddr8"
    renderToTokens JSON = "json"
    renderToTokens JSONB = "jsonb"
    renderToTokens ByteA = "bytea"
    renderToTokens (CharacterVaryingN a) = "charactervarying(" <> integerLiteral a <> ")"
    renderToTokens UUID = "uuid"
    renderToTokens (Array _ _) = "array" -- how to parse this?!
    renderToTokens (Other a) = spaced a
    renderToTokens Unknown = "unknown"
    renderToTokens Record = "record"


-------------------------------------

instance RenderSQL a => RenderSQL (NamedConstraint a) where
    renderToTokens (NamedConstraint _ c) = renderToTokens c

instance RenderSQL a => RenderSQL (Maybe a) where
    renderToTokens Nothing = mempty
    renderToTokens (Just v) = renderToTokens v

instance RenderSQL TableField where
    renderToTokens (TableField mt t) = case mt of
        (Just a) -> spaced a <> "." <> spaced t
        Nothing  -> spaced t

instance RenderSQL FunctionName where
    renderToTokens (FunctionName sch fn) =
        foldMap (\s -> spaced ("\"" <> s <> "\"") <> ".") sch <> spaced ("\"" <> fn <> "\"")

instance RenderSQL AllOrDistinct where
    renderToTokens Data.Schema.Types.AST.All = "ALL"
    renderToTokens Distinct = "DISTINCT"

instance RenderSQL Mapping where
    renderToTokens Everything = "*"
    renderToTokens (Expressions exprs) =
        mintercalate "," (renderToTokens <$> exprs)


instance RenderSQL (ValueExpression, Maybe Text) where
    renderToTokens (expr, mAlias) =
        renderToTokens expr <> optionalAlias mAlias

instance RenderSQL NonJoinFromItem where
    renderToTokens (FromTable tableName mAlias) = spaced tableName <> optionalAlias mAlias
    renderToTokens (Subselect select mAlias) = renderToTokens select <> "as" <> spaced mAlias

instance RenderSQL JoinType where
    renderToTokens InnerJoin = "INNER JOIN"
    renderToTokens LeftJoin = "LEFT JOIN"
    renderToTokens RightJoin = "RIGHT JOIN"
    renderToTokens FullJoin = "FULL JOIN"
    renderToTokens CrossJoin = "CROSS JOIN"

instance RenderSQL JoinCondition where
    renderToTokens (JoinCondition expr) = renderToTokens expr

instance RenderSQL FromItem where
    renderToTokens (NonJoinFromItem item) = renderToTokens item
    renderToTokens (Join fromItem joinType toItem condition) =
        renderToTokens fromItem <>
        renderToTokens joinType <>
        renderToTokens toItem <>
        renderToTokens condition

instance RenderSQL Source where
    renderToTokens (Source fromItems) = mintercalate "," (renderToTokens <$> elems)
        where elems = toList fromItems

instance RenderSQL Where where
    renderToTokens (Where expr) = renderToTokens expr

instance RenderSQL Grouping where
    renderToTokens (GroupExpression expr) = renderToTokens expr
    renderToTokens (GroupList exprs) = mintercalate "," (renderToTokens <$> toList exprs)

instance RenderSQL GroupBy where
    renderToTokens (GroupBy grouping) = mintercalate "," (renderToTokens <$> toList grouping)

instance RenderSQL Having where
    renderToTokens (Having expr) = renderToTokens expr

instance RenderSQL Direction where
    renderToTokens Ascending = "ASCENDING"
    renderToTokens Descending = "DESCENDING"

instance RenderSQL (ValueExpression, Maybe Direction) where
    renderToTokens (expr, direction) = renderToTokens expr <> renderToTokens direction

instance RenderSQL OrderBy where
    renderToTokens (OrderBy exprs) = mintercalate "," (renderToTokens <$> toList exprs)

instance RenderSQL Limit where
    renderToTokens (Limit n) = integerLiteral n

instance RenderSQL Offset where
    renderToTokens (Offset n) = integerLiteral n

prefix :: Tokens -> Tokens -> Tokens
prefix = (<>)

suffix :: Tokens -> Tokens -> Tokens
suffix suf t = t <> suf

instance RenderSQL Select where
    renderToTokens statement =
        "SELECT"
        <> renderToTokens (allOrDistinct statement)
        <> renderToTokens (mapping statement)
        <> foldMap (prefix "FROM" . renderToTokens) (source statement)
        <> foldMap (prefix "WHERE" . renderToTokens)
            (Data.Schema.Types.AST.filter statement)
        <> foldMap (prefix "GROUP BY" . renderToTokens) (grouping statement)
        <> foldMap (prefix "HAVING" . renderToTokens) (having statement)
        <> foldMap (prefix "ORDER BY" . renderToTokens) (orderBy statement)
        <> foldMap (prefix "LIMIT" . renderToTokens) (limit statement)
        <> foldMap (prefix "OFFSET" . renderToTokens)
            ((offset :: Select -> Maybe Offset) statement) <> ";"

instance RenderSQL CaseAlternative where
    renderToTokens (CaseAlternative cond expr) =
        "WHEN" <> renderToTokens cond <> "THEN" <> renderToTokens expr

instance RenderSQL ElseAlternative where
    renderToTokens (ElseAlternative expr) = "ELSE" <> renderToTokens expr

instance RenderSQL ValueExpression where
    renderToTokens (NumberLiteral a) = fromString $ show a
    renderToTokens (StringLiteral a) = spaced ("'" <> a <> "'")
    renderToTokens (Operator t v1 v2) =
        "(" <> renderToTokens v1 <> spaced t <> renderToTokens v2 <> ")"
    renderToTokens (TypeCast v t) =
        "CAST" <> "(" <> renderToTokens v <> "AS" <> renderToTokens t <> ")"
    renderToTokens (TableFieldValue tf) = renderToTokens tf
    renderToTokens (Application t vs) =
        renderToTokens t <> "(" <> mintercalate "," (map renderToTokens vs) <> ")"
    renderToTokens (UnaryOperator uop val) = spaced uop <> renderToTokens val
    renderToTokens (NullCheck isNullCheck val)
        | isNullCheck = "(" <> renderToTokens val <> "IS" <> "NULL" <> ")"
        | otherwise = "(" <> renderToTokens val <> "IS" <> "NOT" <> "NULL" <> ")"
    renderToTokens (AllTableFields tab) = spaced tab <> "." <> "*"
    renderToTokens NullExpr = "NULL"
    renderToTokens (SingleRowSelect s) = renderToTokens s
    renderToTokens (CaseExpression caseAlts elseAlt) = "CASE"
        <> foldMap (renderToTokens) caseAlts
        <> foldMap (renderToTokens) elseAlt
        <> "END"


instance RenderSQL ColumnConstraint where
    renderToTokens NotNull = "NOT NULL"
    renderToTokens PrimaryKey = "PRIMARY KEY"
    renderToTokens Unique = "UNIQUE"
    renderToTokens (Default x) = "DEFAULT" <> renderToTokens x
    renderToTokens (References (TableName _ tn') mt) =
        "REFERENCES" <> spaced tn' <> "(" <> foldMap spaced mt <> ")"
    renderToTokens Null = "NULL"

instance RenderSQL TableConstraint where
    renderToTokens (TablePrimaryKey xs) = "PRIMARY KEY" <> "(" <> mintercalate "," (fmap spaced xs) <> ")"
    renderToTokens (TableUnique xs) = "UNIQUE" <> "(" <> mintercalate "," (fmap spaced xs) <> ")"
    renderToTokens (TableForeignKey fks t cs) = mintercalate ", \n" xs
        where
        xs = zipWith3
            fkText
            (toList (fmap spaced fks))
            (replicate (length fks) (renderToTokens t))
            (toList (fmap spaced cs))

instance RenderSQL CreateTable where
    renderToTokens (CreateTable name columns constraints) =
        "CREATE" <> "TABLE" <> renderToTokens name <> "(" <>
        mintercalate "," (renderToTokens <$> columns) <> separator <>
        mintercalate "," (renderToTokens <$> constraints) <> ")" <> ";"
        where separator = if length constraints > 0 then "," else ""

instance RenderSQL Sequence where
    renderToTokens (Sequence tableName) = "CREATE" <> "SEQUENCE" <> renderToTokens tableName <> ";"

instance RenderSQL CreateType where
    renderToTokens (CreateType typeName customType) = "CREATE TYPE" <> renderToTokens typeName <> renderToTokens customType <> ";"

instance RenderSQL CustomType where
    renderToTokens (Composite types) = "AS" <> "(" <> combined <> ")"
        where
        sqlTextTypes = (\(name, sqlType) -> spaced name <> renderToTokens sqlType) <$> types
        combined = mintercalate "," sqlTextTypes
    renderToTokens (Enumerated types) =
        "AS" <> "ENUM" <> "(" <> mintercalate "," (fmap spaced types) <> ")"
    renderToTokens (Scalar) = ""
    renderToTokens (Range (start, stop)) =
        "AS" <> "RANGE" <> "(" <> spaced start <> "," <> spaced stop <> ")"

instance RenderSQL CreateView where
    renderToTokens (CreateView tableName columnNames query) =
        "CREATE" <> "VIEW" <> renderToTokens tableName <> spaced columns <> "AS"<> renderToTokens query <> ";"
        where columns = case columnNames of
                Nothing -> ""
                Just cols -> "(" <> T.intercalate "," (NE.toList cols) <> ")"

instance RenderSQL AlterTable where
    renderToTokens (AlterTable tableName alteration) =
        "ALTER" <> "TABLE" <> renderToTokens tableName <> renderToTokens alteration <> ";"

instance RenderSQL ColumnAlteration where
    renderToTokens (SetDefault val) = "SET" <> "DEFAULT" <> renderToTokens val

instance RenderSQL RLSAction where
    renderToTokens EnableRLS = "ENABLE ROW LEVEL SECURITY"
    renderToTokens DisableRLS = "DISABLE ROW LEVEL SECURITY"
    renderToTokens ForceRLS = "FORCE ROW LEVEL SECURITY"
    renderToTokens NoForceRLS = "NO FORCE ROW LEVEL SECURITY"

instance RenderSQL Alteration where
    renderToTokens (AddConstraint c) = "ADD" <> "CONSTRAINT" <> renderToTokens c
    renderToTokens (AlterOwner o) = "OWNER" <> "TO" <> spaced o
    renderToTokens (AlterColumn name alteration) =
        "ALTER" <> "COLUMN" <> spaced name <> renderToTokens alteration
    renderToTokens (RowLevelSecurity action) = renderToTokens action
    renderToTokens (UnknownAlteration a) = spaced a

instance RenderSQL Statement where
    renderToTokens (SelectStatement statement) = renderToTokens statement
    renderToTokens (CreateTableStatement statement) = renderToTokens statement
    renderToTokens (UnknownStatement statement) = spaced statement
    renderToTokens (SetStatement statement) = spaced statement
    renderToTokens (CreateSequence statement) = renderToTokens statement
    renderToTokens (CreateTypeStatement statement) = renderToTokens statement
    renderToTokens (CreateViewStatement statement) = renderToTokens statement
    renderToTokens (AlterTableStatement statement) = renderToTokens statement

fkText :: Tokens -> Tokens -> Tokens -> Tokens
fkText fk t c =
    "FOREIGN" <> "KEY" <> "(" <> fk <> ")" <> "REFERENCES" <> t <> "(" <> c <> ")"

-------------------------------------

instance RenderSQL CreateTableColumn where
    renderToTokens (CreateTableColumn n t c) = spaced n <> renderToTokens t <> c'
        where c' = foldMap renderToTokens c

instance RenderSQL TableName where
    renderToTokens (TableName sch tn) =
        foldMap (\s -> spaced ("\"" <> s <> "\"") <> ".") sch <> spaced ("\"" <> tn <> "\"")

instance RenderSQL SQLQuery where
    renderToTokens (Basic statement) = renderToTokens statement
    renderToTokens (AltTable tn act rest) = alterTableOther (renderToTokens tn) (renderToTokens act) rest
    renderToTokens (InsertInto tn xs) = insertIntoTable (renderToTokens tn) xs
    renderToTokens (Update tn xs t) = updateTable (renderToTokens tn) (mintercalate "," (fmap (\(a, b) -> spaced a <> "=" <> renderToTokens b) xs)) (fmap spaced t)
    renderToTokens (DropTable tn rest) = dropTable tn (spaced rest)
    renderToTokens (RawQuery txt) = spaced txt

createTable :: Text -> Text -> [Text] -> Text
createTable tn constraints cols = "CREATE TABLE" <> tn <> "(" <> "\n"
  <> constraints <> ",\n"
  <> T.intercalate ",\n" cols <>
  "\n);\n"

alterTableOther :: Tokens -> Tokens -> Text -> Tokens
alterTableOther tName act rest = "ALTER" <> "TABLE" <> tName <> act <> spaced rest <> ";\n"

insertIntoTable :: Tokens -> NonEmpty (Text, ValueExpression) -> Tokens
insertIntoTable tn xs =
    "INSERT" <> "INTO" <> tn <> "(" <> mintercalate "," (fmap spaced columnNames) <> ")" <> "VALUES" <> "(" <> mintercalate "," (fmap renderToTokens values) <> ");\n"
    where (columnNames, values) = unzip (toList xs)

updateTable :: Tokens -> Tokens -> Maybe Tokens -> Tokens
updateTable tn xs (Just t) = "UPDATE" <> tn <> "\n" <> "SET" <> xs <> "\nWHERE" <> t <> ";\n"
updateTable tn xs Nothing = "UPDATE" <> tn <> "\n" <> "SET" <> xs <> ";\n"

dropTable :: TableName -> Tokens -> Tokens
dropTable tn rest = "DROP" <> "TABLE" <> renderToTokens tn <> rest

data Action
    = AddColumn
    | DropConstraint
    | AddPrimaryKey
    | AltColumn
    | DropColumn
    deriving (Eq, Show)

instance RenderSQL Action where
    renderToTokens AddColumn      = "ADD COLUMN"
    renderToTokens DropConstraint = "DROP CONSTRAINT"
    renderToTokens AddPrimaryKey  = "ADD PRIMARY KEY"
    renderToTokens AltColumn      = "ALTER COLUMN"
    renderToTokens DropColumn     = "DROP COLUMN"

data SQLQuery
    = Basic Statement
    | AltTable TableName Action Text -- name clash with AST.AlterColumn
    | InsertInto TableName (NonEmpty (Text, ValueExpression))
    | Update TableName (NonEmpty (Text, ValueExpression)) (Maybe Text)
    | DropTable TableName Text
    | RawQuery Text
    deriving (Eq, Show)


createTableQuery :: Text -> [(Text, SqlType, [ColumnConstraint])] -> [TableConstraint] -> SQLQuery
createTableQuery tName xs constr = Basic (CreateTableStatement t)
    where t = CreateTable
            { _tableName = TableName Nothing tName
            , _columns = map (\(n, s, cs) -> CreateTableColumn n s (w cs)) xs
            , _tableConstraints = w constr }
          w = map (NamedConstraint Nothing)

alterTableQuery :: TableName -> Action -> Text -> SQLQuery
alterTableQuery = AltTable

insertIntoQuery ::  TableName -> NonEmpty (Text, ValueExpression) -> SQLQuery
insertIntoQuery = InsertInto

updateTableQuery :: TableName -> NonEmpty (Text, ValueExpression) -> Maybe Text -> SQLQuery
updateTableQuery = Update

dropTableQuery :: TableName -> Text -> SQLQuery
dropTableQuery = DropTable

rawQuery ::Text -> SQLQuery
rawQuery = RawQuery

---------------------------------------------------------
-- Testing
---------------------------------------------------------


query0, query1, query2, query3, query4, query5, query6, query7 :: SQLQuery
query0 = createTableQuery "transport"
                          [("name", SqlText, [NotNull]), ("id", Serial, [])]
                          [TablePrimaryKey ("id" :| [])]


query1 = alterTableQuery "client_trip" DropConstraint "client_trip_pkey"

query2 = alterTableQuery "client_trip" AddColumn "id serial"

query3 = alterTableQuery "offer_pdf" AltColumn "edit_number SET NOT NULL"

query4 = updateTableQuery "offer" [("last_update", TableFieldValue (TableField Nothing "created_date"))] (Just "last_update IS NULL")

query5 = insertIntoQuery "applied_migrations" [("code", StringLiteral "H"), ("name", StringLiteral "rooms connect")]

query6 = updateTableQuery "accomodation" [("code", StringLiteral "Vlastiti"), ("name", StringLiteral "Vlastiti smještaj"), ("address", StringLiteral "/"), ("note", StringLiteral "/")] Nothing

query7 = alterTableQuery "consent" AltColumn "valid_to SET not null"

printAll :: IO ()
printAll = mapM_ (putStrLn . toS . renderToSQL) ([query0, query1, query2, query3, query4, query5, query6, query7] :: [SQLQuery])
