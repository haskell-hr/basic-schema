module Data.Schema.Query where

import Internal.Interlude hiding (id)
import Control.Lens hiding (Context(..))

import qualified Data.Schema.Types.Schema as Semantic
import qualified Data.Schema.Types.AST as AST
import qualified Data.Schema.Types.Lens as L
import Data.List (find)


findTable :: Semantic.Schema -> Semantic.TableId -> Maybe Semantic.Table
findTable dbModel tableId =
  find (\t -> t ^. L.id == tableId) (dbModel ^. L.tables)

findColumn :: Semantic.Schema -> Semantic.ColumnId -> Maybe Semantic.Column
findColumn dbModel columnId =
  find (\t -> t ^. L.id == columnId) (dbModel ^. L.columns)


getTable :: Semantic.Schema -> Text -> Text -> Maybe Semantic.Table
getTable databaseModel schema' tableName =
  let tableId = Semantic.TableId schema' tableName
  in findTable databaseModel tableId

getTableById :: Semantic.Schema -> Semantic.TableId -> Maybe Semantic.Table
getTableById = findTable

hasPrimaryKey :: Semantic.Schema -> Semantic.Table -> Bool
hasPrimaryKey dbModel table' = not (null (getPrimaryKey dbModel table'))

getPrimaryKey :: Semantic.Schema -> Semantic.Table -> [Semantic.Column]
getPrimaryKey schema' table' =
  let columns' = getTableColumns schema' table'
  in filter (\c -> or $ isPrimaryKey <$> getColumnConstraints table' c) columns'

getForeignKeys :: Semantic.Schema -> Semantic.Table -> [Semantic.Column]
getForeignKeys schema' table' =
  let columns' = getTableColumns schema' table'
  in filter (\c -> or $ isForeignKey <$> getColumnConstraints table' c) columns'

hasColumn :: Semantic.Schema -> Semantic.Table -> Text -> Bool
hasColumn dbModel table' columnName = isJust $ getColumn dbModel table' columnName

getColumn :: Semantic.Schema -> Semantic.Table -> Text -> Maybe Semantic.Column
getColumn databaseModel table' columnName =
  let columnId = Semantic.getColumnId table' columnName
  in findColumn databaseModel columnId

getColumnType :: Semantic.Schema -> Semantic.Table -> Text -> Maybe Semantic.SqlType
getColumnType dbModel table' columnName = do
  c <- getColumn dbModel table' columnName
  return (c ^. L.sqlType)

getTableColumns :: Semantic.Schema -> Semantic.Table -> [Semantic.Column]
getTableColumns databaseModel table' =
  getColumnsById' databaseModel (table' ^. L.columns)

getColumnById :: Semantic.Schema -> Semantic.ColumnId -> Maybe Semantic.Column
getColumnById = findColumn

getColumnsById' :: Semantic.Schema -> [Semantic.ColumnId] -> [Semantic.Column]
getColumnsById' databaseModel colIds = catMaybes $ findColumn databaseModel <$> colIds

getColumnConstraints :: Semantic.Table -> Semantic.Column -> [AST.NamedConstraint Semantic.TableConstraint]
getColumnConstraints table' column =
  filter (isColumnConstraint column) (table' ^. L.constraints)

isColumnConstraint :: Semantic.Column -> AST.NamedConstraint Semantic.TableConstraint -> Bool
isColumnConstraint col (AST.NamedConstraint _ (Semantic.NotNull cid)) =
  col ^. L.id == cid
isColumnConstraint col (AST.NamedConstraint _ (Semantic.Null cid)) =
  col ^. L.id == cid
isColumnConstraint col (AST.NamedConstraint _ (Semantic.Default cid _)) =
  col ^. L.id == cid
isColumnConstraint col (AST.NamedConstraint _ (Semantic.ForeignKey cids _ _)) =
  (col ^. L.id) `elem` cids
isColumnConstraint col (AST.NamedConstraint _ (Semantic.Unique cids)) =
  (col ^. L.id) `elem` cids
isColumnConstraint col (AST.NamedConstraint _ (Semantic.PrimaryKey cids)) =
  (col ^. L.id) `elem` cids
isColumnConstraint col (AST.NamedConstraint _ (Semantic.Check cid _)) =
  col ^. L.id == cid

isNotNull :: AST.NamedConstraint Semantic.TableConstraint -> Bool
isNotNull (AST.NamedConstraint _ (Semantic.NotNull _)) = True
isNotNull _ = False

isNull :: AST.NamedConstraint Semantic.TableConstraint -> Bool
isNull (AST.NamedConstraint _ (Semantic.Null _)) = True
isNull _ = False

hasDefaultValue :: AST.NamedConstraint Semantic.TableConstraint -> Bool
hasDefaultValue (AST.NamedConstraint _ (Semantic.Default _ _)) = True
hasDefaultValue _ = False

isUnique :: AST.NamedConstraint Semantic.TableConstraint -> Bool
isUnique (AST.NamedConstraint _ (Semantic.Unique _)) = True
isUnique _ = False

isForeignKey :: AST.NamedConstraint Semantic.TableConstraint -> Bool
isForeignKey (AST.NamedConstraint _ Semantic.ForeignKey{}) = True
isForeignKey _ = False

isPrimaryKey :: AST.NamedConstraint Semantic.TableConstraint -> Bool
isPrimaryKey (AST.NamedConstraint _ (Semantic.PrimaryKey _)) = True
isPrimaryKey _ = False


requiresMaybeWrapper :: Semantic.Table -> Semantic.Column -> Bool
requiresMaybeWrapper table' column =
  any isNull consts
  where consts = getColumnConstraints table' column

isColumnOptional :: Semantic.Table -> Semantic.Column -> Bool
isColumnOptional table' column' =
  any (\const' -> or $ [isNull, hasDefaultValue] <*> [const']) consts
  where consts = getColumnConstraints table' column'

isColumnRequired :: Semantic.Table -> Semantic.Column -> Bool
isColumnRequired table' column' =
  not (isColumnOptional table' column')

isCompositeType :: AST.CustomType -> Bool
isCompositeType (AST.Composite _) = True
isCompositeType _ = False

isEnumeratedType :: AST.CustomType -> Bool
isEnumeratedType (AST.Enumerated _) = True
isEnumeratedType _ = False

isRangeType :: AST.CustomType -> Bool
isRangeType (AST.Range _) = True
isRangeType _ = False

isScalar :: AST.CustomType -> Bool
isScalar AST.Scalar = True
isScalar _ = False

getEnumType :: Semantic.Schema -> Semantic.TableId -> Maybe AST.CustomType
getEnumType databaseModel typeName =
    snd <$> find (\(typeSchema', type') ->
      case type' of
          AST.Enumerated _ -> typeName == typeSchema'
          _ -> False) (databaseModel ^. L.types)
