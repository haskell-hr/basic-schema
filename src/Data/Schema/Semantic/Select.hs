{-# LANGUAGE RecordWildCards, FlexibleContexts, TypeApplications, DataKinds, TemplateHaskell #-}
{-# LANGUAGE TupleSections, OverloadedStrings, DuplicateRecordFields, FunctionalDependencies #-}
{-# LANGUAGE DerivingStrategies, GeneralizedNewtypeDeriving #-}
module Data.Schema.Semantic.Select where

import Internal.Interlude as Interlude
import Data.Schema.Types.Lens as L
import Data.Schema.Types.AST hiding (SqlType(..))
import Data.Schema.Types.Schema
import Control.Lens hiding (mapping)
import Data.Schema.Query
import qualified Data.Text as Text
import qualified Data.List.NonEmpty as NE
import qualified Data.Map as Map
import Text.Show.Pretty (ppShow)

data ArityMismatch = ArityMismatch
    { arityMismatchFunctionName :: Text
    , arityMismatchExpectedArguments :: Int
    , arityMismatchActualArguments ::  Int }
    deriving (Eq, Ord, Read, Show)

data BadArgumentType = BadArgumentType
    { badArgumentTypeFunctionName :: Text
    , badArgumentTypeArgumentIndex :: Int
    , badArgumentTypeInferredType :: InferredType
    , badArgumentTypeErrorMessage :: Text }
    deriving (Eq, Ord, Read, Show)

data BadArgumentTypeMultiple = BadArgumentTypeMultiple
    { badArgumentTypeMultipleFunctionName :: Text
    , badArgumentTypeMultipleInferredTypes :: [InferredType] }
    deriving (Eq, Ord, Read, Show)

data TypeCheckError =
    UnknownTable Text
    | InvalidTypeCast InferredType SqlType
    | UnknownField TableField
    | UnknownOperator Text
    | ArityMismatchError ArityMismatch
    | BadArgumentTypeError BadArgumentType
    | BadArgumentTypeMultipleError BadArgumentTypeMultiple
    | UnknownFunction Text
    | SubselectMustReturnSingleColumn [(Maybe Text, InferredType)]
    | CaseBranchesIncompatible (NonEmpty (Maybe Text, InferredType))
    deriving (Eq, Ord, Read)

data SimpleOrComplex
    = SimpleType SqlType
    | RowType [(Maybe Text, InferredType)]
    | DotStar SqlType [(Maybe Text, InferredType)]
    deriving (Eq, Ord, Read, Show)

data InferredType = InferredType
    { inferredTypeSimpleOrComplex :: SimpleOrComplex
    , inferredTypeIsOptional :: Bool }
    deriving (Eq, Ord, Read, Show)

data TypeCheckContext = TypeCheckContext
    { typeCheckContextList :: [([Text], Maybe Text, InferredType)]
    , typeCheckContextAliases :: [(Text, SqlType)] }
    deriving stock Show

instance Semigroup TypeCheckContext where
    (TypeCheckContext a b) <> (TypeCheckContext c d) = TypeCheckContext (a <> c) (b <> d)

instance Monoid TypeCheckContext where
    mempty = TypeCheckContext [] []
    mappend = (<>)

makeFields ''InferredType
makeFields ''ArityMismatch
makeFields ''BadArgumentType
makeFields ''BadArgumentTypeMultiple
makeFields ''TypeCheckContext
makePrisms ''TypeCheckError

instance Show TypeCheckError where
    show (UnknownTable t) = "Table '" <> toS t <> "' is unknown"
    show (InvalidTypeCast it st) = "Type " <> show it <> " can't be cast to " <> show st
    show (UnknownField tf) = "Table field '" <> show tf <> "' is unknown"
    show (UnknownOperator t) = "Operator " <> toS t <> " is unknown"
    show (ArityMismatchError am) = "The function " <> toS (am ^. functionName) <> " takes "
        <> show (am ^. expectedArguments) <> " arguments, but it was given "
        <> show (am ^. actualArguments)
    show (BadArgumentTypeError ba) = "The function " <> toS (ba ^. functionName) <> " was given an \
        \argument of a bad type. The argument number " <> show (ba ^. argumentIndex + 1) <> " has \
        \the inferred type " <> ppShow (ba ^. inferredType) <> " but the function "
        <> toS (ba ^. errorMessage)
    show (BadArgumentTypeMultipleError ba) = "The function " <> toS (ba ^. functionName) <> " does \
        \not have an overload matching the given types: " <> ppShow (ba ^. inferredTypes)
    show (UnknownFunction f) = "The function " <> toS f <> " is unknown"
    show (SubselectMustReturnSingleColumn f) = "Subselect must return a single column, but it's \
        \return columns are " <> show f
    show (CaseBranchesIncompatible ts) = "Branches of a case expression don't have the same value: "
        <> show ts

findInContext :: TypeCheckContext -> TableField -> Maybe InferredType
findInContext tcc (TableField tabName colName) =
    view _3
    <$> find
        (\(tableNames, mcolName, _) ->
            mcolName == Just colName
            && maybe True (`elem` tableNames) tabName)
        (tcc ^. list)

findTableInContext :: TypeCheckContext -> Text -> [(Maybe Text, InferredType)]
findTableInContext tcc tabName =
    (\(_, f, t) -> (f, t)) <$>
    Interlude.filter (elem tabName . view _1) (tcc ^. list)

selectType ::
    MonadEffects '[ReadEnv Schema, Throw TypeCheckError] m
    => Select -> m [(Maybe Text, InferredType)]
selectType Select{mapping, source} = projectAndFold mapping =<< sourceType source

type Overload = SimpleOrComplex -> SimpleOrComplex -> Maybe SimpleOrComplex

isSqlType :: Pred SqlType -> Pred SimpleOrComplex
isSqlType p (SimpleType s) = p s
isSqlType _ _ = False

fixedResultType :: Pred SimpleOrComplex -> Pred SimpleOrComplex -> SimpleOrComplex -> Overload
fixedResultType pred1 pred2 res t1 t2
    | isAssignableTo pred1 t1 && isAssignableTo pred2 t2 = Just res
    | otherwise = Nothing

sameAsFirstParam :: Pred SimpleOrComplex -> Pred SimpleOrComplex -> Overload
sameAsFirstParam pred1 pred2 t1 t2
    | isAssignableTo pred1 t1 && isAssignableTo pred2 t2 = Just t1
    | otherwise = Nothing

opTable :: Map.Map Text [(Overload, Bool -> Bool -> Bool)]
opTable = Map.fromList
    [
        ( "||"
        , [
            ( fixedResultType
                (isSqlType (isSpecific Text))
                (isSqlType (isSpecific Text))
                (SimpleType Text)
            , (||) ) ] )
    ,
        ( "+"
        , [
            ( sameAsFirstParam
                (isSqlType (\case Interval -> True; Timestamp _ _ -> True; Time _ _ -> True; _ -> False))
                (isSqlType (isSpecific Interval))
            , (||) )  ] ) 
    ,
        ( "*"
        , [
            ( fixedResultType
                (isSqlType isNumeric)
                (isSqlType (isSpecific Interval))
                (SimpleType Interval)
            , (||) )  ] ) ]

isAssignableTo :: Pred SimpleOrComplex -> Pred SimpleOrComplex
isAssignableTo _ (SimpleType Unknown) = True
isAssignableTo p t = p t

operatorType ::
    MonadEffect (Throw TypeCheckError) m
    => Text -> (Maybe Text, InferredType) -> (Maybe Text, InferredType)
    -> m (Maybe Text, InferredType)
operatorType op' (_, it1@(InferredType t1 opt1)) (_, it2@(InferredType t2 opt2)) =
    case Map.lookup op' opTable of
        Nothing -> throwSignal (UnknownOperator op')
        Just ops -> case asum (map (\(overload, opt) -> (, opt) <$> overload t1 t2) ops) of
            Just (res, optionality) ->
                return (Nothing, InferredType res (optionality opt1 opt2))
            Nothing -> throwSignal (BadArgumentTypeMultipleError
                (BadArgumentTypeMultiple op' [it1, it2]))

unaryOperatorType ::
    MonadEffect (Throw TypeCheckError) m
    => Text -> (Maybe Text, InferredType) -> m (Maybe Text, InferredType)
unaryOperatorType op' _ = throwSignal (UnknownOperator op')

castComposite ::
    MonadEffects '[ReadEnv Schema, Throw TypeCheckError] m
    => InferredType -> [(a, InferredType)] -> SqlType -> TableId -> m SimpleOrComplex
castComposite it typs sqlT tn =  do
    sch <- readEnv @Schema
    table' <- case findTable sch tn of
        Nothing -> throwSignal (UnknownTable (_tableName (tn :: TableId)))
        Just table' -> return table'
    let cols = getTableColumns sch table'
    unless (allMatch cols) (throwSignal (InvalidTypeCast it sqlT))
    return (SimpleType sqlT)
    where
    allMatch cols = and $ zipWith (\infTyp Column{_sqlType} -> case infTyp of
        (_, InferredType (SimpleType t) _) -> _sqlType == t
        _ -> False) typs cols

castResult ::
    MonadEffects '[Throw TypeCheckError, ReadEnv Schema] m
    => InferredType -> SqlType -> m SimpleOrComplex
castResult it@(InferredType (RowType typs) _) sqlT@(CompositeType tn) = castComposite it typs sqlT tn
castResult it@(InferredType (DotStar _ typs) _) sqlT@(CompositeType tn) = castComposite it typs sqlT tn
castResult _ st = return (SimpleType st)


castable :: InferredType -> SqlType -> Bool
castable _ _ = True

simpleAggregate ::
    MonadEffect (Throw TypeCheckError) m
    => Text -> Text -> (SqlType -> Bool) -> Maybe (SqlType -> SqlType) -> [InferredType]
    -> m InferredType
simpleAggregate name' err cond (fromMaybe Interlude.id -> trans) [t] = case t ^. simpleOrComplex of
    SimpleType sqlT | cond sqlT -> go sqlT
    DotStar sqlT _ | cond sqlT -> go sqlT
    _ -> throwSignal (BadArgumentTypeError $ BadArgumentType name' 0 t err)
    where
    go sqlT = t
        & isOptional .~ True
        & simpleOrComplex .~ SimpleType (trans sqlT)
        & return
simpleAggregate name' _ _ _ args =
    throwSignal (ArityMismatchError (ArityMismatch name' 1 (length args)))

functionType :: MonadEffect (Throw TypeCheckError) m => Text -> [InferredType] -> m InferredType
functionType "avg" =
    simpleAggregate
        "avg"
        "expected smallint, int, bigint, real, double precision, numeric"
        (let p = isNumeric `orPred` isArrayOf p in p)
        (Just $ \t ->
            if t `elem` [Smallint, Integer, Bigint] then Numeric Nothing Nothing
            else Double)
functionType "max" =
    simpleAggregate
        "max"
        "expected any numeric, string, date/time, network, or enum type, or array of these types"
        (isNumeric `orPred` isString `orPred` isDateTime `orPred` isNetworkType)
        Nothing
functionType "min" =
    simpleAggregate
        "min"
        "expected any numeric, string, date/time, network, or enum type, or array of these types"
        (isNumeric `orPred` isString `orPred` isDateTime `orPred` isNetworkType)
        Nothing
functionType "sum" =
    simpleAggregate
        "sum"
        "expected smallint, int, bigint, real, double precision, numeric"
        isNumeric
        (Just $ \t ->
            if t `elem` [Smallint, Integer] then Bigint
            else if t == Bigint then Numeric Nothing Nothing
            else t)
functionType "array_agg" =
    simpleAggregate
        "array_agg"
        "expected non-array type"
        (notPred (isArrayOf anyPred))
        (Just (Array 1))
functionType "count" = \case
    [_] -> return (InferredType (SimpleType Bigint) False)
    l -> throwSignal (ArityMismatchError (ArityMismatch "count" 1 (length l)))
functionType "row" = \case
    [InferredType (DotStar _ cols) opt] -> return (InferredType (RowType cols) opt)
    its -> return (InferredType (RowType (fmap (Nothing,) its)) False)
functionType fname = const (throwSignal (UnknownFunction fname))

unknownName :: InferredType -> (Maybe Text, InferredType)
unknownName = return

commonType :: MonadEffect (Throw TypeCheckError) m => NonEmpty (Maybe Text, InferredType) -> m (Maybe Text, InferredType)
commonType ts = case NE.group (fmap (view simpleOrComplex . snd) ts) of
    [g] -> return (Just "case", InferredType (NE.head g) (and $ fmap (view isOptional . snd) ts))
    _ -> throwSignal (CaseBranchesIncompatible ts)

expToType ::
    (MonadEffects '[Throw TypeCheckError, ReadEnv Schema] m)
    => TypeCheckContext -> ValueExpression -> m (Maybe Text, InferredType)
expToType _ (NumberLiteral _) = return $ unknownName $
    InferredType (SimpleType (Numeric Nothing Nothing)) False
expToType _ (StringLiteral _) = return $ unknownName $ InferredType (SimpleType Unknown) False
expToType ctx (Operator op' e1 e2) = join $
    operatorType (Text.toLower op') <$> expToType ctx e1 <*> expToType ctx e2
expToType ctx (UnaryOperator op' e) =
    unaryOperatorType op' =<< expToType ctx e
expToType ctx (TypeCast exp' astType) = do
    sch <- readEnv @Schema
    let typ = astTypeToSqlType sch astType
    (n, typ') <- expToType ctx exp'
    resType <- castResult typ' typ
    return (n, InferredType resType (typ' ^. isOptional))
expToType ctx (TableFieldValue tf) = case findInContext ctx tf of
    Nothing -> throwSignal (UnknownField tf)
    Just t -> return (Just (_field tf), t)
expToType ctx (Application funName args) = do
    argTypes <- mapM (expToType ctx) args
    resType <- functionType (Text.toLower (_functionName funName)) (snd <$> argTypes)
    return (Just (_functionName funName), resType)
expToType ctx (NullCheck _ exp') = do
    void $ expToType ctx exp'
    return $ unknownName $ InferredType (SimpleType Boolean) False
expToType ctx (AllTableFields tab) = do
    case lookup tab (ctx ^. aliases) of
        Nothing -> throwSignal (UnknownTable tab)
        Just tabType -> return $ unknownName $
            InferredType (DotStar tabType  (findTableInContext ctx tab)) False
expToType _ (SingleRowSelect sel) = do
    cols <- selectType sel
    case cols of
        [col] -> return col
        _ -> throwSignal (SubselectMustReturnSingleColumn cols)
expToType ctx (CaseExpression alts els) = do
    altTypes <- mapM (\x -> expToType ctx (_result (x :: CaseAlternative))) alts
    elseType <- mapM (\x -> expToType ctx (_result (x :: ElseAlternative))) els
    let allTypes = case elseType of
            Nothing -> altTypes
            Just t -> NE.cons t altTypes
    commonType allTypes
expToType _ NullExpr = return $ unknownName $ InferredType (SimpleType Unknown) True

unfoldDotStar :: (Maybe Text, InferredType) -> [(Maybe Text, InferredType)]
unfoldDotStar (_, InferredType (DotStar _ fs) _) = fs
unfoldDotStar t = [t]

projectAndFold ::
    MonadEffects '[Throw TypeCheckError, ReadEnv Schema] m
    => Mapping -> TypeCheckContext -> m [(Maybe Text, InferredType)]
projectAndFold Everything ts = return ((\(_, x, y) -> (x, y)) <$> (ts ^. list))
projectAndFold (Expressions exps) ctx = do
    withDotStars <- mapM (\(v, a) -> first (a <|>) <$> expToType ctx v) exps
    -- any naked table.* expression left in the select should be unpacked into multiple columns
    return (concatMap unfoldDotStar withDotStars)

aliasesForTable :: Table -> Maybe Text -> [Text]
aliasesForTable tab mal = [view name tab] <> catMaybes [mal]

columnToFieldType :: Table -> Maybe Text -> Column -> ([Text], Maybe Text, InferredType)
columnToFieldType tab mal col = toContextColumn (aliasesForTable tab mal)
    where
    toContextColumn tableNames =
        ( tableNames
        , Just (view name col)
        , InferredType
            (SimpleType (view sqlType col))
            (requiresMaybeWrapper tab col) )

allTypesOptional :: TypeCheckContext -> TypeCheckContext
allTypesOptional = list . each . _3 . isOptional .~ True

nonJoinType ::
    MonadEffects '[ReadEnv Schema, Throw TypeCheckError] m
    => NonJoinFromItem -> m TypeCheckContext
nonJoinType (FromTable tab alias) = do
    sch <- readEnv @Schema
    case find ((== tab) . view name) (view tables sch) of
        Nothing -> throwSignal (UnknownTable tab)
        Just tab' -> return (TypeCheckContext
            (map (columnToFieldType tab' alias) (getTableColumns sch tab'))
            (fmap (\al -> (al, CompositeType (tab' ^. L.id))) (aliasesForTable tab' alias)))
nonJoinType (Subselect sel alias) =
    flip TypeCheckContext [(alias, Record)] . fmap (uncurry ([alias],,)) <$> selectType sel

fromItemType ::
    MonadEffects '[ReadEnv Schema, Throw TypeCheckError] m
    => FromItem -> m TypeCheckContext
fromItemType (NonJoinFromItem nj) = nonJoinType nj
fromItemType (Join l t r _) = do
    leftTypes <- fromItemType l
    rightTypes <- fromItemType r
    return (transformLeft leftTypes <> transformRight rightTypes)
    where
    (transformLeft, transformRight) = case t of
        LeftJoin -> (Interlude.id, allTypesOptional)
        RightJoin -> (allTypesOptional, Interlude.id)
        FullJoin -> (allTypesOptional, allTypesOptional)
        _ -> (Interlude.id, Interlude.id)

sourceType ::
    MonadEffects '[ReadEnv Schema, Throw TypeCheckError] m
    => Maybe Source -> m TypeCheckContext
sourceType Nothing = return mempty
sourceType (Just (Source fromItems)) = foldMap Interlude.id <$> mapM fromItemType fromItems
