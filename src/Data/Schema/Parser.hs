{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleContexts, OverloadedStrings, DisambiguateRecordFields #-}
module Data.Schema.Parser where

import Internal.Interlude hiding (many, some, All)
import Text.Megaparsec
import Data.Schema.Types.AST
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import qualified Control.Monad.Combinators.Expr as E
import qualified Data.Text as T
import Data.Functor (($>))
import qualified Data.List.NonEmpty as NE
import Data.Schema.Parser.Utils

simpleTableIdentifier :: Parser TableName
simpleTableIdentifier = do
    tableName <- identifier
    notFollowedBy dot
    return (TableName Nothing tableName)

qualifiedTable :: Parser TableName
qualifiedTable = label "qualified table" $ do
    schema' <- identifier
    void dot
    TableName (Just schema') <$> identifier

tableIdentifier :: Parser TableName
tableIdentifier = try simpleTableIdentifier <|> qualifiedTable

functionName :: Parser FunctionName
functionName = (\(TableName s n) -> FunctionName s n) <$> tableIdentifier

keyOperator :: Text -> Parser Text
keyOperator op = keysymbol op
    <?> toS ("operator '" <> op <> "'")
keyOperator_ :: Text -> Parser ()
keyOperator_ op = void $ keyOperator op

anyToken :: Parser Text
anyToken =
        (T.singleton <$> comma) <|> stringLit <|> (toS . show <$> numberLit)
    <|> (T.singleton <$> lparen) <|> (T.singleton <$> rparen) <|> (T.singleton <$> lbracket)
    <|> (T.singleton <$> rbracket) <|> (T.singleton <$> lbrace) <|> (T.singleton <$> rbrace)
    <|> (T.singleton <$> semicolon) <|> (T.singleton <$> dot) <|> operator <|> quotedIdent
    <|> word

------
-- AST
------

inParens :: Parser a -> Parser a
inParens = between lparen rparen

optionallyInParens :: Parser a -> Parser a
optionallyInParens p = try p <|> inParens (optionallyInParens p)

tupled :: Parser a -> Parser [a]
tupled p = inParens (p `sepBy` comma)

tupled1 :: Parser a -> Parser (NonEmpty a)
tupled1 p = NE.fromList <$> inParens (p `sepBy1` comma)

sqlType :: Parser SqlType
sqlType =
    try array <|> primitive <?> "column type"
    where
    primitive =
        -- @TODO support for
        --  * monetary types (depends on postgresql settings, lc_monetary)
        --  * interval date type - complex data parsing
        --  * arrays
        --  * composite types
        --  * range types
        ----------------------------
        ---- Numeric data types ----
        ----------------------------
            keyword_ "smallint" $> SmallInt
        <|> keyword_ "integer" $> SqlInteger
        <|> keyword_ "bigint" $> BigInt
        <|> keyword_ "int" $> SqlInt
        <|> keyword_ "int4" $> Int4
        <|> keyword_ "smallserial" $> SmallSerial
        <|> keyword_ "serial" $> Serial
        <|> keyword_ "bigserial" $> BigSerial
        <|> keyword_ "real" $> Real
        <|> keyword_ "double" *> keyword_ "precision" $> DoublePrecision
        <|> keyword_ "double" $> SqlDouble
        <|> (try (keyword_ "numeric") <|>  keyword_ "decimal") *> (do
            void $ try lparen
            prec <- try L.decimal
            void $ try comma
            scale <- L.decimal
            void rparen
            return (NumericPS prec scale))
        <|> keyword_ "numeric" *> lookAhead lparen *> try (NumericP <$> inParens L.decimal)
        <|> keyword_ "numeric" $> Numeric

        ------------------------------
        ---- Character data types ----
        ------------------------------
        <|> try (keyword_ "character" *> keyword_ "varying" *> (CharacterVaryingN <$> inParens L.decimal))
        <|> keyword_ "character" *> keyword_ "varying" $> CharacterVarying
        <|> keyword_ "text" $> SqlText

        ---------------------------
        ---- Binary data types ----
        ---------------------------
        <|> keyword_ "bytea" $> ByteA

        ------------------------------
        ---- Date/time data types ----
        ------------------------------
        <|> keyword_ "date" $> Date

        <|> try (keyword_ "timestamp" *>
            keyword_ "with" *> keyword_ "time" *> keyword_ "zone" $>
            TimestampWithTimeZone)

        <|> keyword_ "timestamp" *> (do
            void $ optional (keyword_ "without" *> keyword_"time" *> keyword_ "zone")
            return Timestamp)

        <|> try (keyword_ "time" *>
            keyword_ "with" *> keyword_"time" *> keyword_ "zone" $>
            TimeWithTimeZone)

        <|> keyword_ "time" *> (do
            void $ optional (keyword_ "without" *> keyword_"time" *> keyword_ "zone")
            return Time)

        <|> keyword_ "timestamptz" $> TimestampWithTimeZone

        <|> keyword_ "interval" $> Interval

        ----------------------------
        ---- Boolean data types ----
        ----------------------------
        <|> keyword_ "boolean" $> Boolean


        ------------------------------
        ---- Geometric data types ----
        ------------------------------
        <|> keyword_ "point" $> Point
        <|> keyword_ "line" $> Line
        <|> keyword_ "lseg" $> LSeg
        <|> keyword_ "box" $> Box
        <|> keyword_ "path" $> Path
        <|> keyword_ "polygon" $> Polygon
        <|> keyword_ "circl" $> Circle

        ------------------------------------
        ---- Network address data types ----
        ------------------------------------
        <|> keyword_ "cidr" $> Cidr
        <|> keyword_ "inet" $> Inet
        <|> keyword_ "macaddr" $> MacAddr
        <|> keyword_ "macaddr8" $> MacAddr8

        -- Other types
        <|> keyword_ "uuid" $> UUID
        <|> keyword_ "json" $> JSON
        <|> keyword_ "jsonb" $> JSONB
        <|> keyword_ "record" $> Record
        <|> Other <$> identifier
    array = do
        p <- primitive
        limits <- sqlStyleArray <|> postgresStyleArray
        return (Array limits p)
    arrayLimit = between lbracket rbracket (optional integerLiteral)
    sqlStyleArray = keyword_ "array" *> fmap (maybe [Nothing] return) (optional arrayLimit)
    postgresStyleArray = some arrayLimit

tableConstraint :: Parser TableConstraint
tableConstraint = (
    (keyword_ "primary" *> keyword_ "key"
        *> (TablePrimaryKey <$> tupled1 identifier)
        <?> "primary key constraint")
    <|> (keyword_ "unique"
        *> (TableUnique <$> tupled1 identifier)
        <?> "unique constraint")
    <|> (keyword_ "foreign" *> keyword_ "key"
        *> (TableForeignKey
            <$> tupled1 identifier
            <*> (keyword_ "references" *> tableIdentifier)
            <*> tupled1 identifier)
            <?> "foreign key constraint")
    )
    <?> "table constraint"

tableField :: Parser TableField
tableField = TableField <$> optional (try (identifier <* dot)) <*> identifier

caseAlternative :: Parser CaseAlternative
caseAlternative =
    CaseAlternative <$> (keyword_ "when" *> valueExpression) <*> (keyword_ "then" *> valueExpression)
        <?> "case alternative"

elseAlternative :: Parser ElseAlternative
elseAlternative =
    ElseAlternative <$> (keyword_ "else" *> valueExpression)

valueExpression :: Parser ValueExpression
valueExpression = E.makeExprParser term table <* whitespace <?> "value expression"
    where
    term =
        try (inParens ((\(expr, _) -> SingleRowSelect expr) <$> select))
        <|> inParens valueExpression
        <|> (NumberLiteral <$> L.signed whitespace L.scientific <* whitespace <?> "number literal")
        <|> (StringLiteral <$> stringLit <?> "string literal")
        <|> (try (Application <$> functionName <*> tupled valueExpression) <?> "function application")
        <|> try (AllTableFields <$> identifier <* dot <* keysymbol_ "*")
        <|> (keyword_ "case" *>
            (CaseExpression <$> someNonEmpty caseAlternative <*> optional elseAlternative)
            <* keyword_ "end")
        <|> (TableFieldValue <$> tableField <?> "table field")
        <|> (keyword_ "null" >> return NullExpr)
    table =
        [ [E.Postfix typeCast]
        , [E.InfixR (symbolOp "^")]
        , [E.InfixR (symbolOp "%"), E.InfixR (symbolOp "*"), E.InfixR (symbolOp "/")]
        , [E.InfixR (symbolOp "+"), E.InfixR (symbolOp "-")]
        , [E.InfixR (symbolOp "&"), E.InfixR (symbolOp "|"),
           E.InfixR (symbolOp "#"), E.InfixR (symbolOp "<<"), E.InfixR (symbolOp ">>") ]
        , [E.InfixR (symbolOp "="), E.InfixR (symbolOp "<"), E.InfixR (symbolOp ">"),
           E.InfixR (symbolOp "<="), E.InfixR (symbolOp ">="), E.InfixR (symbolOp "<>"), E.InfixR (symbolOp "!=")]
        , [E.Postfix nullCheck]
        , [E.InfixL (symbolOp "||")]
        , [E.Prefix (keyword_ "not" >> return (UnaryOperator "not"))]
        , [E.InfixL (wordOp "and")]
        , [E.InfixL (wordOp "or")] ]
    typeCast = do
        keyOperator_ "::"
        t <- Data.Schema.Parser.sqlType
        return (`TypeCast` t)
    symbolOp op = do
        keyOperator_ op
        return (Operator op)
    wordOp op = do
        keyword_ op
        return (Operator op)
    nullCheck = do
        keyword_ "is"
        n <- isJust <$> optional (keyword_ "not")
        keyword_ "null"
        return (NullCheck n)

columnConstraint :: Parser ColumnConstraint
columnConstraint = (
        (keyword_ "not" *> keyword_ "null" *> return NotNull <?> "not null constraint")
    <|> (keyword_ "primary" *> keyword_ "key" *> return PrimaryKey <?> "primary key constraint")
    <|> (keyword_ "unique" *> return Unique <?> "unique constraint")
    <|> (keyword_ "default" *> (Default <$> valueExpression) <?> "unique constraint")
    <|> (keyword_ "references" *>
        (References
            <$> tableIdentifier
            <*> optional (between lparen rparen identifier))
            <?> "foreign key constraint")
    )
    <?> "column constraint"

namedConstraint :: Parser a -> Parser (NamedConstraint a)
namedConstraint p = do
    c <- optional (keyword_ "constraint")
    case c of
        Nothing -> NamedConstraint Nothing <$> p
        Just _ -> NamedConstraint <$> (Just <$> identifier) <*> p

createTableColumn :: Parser CreateTableColumn
createTableColumn = label "column definition" $ do
    name <- identifier
    sqlType' <- Data.Schema.Parser.sqlType
    constraints <- many (namedConstraint columnConstraint)
    return CreateTableColumn { _sqlType = sqlType', _name = name, _constraints = constraints}

createTable :: Parser (CreateTable, SourcePos)
createTable = label "create table statement" $ do
    try (keyword_ "create" >> keyword_ "table")
    void $ optional (keyword_ "if" *> keyword_ "not" *> keyword_ "exists")
    tableIdent <- tableIdentifier
    void lparen
    (columns, tableConstraints) <- partitionEithers <$> columnOrConstraint `sepBy1` comma
    void rparen
    pos <- getSourcePos
    return (CreateTable tableIdent columns tableConstraints, pos)
    where
    columnOrConstraint = label "column definitions" $
        Right <$> namedConstraint tableConstraint <|> Left <$> createTableColumn

columnAlteration :: Parser ColumnAlteration
columnAlteration = keyword_ "set" *> keyword_ "default" *> (SetDefault <$> valueExpression)

rlsAction :: Parser RLSAction
rlsAction =
    (EnableRLS <$ keyword_ "enable")
    <|> (DisableRLS <$ keyword_ "disable")
    <|> (ForceRLS <$ keyword_ "force")

alteration :: Parser Alteration
alteration =
        (keyword_ "add" *> try (AddConstraint <$> namedConstraint tableConstraint) <?> "add constraint")
    <|> (keyword_ "owner" *> keyword_ "to" *> (AlterOwner <$> identifier))
    <|> (keyword_ "alter" *> optional (keyword_ "column") *>
        (AlterColumn <$> identifier <*> columnAlteration))
    <|> (RowLevelSecurity <$> rlsAction <* keyword_ "row" <* keyword_ "level" <* keyword_ "security")
    <|> (UnknownAlteration . T.unwords <$> anyToken `someTill` lookAhead semicolon <?> "unknown alteration")
    <?> "alteration"

alterTable :: Parser (AlterTable, SourcePos)
alterTable = do
    try (keyword_ "alter" >> keyword_ "table")
    _ <- try (keyword_ "only" <|> keyword_ "if exists" <|> whitespace)
    tableIdent <- tableIdentifier
    alteration' <- Data.Schema.Parser.alteration
    sourcePos <- getSourcePos
    return (AlterTable { _tableName = tableIdent, _alteration = alteration' }, sourcePos)

unknownStatement :: Parser (Text, SourcePos)
unknownStatement = do
    sourcePos <- getSourcePos
    statement <- T.unwords <$> recurse ([] :: [Text])
    return (statement, sourcePos)
    where
    stopToken = semicolon <|> lparen <|> rparen <|> lbrace <|> rbrace <|> lbracket <|> rbracket
    recurse parenStack = do
        toks <- anyToken `manyTill` lookAhead stopToken
        term <- T.singleton <$> lookAhead stopToken -- what stopped the `manyTill`
        let pars = [(")", "("), ("]", "["), ("}", "{")]
            open = map snd pars
            closed = map fst pars
        rest <- case term of
            ";"
                | null parenStack -> return []
                | otherwise -> recurse parenStack
            x | x `elem` open -> stopToken *> recurse (x : parenStack)
            x | x `elem` closed -> let Just o = lookup x pars in case parenStack of -- x `elem` closed <=> x `elem` (map fst pars) => isJust (lookup x pars)
                lastOpen : rest | lastOpen == o -> stopToken *> recurse rest
                lastOpen : _ -> fail $ toS $
                    "Mismatched parentheses! Last open parentheses is '" <> lastOpen
                    <> "' but the encountered closing parentheses is '" <> x <> "'"
                [] -> fail $ toS $ "Found '" <> x <> "', but no matching open parentheses"
            _ -> error "Unknown token in lookAhead! Shouldn't happen"
        return (toks <> [term] <> rest)

setStatement :: Parser (Text, SourcePos)
setStatement = do
  (statement, sourcePos) <- keyword_ "set" *> unknownStatement
  return (statement, sourcePos)

createTypeStatement :: Parser (CreateType, SourcePos)
createTypeStatement = do
  try (keyword_ "create" *> keyword_"type")
  tableIdent <- tableIdentifier
  (ct, sourcePos) <- createEnumeratedType
  return (CreateType tableIdent ct, sourcePos)

createEnumeratedType :: Parser (CustomType, SourcePos)
createEnumeratedType = do
  void $ keyword_ "as" *> keyword_ "enum"
  res <- inParens ((between (char '\'') (char '\'') word <* whitespace) `sepBy1` comma)
  sourcePos <- getSourcePos
  return $ (Enumerated res, sourcePos)

createSequence :: Parser (Sequence, SourcePos)
createSequence = do
    try (keyword_ "create" *> keyword_ "sequence")
    void $ optional (keyword_ "if" *> keyword_ "not" *> keyword_ "exists")
    tableIdent <- tableIdentifier
    void unknownStatement -- ignore the rest of the sequence declaration
    sourcePos <- getSourcePos
    return (Sequence tableIdent, sourcePos)

asAlias :: Parser Text
asAlias = keyword_ "as" >> identifier

aliasWithOptionalAs :: Parser Text
aliasWithOptionalAs = optional (keyword_ "as") >> identifier

expWithAlias :: Parser (ValueExpression, Maybe Text)
expWithAlias = (,) <$> valueExpression <*> optional asAlias

-- @TODO FromTable doesn't support different schemas. Instead of `identifier`
-- we should be using something that support schema.table_name syntax
nonJoinFromItem :: Parser NonJoinFromItem
nonJoinFromItem = (\(expr, _) -> Subselect expr) <$> between lparen rparen select <*> aliasWithOptionalAs
    <|> FromTable <$> identifier <*> optional aliasWithOptionalAs

joinTypeParser :: Parser JoinType
joinTypeParser = do
    void $ optional $ keyword "natural"
    (LeftJoin <$ (keyword_ "left" >> optional (keyword_ "outer"))
        <|> RightJoin <$ (keyword_ "right" >> optional (keyword_ "outer"))
        <|> FullJoin <$ (keyword_ "full" >> optional (keyword_ "outer"))
        <|> CrossJoin <$ keyword_ "cross"
        <|> InnerJoin <$ optional (keyword_ "inner")) <* keyword_ "join"

joinConditionParser :: Parser JoinCondition
joinConditionParser = JoinCondition <$> valueExpression

fromItemParser :: Parser FromItem
fromItemParser = do
    firstItem <- try (NonJoinFromItem <$> nonJoinFromItem) <|> inParens fromItemParser
    joins <- many $ do
         typ <- joinTypeParser
         arg <- fromItemParser
         cond <- optional (keyword_ "on" >> joinConditionParser)
         return (\l -> Join l typ arg cond)
    return $ foldr ($) firstItem joins

sourceParser :: Parser Source
sourceParser = Source <$> commaSeparatedNonEmpty (optionallyInParens fromItemParser)

filterParser :: Parser Where
filterParser = Where <$> valueExpression

groupingParser :: Parser Grouping
groupingParser =
    GroupList <$> between lparen rparen (commaSeparatedNonEmpty valueExpression)
    <|> GroupExpression <$> valueExpression

groupByParser :: Parser GroupBy
groupByParser = GroupBy <$> commaSeparatedNonEmpty groupingParser

havingParser :: Parser Having
havingParser = Having <$> valueExpression

orderByParser :: Parser OrderBy
orderByParser = OrderBy <$> commaSeparatedNonEmpty ((,) <$> valueExpression <*> direction)
    where
    direction = optional (Ascending <$ keyword_ "asc" <|> Descending <$ keyword_ "desc")

limitParser :: Parser Limit
limitParser = Limit <$> integerLiteral

offsetParser :: Parser Offset
offsetParser = Offset <$> integerLiteral

select :: Parser (Select, SourcePos)
select = do
    try (keyword_ "select")
    allOrDistinct <- optional $ (All <$ keyword_ "all") <|> (Distinct <$ keyword_ "distinct")
    mapping <- (Everything <$ keysymbol_ "*") <|> (Expressions <$> commaSeparated expWithAlias)
    source <- optional $ keyword_ "from" >> sourceParser
    filter' <- optional $ keyword_ "where" >> filterParser
    grouping <- optional $ keyword_ "group" >> keyword_ "by" >> groupByParser
    having <- optional $ keyword_ "having" >> havingParser
    orderBy <- optional $ keyword_ "order" >> keyword_ "by" >> orderByParser
    limit <- optional $ keyword_ "limit" >> limitParser
    offset <- optional $ keyword_ "offset" >> offsetParser

    sourcePos <- getSourcePos
    return (Select{filter = filter', ..}, sourcePos)
