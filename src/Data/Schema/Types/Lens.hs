{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
module Data.Schema.Types.Lens where

import Internal.Interlude hiding (id)
import Control.Lens.TH

import Data.Schema.Types.AST
import Data.Schema.Types.Schema


makeFieldsNoPrefix ''NamedConstraint
makeFieldsNoPrefix ''CreateTableColumn
makeFieldsNoPrefix ''CreateTable
makeFieldsNoPrefix ''AlterTable
makeFieldsNoPrefix ''Table
makeFieldsNoPrefix ''Column
makeFieldsNoPrefix ''Schema
makeFieldsNoPrefix ''TableField
makeFieldsNoPrefix ''ColumnId
makeFieldsNoPrefix ''TableId

instance HasName Table Text where
    name = id . tableName
instance HasName Column Text where
    name = id . columnName