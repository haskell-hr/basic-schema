{-# LANGUAGE DuplicateRecordFields, DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Data.Schema.Types.AST where

import Internal.Interlude
import Data.Scientific
import GHC.Generics
import Test.SmallCheck.Series hiding (NonEmpty)
import Data.String (IsString(..))
import Text.Megaparsec.Pos

{-# ANN module ("HLint: ignore Use newtype instead of data" :: Text) #-}

data SqlType =
    -- Numeric types
      SqlInt
    | SmallSerial
    | Serial
    | BigSerial
    | SmallInt
    | BigInt
    | SqlInteger
    | Int4
    | DoublePrecision
    | SqlDouble
    | Real
    | Numeric
    | NumericPS Int Int
    | NumericP Int
    -- Character data types
    | CharacterVarying
    | SqlText
    -- Date/Time data types
    | Timestamp
    | TimestampWithTimeZone
    | Time
    | TimeWithTimeZone
    | Date
    | Interval
    -- Boolean data types ----
    | Boolean
    -- Geometric data types
    | Point
    | Line
    | LSeg
    | Box
    | Path
    | Polygon
    | Circle
    -- Network address data types
    | Cidr
    | Inet
    | MacAddr
    | MacAddr8

    -- other
    | JSON
    | JSONB
    | ByteA
    | CharacterVaryingN Int
    | UUID
    | Array [Maybe Integer] SqlType
    | Other Text -- values that we do not know yet, but might later (e.g. enum)
    | Unknown
    | Record
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data TableName = TableName
    { _schema :: Maybe Text
    , _tableName :: Text }
    deriving (Eq, Ord, Read, Generic, Serial m)
instance IsString TableName where
    fromString str = TableName Nothing (toS str)

instance Show TableName where
  show (TableName schema' tableName') = toS (prefix <> tableName')
    where prefix = maybe "" (\s -> s <> ".") schema'

data TableConstraint =
      TablePrimaryKey (NonEmpty Text) | TableUnique (NonEmpty Text) | TableForeignKey (NonEmpty Text) TableName (NonEmpty Text)
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data TableField = TableField
    { _table :: Maybe Text
    , _field :: Text }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data CaseAlternative = CaseAlternative
    { _condition :: ValueExpression
    , _result :: ValueExpression }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

newtype ElseAlternative = ElseAlternative
    { _result :: ValueExpression }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data FunctionName = FunctionName
    { _schema :: Maybe Text
    , _functionName :: Text }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data ValueExpression =
      NumberLiteral Scientific
    | StringLiteral Text
    | Operator Text ValueExpression ValueExpression
    | UnaryOperator Text ValueExpression
    | NullCheck Bool ValueExpression
    | TypeCast ValueExpression SqlType
    | TableFieldValue TableField
    | AllTableFields Text
    | Application FunctionName [ValueExpression]
    | SingleRowSelect Select
    | CaseExpression (NonEmpty CaseAlternative) (Maybe ElseAlternative)
    | NullExpr
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data ColumnConstraint = NotNull | PrimaryKey | Unique | Default ValueExpression | Null | References TableName (Maybe Text)
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data NamedConstraint a = NamedConstraint
    { _name :: Maybe Text
    , _constraint :: a }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data CreateTableColumn = CreateTableColumn
    { _name :: Text
    , _sqlType :: SqlType
    , _constraints :: [NamedConstraint ColumnConstraint] }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data CreateTable = CreateTable
    { _tableName :: TableName
    , _columns :: [CreateTableColumn]
    , _tableConstraints :: [NamedConstraint TableConstraint] }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data ColumnAlteration =
    SetDefault ValueExpression
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data RLSAction = EnableRLS | DisableRLS | ForceRLS | NoForceRLS
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data Alteration =
    AddConstraint (NamedConstraint TableConstraint)
    | AlterOwner Text
    | AlterColumn Text ColumnAlteration
    | RowLevelSecurity RLSAction
    | UnknownAlteration Text
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data AlterTable = AlterTable
    { _tableName :: TableName
    , _alteration :: Alteration }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data CustomType =
    Composite [(Text, SqlType)]
    | Enumerated [Text]
    -- @TODO proper support for range types (subtype, subtype_diff, caonical)
    | Range (Text, Text)
    | Scalar
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data AllOrDistinct = All | Distinct
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data Mapping = Everything | Expressions [(ValueExpression, Maybe Text)]
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data JoinType = InnerJoin | LeftJoin | RightJoin | FullJoin | CrossJoin
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

newtype JoinCondition = JoinCondition ValueExpression
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data NonJoinFromItem =
    FromTable Text (Maybe Text)
    | Subselect Select Text
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data FromItem =
    NonJoinFromItem NonJoinFromItem
    | Join FromItem JoinType FromItem (Maybe JoinCondition)
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

newtype Source = Source (NonEmpty FromItem)
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

newtype Where = Where ValueExpression
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data Grouping = GroupExpression ValueExpression | GroupList (NonEmpty ValueExpression)
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

newtype GroupBy = GroupBy (NonEmpty Grouping)
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

newtype Having = Having ValueExpression
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data Direction = Ascending | Descending
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

newtype OrderBy = OrderBy (NonEmpty (ValueExpression, Maybe Direction))
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

newtype Limit = Limit Integer
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

newtype Offset = Offset Integer
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data Select = Select
    { allOrDistinct :: Maybe AllOrDistinct
    , mapping :: Mapping
    , source :: Maybe Source
    , filter :: Maybe Where
    , grouping :: Maybe GroupBy
    , having :: Maybe Having
    , orderBy :: Maybe OrderBy
    , limit :: Maybe Limit
    , offset :: Maybe Offset }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data CreateView = CreateView
    { _tableName :: TableName
    , _columnNames :: Maybe (NonEmpty Text)
    , _query :: Select }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data Sequence = Sequence
    { _tableName :: TableName }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data CreateType = CreateType
    { _typeName :: TableName
    , _customType :: CustomType }
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data Statement =
      CreateTableStatement CreateTable
    | AlterTableStatement AlterTable
    | UnknownStatement Text
    | SetStatement Text
    | CreateTypeStatement CreateType
    | SelectStatement Select
    | CreateViewStatement CreateView
    -- @TODO Missing proper support for sequences - min, max, start, increment, cache..
    | CreateSequence Sequence
    deriving (Eq, Ord, Read, Show, Generic, Serial m)

data StatementContext = StatementContext
    { statement :: Statement
    , rawStatement :: Text
    , offset :: Int
    , sourcePos :: SourcePos }
    deriving (Eq, Show)

withContext :: Text -> Int -> (t -> Statement) -> (t, SourcePos) -> StatementContext
withContext raw offset statementConstructor =
  \(statement, sourcePos) -> StatementContext (statementConstructor statement) raw offset sourcePos


#if !MIN_VERSION_smallcheck(1,1,0)
instance Serial m a => Serial m (NonEmpty a) where
    series = (:|) <$> series <*> series
#endif

instance Monad m => Serial m Text where
    series = generate (\d -> toS <$> replicateM d "a")

instance Monad m => Serial m Scientific where
    series = realToFrac <$> series @_ @Double
