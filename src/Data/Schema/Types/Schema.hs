{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE DeriveGeneric #-}
module Data.Schema.Types.Schema where

import Internal.Interlude
import qualified Data.Schema.Types.AST as AST
import Data.Text (pack)
import GHC.Generics
import Language.Haskell.TH.Syntax (Lift)
import Instances.TH.Lift ()

-- Constants
defaultSchema :: Text
defaultSchema = "public"
--

type Name = Text

data SqlType =
    Bigint | Bit Bool (Maybe Int) | Boolean | Box | Bytea | Character Bool (Maybe Int) | Cidr | Circle
    | Date | Double | Inet | Integer | Json | Jsonb | Line | Lseg | Macaddr | Money
    | Numeric (Maybe Int) (Maybe Int) | Path | Pglsn | Point | Polygon | Real | Smallint
    | Text | Time (Maybe Int) Bool | Timestamp (Maybe Int) Bool | Interval | Tsquery
    | Tsvector | Txidsnapshot | Uuid | Xml | Unknown | Array Int SqlType | CompositeType TableId
    | EnumType TableId [Text] | Record
    deriving (Eq, Ord, Read, Show, Generic, Lift)

data Column = Column
    { _id :: ColumnId
    , _sqlType :: SqlType
    } deriving (Ord, Read, Show, Generic, Lift)

instance Eq Column where
    (==) a b = _id (a :: Column) == _id (b :: Column)

-- | By design constraints can only be applied to tables, to make main use case easier
--  retrieval of table related data for ORMs. We allow ourself such freadom because of:
--
--  https://www.postgresql.org/docs/10/static/sql-createtable.html
--
--  A table constraint definition is not tied to a particular column,
--  and it can encompass more than one column. Every column constraint can also be written
--  as a table constraint; a column constraint is only a notational convenience for use
--  when the constraint only affects one column.
--
data TableConstraint = NotNull ColumnId
    | Null ColumnId
    | Unique (NonEmpty ColumnId)
    | Default ColumnId AST.ValueExpression
    | ForeignKey (NonEmpty ColumnId) TableId (NonEmpty ColumnId)
    | PrimaryKey (NonEmpty ColumnId)
    | Check ColumnId Text
    deriving (Eq, Ord, Read, Show, Generic)

data TableId = TableId { _schemaName :: Text, _tableName :: Text } deriving (Ord, Read, Show, Eq, Generic, Lift)
data ColumnId = ColumnId { _tableId :: TableId, _columnName ::  Text } deriving (Ord, Read, Show, Eq, Generic, Lift)

data Table = Table
    { _id :: TableId
    , _columns :: [ColumnId]
    , _constraints :: [AST.NamedConstraint TableConstraint]
    } deriving (Ord, Read, Show, Generic)

instance Eq Table where
    (==) a b = _id (a :: Table) == _id (b :: Table)


data Schema = Schema
    { _tables :: [Table]
    , _columns :: [Column]
    , _types :: [(TableId, AST.CustomType)] }
    deriving (Eq, Ord, Read, Show, Generic)

instance Semigroup Schema where
    (<>) = mappend

instance Monoid Schema where
    mempty = Schema [] [] []
    mappend (Schema a1 b1 c1) (Schema a2 b2 c2) = Schema (a1 <> a2) (b1 <> b2) (c1 <> c2)


getColumnId :: Table -> Text -> ColumnId
getColumnId table =
    ColumnId (_id (table :: Table))

astTypeToSqlType :: Schema -> AST.SqlType -> SqlType
astTypeToSqlType schema = \case
    AST.SqlInt -> Integer
    AST.SmallSerial -> Smallint
    AST.Serial -> Integer
    AST.BigSerial -> Bigint
    AST.SmallInt -> Smallint
    AST.BigInt -> Bigint
    AST.SqlInteger -> Integer
    AST.Int4 -> Integer
    AST.DoublePrecision -> Double
    AST.SqlDouble -> Double
    AST.Real -> Real
    AST.Numeric -> Numeric Nothing Nothing
    (AST.NumericPS p s) -> Numeric (Just p) (Just s)
    (AST.NumericP p) -> Numeric (Just p) Nothing
    AST.CharacterVarying -> Character True Nothing
    AST.SqlText -> Text
    AST.Timestamp -> Timestamp Nothing False
    AST.TimestampWithTimeZone -> Timestamp Nothing True
    AST.Time -> Time Nothing False
    AST.TimeWithTimeZone -> Time Nothing True
    AST.Interval -> Interval
    AST.Date -> Date
    AST.Boolean -> Boolean
    AST.Point -> Point
    AST.Line -> Line
    AST.LSeg -> Lseg
    AST.Box -> Box
    AST.Path -> Path
    AST.Polygon -> Polygon
    AST.Circle -> Circle
    AST.Cidr -> Cidr
    AST.Inet -> Inet
    AST.MacAddr -> Macaddr
    AST.MacAddr8 -> Macaddr
    AST.JSON -> Json
    AST.JSONB -> Jsonb
    AST.ByteA -> Bytea
    (AST.CharacterVaryingN n) -> Character True (Just n)
    AST.UUID -> Uuid
    AST.Unknown -> Unknown
    (AST.Array lims typ) -> Array (length lims) (astTypeToSqlType schema typ)
    (AST.Other tabName) ->
        let tId = TableId defaultSchema tabName
        in case lookup tId (_types schema) of
            Nothing -> case find (\Table{_id} -> _id == tId) (_tables schema) of
                Nothing -> error ("Unknown custom type: " <> show tabName)
                Just _ -> CompositeType tId
            Just (AST.Enumerated enums) -> EnumType tId enums
            Just t -> error ("Custom type still unsupported: " <> show t)
    AST.Record -> Record

ifFlag :: Bool -> Text -> Text
ifFlag f t = if f then t else ""

ifJust :: Maybe a -> (a -> Text) -> Text
ifJust m f = maybe "" f m

inParens :: Show a => Maybe a -> Text
inParens Nothing = ""
inParens (Just m) = " (" <> pack (show m) <> ")"

renderTableId :: TableId -> Text
renderTableId (TableId sch tab) = sch <> "." <> tab

renderSqlType :: SqlType -> Text
renderSqlType = \case
    Bigint -> "bigint"
    Bit var n -> "bit" <> ifFlag var " varying" <> inParens n
    Boolean -> "boolean"
    Box -> "box"
    Bytea -> "bytea"
    Character var n -> "character" <> ifFlag var " varying" <> inParens n
    Cidr -> "cidr"
    Circle-> "circle"
    Date -> "date"
    Double -> "double"
    Inet -> "inet"
    Integer -> "integer"
    Json -> "json"
    Jsonb -> "jsonb"
    Line -> "line"
    Lseg -> "lseg"
    Macaddr -> "macaddr"
    Money -> "money"
    Numeric p s -> "numeric"
        <> ifJust p (\p' ->
            " (" <> pack (show p') <> ifJust s (\s' -> ", " <> pack (show s')) <> ")")
    Path -> "path"
    Pglsn -> "pglsn"
    Point -> "point"
    Polygon -> "polygon"
    Real -> "real"
    Smallint -> "smallint"
    Text -> "text"
    Time p tz -> "time" <> inParens p <> ifFlag tz " with time zone"
    Timestamp p tz -> "timestamp" <> inParens p <> ifFlag tz " with time zone"
    Interval -> "interval"
    Tsquery -> "tsquery"
    Tsvector -> "tsvector"
    Txidsnapshot -> "txidsnapshot"
    Uuid -> "uuid"
    Xml -> "xml"
    Unknown -> "unknown"
    Array dims typ -> renderSqlType typ <> mconcat (replicate dims "[]")
    CompositeType tid -> renderTableId tid
    EnumType tid _ -> renderTableId tid
    Record -> "record"

type Pred a = a -> Bool

orPred :: Pred a -> Pred a -> Pred a
orPred = liftA2 (||)

isNumeric :: Pred SqlType
isNumeric Numeric{} = True
isNumeric t = t `elem` [Smallint, Integer, Bigint, Real, Double]

isString :: Pred SqlType
isString Character{} = True
isString Text = True
isString _ = False

isDateTime :: Pred SqlType
isDateTime Time{} = True
isDateTime Timestamp{} = True
isDateTime Date = True
isDateTime _ = False

isNetworkType :: Pred SqlType
isNetworkType = (`elem` [Inet, Macaddr, Cidr])

isArrayOf :: Pred SqlType -> Pred SqlType
isArrayOf p (Array _ t) = p t
isArrayOf _ _ = False

notPred :: Pred a -> Pred a
notPred p = not . p

anyPred :: Pred a
anyPred = const True

isSpecific :: SqlType -> Pred SqlType
isSpecific = (==)

