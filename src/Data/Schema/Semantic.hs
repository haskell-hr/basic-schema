{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveFunctor #-}
module Data.Schema.Semantic where

import Internal.Interlude hiding (id)
import Control.Lens hiding (Context(..))

import Data.Schema.Types.Schema (defaultSchema)
import qualified Data.Schema.Types.Schema as Semantic
import qualified Data.Schema.Types.AST as AST
import Data.Schema.Query
import Data.Schema.Types.Lens
import Data.Schema.Semantic.Select
import Data.Schema.SemanticError
import Data.List
import qualified Data.List.NonEmpty as NE

tableNameToTableId :: AST.TableName -> Semantic.TableId
tableNameToTableId (AST.TableName msch tab) = Semantic.TableId (fromMaybe defaultSchema msch) tab

semanticCompile ::
  (MonadEffects '[Throw SemanticErrorContext] m) =>
  [AST.StatementContext] -> m Semantic.Schema
semanticCompile =
  foldM (\schema' ctx' -> semanticCompile' schema' ctx') mempty

semanticCompile' ::
  (MonadEffects '[Throw SemanticErrorContext] m) =>
  Semantic.Schema ->
  AST.StatementContext ->
  m Semantic.Schema
semanticCompile' _schema ctx'@(AST.StatementContext (AST.CreateTableStatement ct) _ _ _) = do
  let tableId = tableNameToTableId (ct ^. tableName)
  let tablePlaceholder = Semantic.Table tableId [] []
  case findTable _schema tableId of
    Just _ -> throwSignal (SemanticErrorContext (TableAlreadyExists ct) ctx')
    Nothing -> do
       (newColumns, newConstraints) <- addColumns _schema tableId ctx' (ct ^. columns)
       -- save temporary schema so we ca use it when applying constraints defined
       -- at the table level
       let schema' = _schema & columns %~ (<> newColumns)
       -- convert table constraints to something usable
       let tableConsts = tableASTToSemantic schema' tableId <$> (ct ^. tableConstraints)
       -- recreate the table with table constraints added
       let table = tablePlaceholder & columns %~ (<> (view id <$> newColumns))
                                    & constraints %~ (<> nub (newConstraints <> tableConsts))
       -- recreate schema with table added
       let schema = _schema & tables %~ (<> [table]) & columns %~ (<> newColumns)
       return schema

semanticCompile' _schema ctx'@(AST.StatementContext (AST.AlterTableStatement (AST.AlterTable tableName alteration)) _ _ _) = do
  let mTable = findTable _schema (tableNameToTableId tableName)
  case mTable of
     Nothing -> throwSignal (SemanticErrorContext (TableDoesNotExists tableName) ctx')
     Just semanticTable ->
       updateTable _schema <$> applyTableAlteration _schema semanticTable alteration ctx'
semanticCompile' _schema (AST.StatementContext (AST.CreateTypeStatement (AST.CreateType typeName customType)) _ _ _) = do
  return $ _schema & types %~ (<> [(tableNameToTableId typeName, customType)])
semanticCompile' _schema ctx'@(AST.StatementContext (AST.CreateSequence (AST.Sequence tableName)) _ _ _) =
  semanticCompile' _schema (ctx' {AST.statement = AST.CreateTableStatement (AST.CreateTable tableName [] [])})
semanticCompile' _schema ctx'@(AST.StatementContext (AST.CreateViewStatement AST.CreateView{_tableName, ..}) _ _ _) = do
  inf <- implementReadEnv (return _schema) $ selectType _query
      & handleException @TypeCheckError (\x -> throwSignal (SemanticErrorContext (TypeCheckError x) ctx'))
  colNames <- case (mapM fst inf, _columnNames) of
    (Nothing, Nothing) -> throwSignal (SemanticErrorContext (ViewColumnNamesAmbiguous inf) ctx')
    (_, Just names) -> return (toList names)
    (Just names, _) -> return names
  when (length colNames /= length (nub colNames)) (throwSignal (SemanticErrorContext (ViewColumnNamesDuplicate (colNames \\ nub colNames)) ctx'))
  let viewId = tableNameToTableId _tableName
  cols <- zipWithM (makeColumn viewId) colNames (fmap snd inf)
  let colIds = view id . fst <$> cols
      tableConss = concatMap snd cols
  return $ _schema
    & tables %~ (<> [Semantic.Table viewId colIds tableConss])
    & columns %~ (<> fmap fst cols)
  where
  makeColumn
    :: MonadEffect (Throw SemanticErrorContext) m => Semantic.TableId -> Text -> InferredType
    -> m (Semantic.Column, [AST.NamedConstraint Semantic.TableConstraint])
  makeColumn tabId colName (InferredType (SimpleType sqt) isOpt) = return
    ( Semantic.Column colId sqt
    , if isOpt then [AST.NamedConstraint Nothing (Semantic.Null colId)]
        else [AST.NamedConstraint Nothing (Semantic.NotNull colId)] )
    where
    colId = Semantic.ColumnId tabId colName
  makeColumn _ _ (InferredType (DotStar _ _) _) = error "Dot star shouldn't get to this stage"
  makeColumn _ _ (InferredType (RowType ts) _) = throwSignal (SemanticErrorContext (ViewColumnIsUnknownRowType ts) ctx')
semanticCompile' _schema _ = do
  return _schema

data MaybeFiltered a = SomeFiltered [a] | NoneFiltered (NonEmpty a)
  deriving (Functor)

mapMaybeNonEmpty :: (a -> Maybe b) -> NonEmpty a -> MaybeFiltered b
mapMaybeNonEmpty f (a :| as) = case f a of
  Nothing -> SomeFiltered (mapMaybe f as)
  Just b -> go as (return b) (return b)
  where
  go [] _ ne = NoneFiltered ne
  go (x : xs) ls ne = case f x of
    Nothing -> SomeFiltered (mapMaybe f xs <> ls)
    Just b -> go xs (b : ls) (NE.cons b ne)

applyTableAlteration :: (MonadEffect (Throw SemanticErrorContext) m) =>
  Semantic.Schema ->
  Semantic.Table ->
  AST.Alteration ->
  AST.StatementContext ->
  m Semantic.Table
applyTableAlteration schema table (AST.AddConstraint (
                              AST.NamedConstraint const (AST.TablePrimaryKey names))
                          ) ctx' = do
  let columnIds = Semantic.getColumnId table <$> names
  case mapMaybeNonEmpty (findColumn schema) columnIds of
    SomeFiltered colsFound -> throwSignal $ (SemanticErrorContext (ColumnNameMissmatch "1" table names colsFound) ctx')
    NoneFiltered colsFound -> do
      let semanticConst = AST.NamedConstraint const (Semantic.PrimaryKey $ view id <$> colsFound)
      return $ table & constraints %~ (<> [semanticConst])
applyTableAlteration schema table (AST.AddConstraint (
                              AST.NamedConstraint const (AST.TableUnique names))
                          ) ctx' = do
  let columnIds = Semantic.getColumnId table <$> names
  case mapMaybeNonEmpty (findColumn schema) columnIds of
    SomeFiltered colsFound -> throwSignal $ (SemanticErrorContext (ColumnNameMissmatch "2" table names colsFound) ctx')
    NoneFiltered colsFound -> do
      let semanticConst = AST.NamedConstraint const (Semantic.Unique $ view id <$> colsFound)
      return $ table & constraints %~ (<> [semanticConst])
applyTableAlteration schema fromTable (AST.AddConstraint (
                              AST.NamedConstraint const (
                                  astConst@(AST.TableForeignKey fromCols toTableName toCols))
                              )
                          ) ctx' = do

  let tabId = tableNameToTableId toTableName
  let mTable = findTable schema tabId
  toTable <- case mTable of
     Nothing -> throwSignal (SemanticErrorContext (TableDoesNotExists toTableName) ctx')
     Just semanticTable -> return semanticTable
  let fromColumnIds = Semantic.getColumnId fromTable <$> fromCols
  let toColumnIds = Semantic.getColumnId toTable <$> toCols
  case mapMaybeNonEmpty (findColumn schema) fromColumnIds of
    SomeFiltered fromColsFound -> throwSignal $ SemanticErrorContext (TableAlternationError astConst fromTable fromColsFound fromCols) ctx'
    NoneFiltered fromColsFound ->
      case mapMaybeNonEmpty (findColumn schema) toColumnIds of
        SomeFiltered toColsFound -> throwSignal $ SemanticErrorContext (ColumnNameMissmatch "4" toTable toCols toColsFound) ctx'
        NoneFiltered toColsFound -> do
          let semanticConst = AST.NamedConstraint const (
                Semantic.ForeignKey (view id <$> fromColsFound) (toTable ^. id) (view id <$> toColsFound)
                )
          return $ fromTable & constraints %~ (<> [semanticConst])
applyTableAlteration _ table (AST.AlterOwner _) _ = return table
applyTableAlteration _schema table (AST.AlterColumn col colAlt) ctx' =
    case getColumnIds _schema (table ^. id) (return col) of
        SomeFiltered _ -> throwSignal (SemanticErrorContext (ColumnNameMissmatch "5" table (col :| []) []) ctx')
        NoneFiltered (colId :| _) -> case colAlt of
            AST.SetDefault exp ->
                return $ table & constraints %~ replaceOrAdd colId exp
    where
    replaceOrAdd colId exp [] = [AST.NamedConstraint Nothing (Semantic.Default colId exp)]
    replaceOrAdd colId exp (AST.NamedConstraint _ (Semantic.Default colId' _) : rest)
        | colId == colId' =
            AST.NamedConstraint Nothing (Semantic.Default colId exp) : rest
    replaceOrAdd colId exp (c : cs) = c : replaceOrAdd colId exp cs
applyTableAlteration _ t (AST.RowLevelSecurity _) _ = return t
applyTableAlteration _ t a@(AST.UnknownAlteration _) sp = throwSignal $ SemanticErrorContext (UnknownAlteration (t ^. id) a) sp


updateTable ::
  Semantic.Schema ->
  Semantic.Table ->
  Semantic.Schema
updateTable schema table =
  schema & tables .~ (table : filter (/= table) (schema ^. tables))

-- | Adds new columns to the schema and converts all column constraints
-- | to table level constraints
addColumns ::
  (MonadEffect (Throw SemanticErrorContext) m) =>
  Semantic.Schema ->
  Semantic.TableId ->
  AST.StatementContext ->
  [AST.CreateTableColumn] ->
  m ([Semantic.Column], [AST.NamedConstraint Semantic.TableConstraint])
addColumns schema tableId ctx' =
  foldM (\(cols, consts) (AST.CreateTableColumn _name _type _consts) -> do
    (newCol, newConsts) <- addColumn schema tableId _name _type _consts ctx'
    return (cols ++ [newCol], consts <> newConsts)
    ) mempty

-- | Adds a new column to the schema and converts column constraints
-- | to table level constraints
addColumn :: (MonadEffect (Throw SemanticErrorContext) m) =>
  Semantic.Schema ->
  Semantic.TableId ->
  Text ->
  AST.SqlType ->
  [AST.NamedConstraint AST.ColumnConstraint] ->
  AST.StatementContext ->
  m (Semantic.Column, [AST.NamedConstraint Semantic.TableConstraint])
addColumn schema tableId _name _sqlType _constraints ctx' =
  let columnId = Semantic.ColumnId tableId _name
      semType = Semantic.astTypeToSqlType schema _sqlType
      column = Semantic.Column columnId semType
  in case findColumn schema columnId of
     Just _ -> throwSignal (SemanticErrorContext (ColumnAlreadyExists _name semType _constraints) ctx')
     Nothing -> return (column, convertToTableConstraints columnId _sqlType _constraints)

-- | Converts all column constraints to table constraints.
convertToTableConstraints ::
  Semantic.ColumnId ->
  AST.SqlType ->
  [AST.NamedConstraint AST.ColumnConstraint] ->
  [AST.NamedConstraint Semantic.TableConstraint]
convertToTableConstraints columnId sqlType consts =
    let transform = fmap (convertToTableConstraint columnId)
        consts' = transform consts
        (Semantic.ColumnId (Semantic.TableId _ tabName) colName) = columnId
        seqName = tabName <> "_" <> colName <> "_seq"
        nextValDefault = AST.NamedConstraint Nothing (AST.Default
          (AST.Application (AST.FunctionName Nothing "nextval") [AST.StringLiteral seqName]))
        specialCases =
          [ -- if we do not have not null constraint, add null constraint
            (\constraints ->  if any (\c -> isNotNull c || isPrimaryKey c) constraints
              then constraints
              else constraints <> transform [AST.NamedConstraint Nothing AST.Null])

          -- if type is serial, add primary key and not null constraints
          , (\constraints -> if sqlType `elem` [AST.Serial, AST.BigSerial, AST.SmallSerial]
              then constraints <> transform
                [ AST.NamedConstraint Nothing AST.NotNull
                , nextValDefault ]
              else constraints)
          ]
        additionalConstraints = foldr (\specialCaseHandler constraints -> specialCaseHandler constraints) consts' specialCases
    in additionalConstraints

-- | Defines how are column constraints convereted to table level constraints
convertToTableConstraint ::
  Semantic.ColumnId -> -- context
  AST.NamedConstraint AST.ColumnConstraint ->
  AST.NamedConstraint Semantic.TableConstraint
convertToTableConstraint columnId (AST.NamedConstraint cname AST.NotNull) =
  AST.NamedConstraint cname (Semantic.NotNull columnId)
convertToTableConstraint columnId (AST.NamedConstraint cname AST.Null) =
  AST.NamedConstraint cname (Semantic.Null columnId)
convertToTableConstraint columnId (AST.NamedConstraint cname AST.PrimaryKey) =
  AST.NamedConstraint cname (Semantic.PrimaryKey (pure columnId))
convertToTableConstraint columnId (AST.NamedConstraint cname AST.Unique) =
  AST.NamedConstraint cname (Semantic.Unique (pure columnId))
convertToTableConstraint columnId (AST.NamedConstraint cname (AST.Default val)) =
  AST.NamedConstraint cname (Semantic.Default columnId val)
convertToTableConstraint columnId (AST.NamedConstraint cname (AST.References tableName fs)) =
  case fs of
    Nothing -> error "Can't use REFERENCES on a column without specifying the field (primary key default not yet supported)"
    Just fs ->
      AST.NamedConstraint cname
        (Semantic.ForeignKey (return columnId) tabId (return (Semantic.ColumnId tabId fs)))
  where
  tabId = tableNameToTableId tableName

unsafeMaybeFilteredToNonEmpty :: MaybeFiltered a -> NonEmpty a
unsafeMaybeFilteredToNonEmpty (NoneFiltered a) = a
unsafeMaybeFilteredToNonEmpty _ = error "unsafeMaybeFilteredToNonEmpty: unexpected filtered list"

tableASTToSemantic ::
  Semantic.Schema ->
  Semantic.TableId ->
  AST.NamedConstraint AST.TableConstraint ->
  AST.NamedConstraint Semantic.TableConstraint
tableASTToSemantic schema' tableId (AST.NamedConstraint cname (AST.TablePrimaryKey cols)) =
  -- @TODO handle lookup failure
  AST.NamedConstraint cname
   (Semantic.PrimaryKey $ unsafeMaybeFilteredToNonEmpty $ getColumnIds schema' tableId cols)
tableASTToSemantic schema' tableId (AST.NamedConstraint cname (AST.TableUnique cols)) =
  -- @TODO handle lookup failure
  AST.NamedConstraint cname
   (Semantic.Unique $ unsafeMaybeFilteredToNonEmpty $ getColumnIds schema' tableId cols)
tableASTToSemantic schema' tableId (AST.NamedConstraint cname (AST.TableForeignKey fromCols tableName toCols)) =
  -- @TODO handle lookup failure
  let tabId = tableNameToTableId tableName
      fromCols' = unsafeMaybeFilteredToNonEmpty $ getColumnIds schema' tableId fromCols
      toCols' = unsafeMaybeFilteredToNonEmpty $ getColumnIds schema' tabId toCols
  in AST.NamedConstraint cname
       (Semantic.ForeignKey fromCols' tabId toCols')


getTable' :: Semantic.Schema -> Text -> Text -> Maybe Semantic.Table
getTable' schema schema' tableName =
  let tableId = Semantic.TableId schema' tableName
  in findTable schema tableId


getColumnIds :: Semantic.Schema -> Semantic.TableId -> NonEmpty Text -> MaybeFiltered Semantic.ColumnId
getColumnIds schema' table' cols = view id <$> mapMaybeNonEmpty (getColumn' schema' table') cols

getColumn' :: Semantic.Schema -> Semantic.TableId -> Text -> Maybe Semantic.Column
getColumn' schema table' columnName =
  let columnId = Semantic.ColumnId table' columnName
  in findColumn schema columnId
