module Data.Schema.Parser.View where

import Internal.Interlude
import Data.Schema.Parser
import Data.Schema.Parser.Utils
import Data.Schema.Types.AST
import Text.Megaparsec (try, SourcePos)

createViewStatement :: Parser (CreateView, SourcePos)
createViewStatement =
    try (keyword_ "create"
    *> optional (keyword_ "or " *> keyword_ "replace")
    *> keyword_ "view")
    *> do
        tableIdent <- tableIdentifier
        colNames <- optional (tupled1 identifier)
        (query, sourcePos) <- keyword_ "as" *> select
        return (CreateView tableIdent colNames query, sourcePos)
