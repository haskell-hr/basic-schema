module Data.Schema.Parser.Utils where

import Internal.Interlude hiding (many, some, All)
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Data.Text as T
import qualified Text.Megaparsec.Char.Lexer as L
import Data.Scientific
import qualified Data.Set as Set
import Data.Set (Set)

type Parser = Parsec Void Text

someNonEmpty :: MonadPlus f => f a -> f (NonEmpty a)
someNonEmpty = fmap (\case (x : xs) -> x :| xs; [] -> error "some returned an empty list") .  some

commaSeparatedNonEmpty :: Parser a -> Parser (NonEmpty a)
commaSeparatedNonEmpty =
    fmap (\case (x : xs) -> x :| xs; [] -> error "sepBy1 returned an empty list")
    . commaSeparated

commaSeparated :: Parser a -> Parser [a]
commaSeparated =
    flip sepBy comma

whitespace :: Parser ()
whitespace = L.space
    (void spaceChar)
    (L.skipLineComment "--")
    (L.skipBlockComment "/*" "*/")

---------
-- Tokens
---------

word :: Parser Text
word = toS <$> some (alphaNumChar <|> char '_') <* whitespace

lparen, rparen, comma, singleQuote, doubleQuote, semicolon, lbracket, rbracket, lbrace, rbrace, dot
    :: Parser Char
lparen = char '(' <* whitespace
rparen = char ')' <* whitespace
lbrace = char '{' <* whitespace
rbrace = char '}' <* whitespace
lbracket = char '[' <* whitespace
rbracket = char ']' <* whitespace
comma = char ',' <* whitespace
singleQuote = char '\'' <* whitespace
doubleQuote = char '\"' <* whitespace
semicolon = char ';' <* whitespace
dot = char '.' <* whitespace

stringLit :: Parser Text
stringLit = toS <$>
    ((char '\'' >> manyTill L.charLiteral (char '\''))
    <|> (do
        tag <- char '$' *> many (noneOf ("$" :: String)) <* char '$'
        let tagParser = try (char '$' *> string (toS tag) <* char '$' <?> "end of dollar escaped string literal" <> if null tag then "" else " tagged with $" <> tag <> "$")
        manyTill anySingle tagParser
        )
    )
    <* whitespace
    <?> "string literal"

numberLit :: Parser Scientific
numberLit = L.scientific <* whitespace
    <?> "number literal"

integerLiteral :: Parser Integer
integerLiteral = L.decimal <* whitespace
    <?> "integer literal"

operator :: Parser Text
operator = toS <$> some (symbolChar <|> oneOf [':', '*']) <* whitespace
    <?> "operator"

keySyntax :: Parser Text -> Text -> Parser Text
keySyntax p w = try (p >>= \w' -> guard (T.toLower w == T.toLower w') >> return w')

keyword :: Text -> Parser Text
keyword w = keySyntax word w
    <?> toS ("keyword '" <> w <> "'")
keyword_ :: Text -> Parser ()
keyword_ w = void $ keyword w

keysymbol :: Text -> Parser Text
keysymbol w = string' w <* whitespace
    <?> toS ("key symbol '" <> w <> "'")

keysymbol_ :: Text -> Parser ()
keysymbol_ w = void $ keysymbol w

quotedIdent :: Parser Text
quotedIdent = toS <$> (char '\"' >> manyTill L.charLiteral (char '\"')) <* whitespace
    <?> "quoted identifier"

keywordList :: Set Text
keywordList =
    Set.fromList ["alias", "allocate", "analyse", "analyze", "and", "are", "array", "as", "authorization", "binary", "blob", "both", "call", "cascaded", "case", "cast", "ceil", "ceiling", "check", "clob", "collate", "collect", "column", "completion", "condition", "connect", "constraint", "corr", "corresponding", "covar_pop", "covar_samp", "create", "cross", "cube", "cume_dist", "current", "current_date", "current_default_transform_group", "current_path", "current_role", "current_time", "current_timestamp", "current_transform_group_for_type", "current_user", "date", "default", "dense_rank", "deref", "describe", "destroy", "destructor", "deterministic", "dictionary", "disconnect", "distinct", "do", "dynamic", "element", "else", "end", "end", "every", "except", "exec", "exp", "false", "filter", "floor", "for", "foreign", "free", "freeze", "from", "full", "fusion", "get", "grant", "group", "grouping", "having", "host", "identity", "ignore", "ilike", "in", "indicator", "initialize", "inner", "intersect", "intersection", "into", "is", "isnull", "iterate", "join", "lateral", "leading", "left", "less", "like", "limit", "ln", "localtime", "localtimestamp", "member", "merge", "modifies", "modify", "module", "multiset", "natural", "nclob", "new", "normalize", "not", "notnull", "null", "off", "offset", "old", "on", "only", "open", "operation", "or", "order", "outer", "over", "parameter", "parameters", "partition", "percentile_cont", "percentile_disc", "percent_rank", "postfix", "power", "prefix", "preorder", "primary", "range", "rank", "reads", "recursive", "ref", "references", "referencing", "regr_avgx", "regr_avgy", "regr_count", "regr_intercept", "regr_r2", "regr_slope", "regr_sxx", "regr_sxy", "regr_syy", "result", "return", "right", "rollup", "row_number", "scope", "search", "select", "session_user", "some", "specific", "specifictype", "sql", "sqlcode", "sqlerror", "sqlexception", "sqlstate", "sqlwarning", "sqrt", "static", "stddev_pop", "stddev_samp", "submultiset", "system_user", "table", "tablesample", "terminate", "than", "then", "timezone_hour", "timezone_minute", "to", "trailing", "translation", "true", "uescape", "union", "unique", "unnest", "user", "using", "value", "variable", "var_pop", "var_samp", "verbose", "when", "whenever", "where", "width_bucket", "window", "within"]

keywordsAllowedAsNames :: Set Text
keywordsAllowedAsNames =
    Set.fromList ["abort_p", "absolute_p", "access", "action", "add_p", "admin", "after", "aggregate", "also", "alter", "always", "assertion", "assignment", "at", "attach", "attribute", "backward", "before", "begin_p", "by", "cache", "call", "called", "cascade", "cascaded", "catalog_p", "chain", "characteristics", "checkpoint", "class", "close", "cluster", "columns", "comment", "comments", "commit", "committed", "configuration", "conflict", "connection", "constraints", "content_p", "continue_p", "conversion_p", "copy", "cost", "csv", "cube", "current_p", "cursor", "cycle", "data_p", "database", "day_p", "deallocate", "declare", "defaults", "deferred", "definer", "delete_p", "delimiter", "delimiters", "depends", "detach", "dictionary", "disable_p", "discard", "document_p", "domain_p", "double_p", "drop", "each", "enable_p", "encoding", "encrypted", "enum_p", "escape", "event", "exclude", "excluding", "exclusive", "execute", "explain", "extension", "external", "family", "filter", "first_p", "following", "force", "forward", "function", "functions", "generated", "global", "granted", "groups", "handler", "header_p", "hold", "hour_p", "identity_p", "if_p", "immediate", "immutable", "implicit_p", "import_p", "include", "including", "increment", "index", "indexes", "inherit", "inherits", "inline_p", "input_p", "insensitive", "insert", "instead", "invoker", "isolation", "key", "label", "language", "large_p", "last_p", "leakproof", "level", "listen", "load", "local", "location", "lock_p", "locked", "logged", "mapping", "match", "materialized", "maxvalue", "method", "minute_p", "minvalue", "mode", "month_p", "move", "name_p", "names", "new", "next", "no", "nothing", "notify", "nowait", "nulls_p", "object_p", "of", "off", "oids", "old", "operator", "option", "options", "ordinality", "others", "over", "overriding", "owned", "owner", "parallel", "parser", "partial", "partition", "passing", "password", "plans", "policy", "preceding", "prepare", "prepared", "preserve", "prior", "privileges", "procedural", "procedure", "procedures", "program", "publication", "quote", "range", "read", "reassign", "recheck", "recursive", "ref", "referencing", "refresh", "reindex", "relative_p", "release", "rename", "repeatable", "replace", "replica", "reset", "restart", "restrict", "returns", "revoke", "role", "rollback", "rollup", "routine", "routines", "rows", "rule", "savepoint", "schema", "schemas", "scroll", "search", "second_p", "security", "sequence", "sequences", "serializable", "server", "session", "set", "sets", "share", "show", "simple", "skip", "snapshot", "sql_p", "stable", "standalone_p", "start", "statement", "statistics", "stdin", "stdout", "storage", "strict_p", "strip_p", "subscription", "sysid", "system_p", "tables", "tablespace", "temp", "template", "temporary", "text_p", "ties", "transaction", "transform", "trigger", "truncate", "trusted", "type_p", "types_p", "unbounded", "uncommitted", "unencrypted", "unknown", "unlisten", "unlogged", "until", "update", "vacuum", "valid", "validate", "validator", "value_p", "varying", "version_p", "view", "views", "volatile", "whitespace_p", "within", "without", "work", "wrapper", "write", "xml_p", "year_p", "yes_p", "zone"]
    <> Set.fromList ["between", "bigint", "bit", "boolean_p", "char_p", "character", "coalesce", "dec", "decimal_p", "exists", "extract", "float_p", "greatest", "grouping", "inout", "int_p", "integer", "interval", "least", "national", "nchar", "none", "nullif", "numeric", "out_p", "overlay", "position", "precision", "real", "row", "setof", "smallint", "substring", "time", "timestamp", "treat", "trim", "values", "varchar", "xmlattributes", "xmlconcat", "xmlelement", "xmlexists", "xmlforest", "xmlnamespaces", "xmlparse", "xmlpi", "xmlroot", "xmlserialize", "xmltable"]


notKeyword :: Text -> Bool
notKeyword (T.toLower -> kv) = Set.member kv keywordsAllowedAsNames || not (Set.member kv keywordList)

identifier :: Parser Text
identifier = try quotedIdent <|> try (do { w <- word; guard (notKeyword w); return w })
