{-# LANGUAGE ScopedTypeVariables #-}
module Data.Schema.Parser.Statement where

import Internal.Interlude
import Data.Schema.Types.AST
import Data.Schema.Parser
import Data.Schema.Parser.View
import Data.Schema.Parser.Utils
import Text.Megaparsec

statement :: Text -> Parser StatementContext
statement raw = do
    ofs <- getOffset
    withContext raw ofs CreateTableStatement <$> createTable
        <|> withContext raw ofs AlterTableStatement <$> alterTable
        <|> withContext raw ofs CreateTypeStatement <$> createTypeStatement
        <|> withContext raw ofs SetStatement <$> setStatement
        <|> withContext raw ofs SelectStatement <$> select
        <|> withContext raw ofs CreateViewStatement <$> createViewStatement
        <|> withContext raw ofs CreateSequence <$> createSequence
        <|> withContext raw ofs UnknownStatement <$> unknownStatement
