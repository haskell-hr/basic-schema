[ SetStatement "statement_timeout = 0.0 ;"
, SetStatement "lock_timeout = 0.0 ;"
, SetStatement "idle_in_transaction_session_timeout = 0.0 ;"
, SetStatement "client_encoding = UTF8 ;"
, SetStatement "standard_conforming_strings = on ;"
, SetStatement "check_function_bodies = false ;"
, SetStatement "client_min_messages = warning ;"
, SetStatement "row_security = off ;"
, UnknownStatement
    "CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog ;"
, UnknownStatement
    "COMMENT ON EXTENSION plpgsql IS PL/pgSQL procedural language ;"
, SetStatement "search_path = public , pg_catalog ;"
, CreateTypeStatement
    CreateType
      { _typeName =
          TableName { _schema = Nothing , _tableName = "operation_type" }
      , _customType = Enumerated [ "insert" , "delete" , "update" ]
      }
, UnknownStatement "ALTER TYPE operation_type OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION apply_all_diffs ( ) RETURNS void LANGUAGE plpgsql AS      DECLARE         d diff;         q text;     BEGIN         FOR d IN (SELECT * FROM diff)         LOOP             q = apply_diff(d);             RAISE NOTICE 'Applying: %', q;             EXECUTE q;         END LOOP;     END;  ;"
, UnknownStatement
    "ALTER FUNCTION public . apply_all_diffs ( ) OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION apply_diff ( id integer , table_name text , operation text , payload jsonb ) RETURNS text LANGUAGE plpgsql AS      BEGIN         IF operation = 'INSERT' THEN             RETURN ('INSERT INTO ' || table_name || col_names(payload) || ' VALUES ' || col_values(payload));         ELSIF operation = 'DELETE' THEN             RETURN ('DELETE FROM ' || table_name || ' WHERE ' || array_to_string(key_equal_value(payload), ' and '));         ELSE             RETURN                 ('UPDATE ' || table_name ||                 ' SET (' || array_to_string(key_equal_value(payload), ', ') || ') WHERE ' ||                 array_to_string(key_equal_value(payload), ' and '));         END IF;     END;  ;"
, UnknownStatement
    "ALTER FUNCTION public . apply_diff ( id integer , table_name text , operation text , payload jsonb ) OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION apply_diff ( id integer , table_name text , operation text , old_row jsonb , payload jsonb ) RETURNS text LANGUAGE plpgsql AS      BEGIN         IF operation = 'INSERT' THEN             RETURN ('INSERT INTO ' || table_name || col_names(payload) || ' VALUES ' || col_values(payload));         ELSIF operation = 'DELETE' THEN             RETURN ('DELETE FROM ' || table_name || ' WHERE ' || array_to_string(key_equal_value(true, old_row), ' and '));         ELSE             RETURN                 ('UPDATE ' || table_name ||                 ' SET ' || array_to_string(key_equal_value(false, payload), ', ') || ' WHERE ' ||                 array_to_string(key_equal_value(true, old_row), ' and '));         END IF;     END;  ;"
, UnknownStatement
    "ALTER FUNCTION public . apply_diff ( id integer , table_name text , operation text , old_row jsonb , payload jsonb ) OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION col_names ( payload jsonb ) RETURNS text LANGUAGE plpgsql AS      DECLARE         _res text;     BEGIN         SELECT '(' || array_to_string(array_agg(quote_ident(k)), ',') || ')'             INTO _res             FROM jsonb_object_keys(payload) as k;         RETURN _res;     END;  ;"
, UnknownStatement
    "ALTER FUNCTION public . col_names ( payload jsonb ) OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION col_values ( payload jsonb ) RETURNS text LANGUAGE plpgsql AS      DECLARE         _res text;     BEGIN         SELECT '(' || array_to_string(array_agg(quote(payload ->> k)), ',') || ')'             INTO _res             FROM jsonb_object_keys(payload) as k;         RETURN _res;     END;  ;"
, UnknownStatement
    "ALTER FUNCTION public . col_values ( payload jsonb ) OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION diff_trigger ( ) RETURNS trigger LANGUAGE plpgsql AS      BEGIN         INSERT INTO diff(table_name, operation, old_row, payload) VALUES (             TG_TABLE_NAME,             TG_OP,             CASE WHEN TG_OP = 'INSERT' THEN null                 ELSE row_to_json(OLD)             END,             CASE WHEN TG_OP = 'DELETE' THEN null                 ELSE row_to_json(NEW)             END);         RETURN NEW;     END;  ;"
, UnknownStatement
    "ALTER FUNCTION public . diff_trigger ( ) OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION exec_string ( q text ) RETURNS void LANGUAGE plpgsql AS      BEGIN         EXECUTE IMMEDIATE q;     END;  ;"
, UnknownStatement
    "ALTER FUNCTION public . exec_string ( q text ) OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION key_equal_value ( payload jsonb ) RETURNS text [ ] LANGUAGE plpgsql AS      DECLARE         _res text[];     BEGIN         SELECT array_agg(quote_ident(k) || '=' || quote_literal(payload ->> k))             INTO _res             FROM jsonb_object_keys(payload) as k;         RETURN _res;     END;  ;"
, UnknownStatement
    "ALTER FUNCTION public . key_equal_value ( payload jsonb ) OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION key_equal_value ( is_condition boolean , payload jsonb ) RETURNS text [ ] LANGUAGE plpgsql AS      DECLARE         _res text[];     BEGIN         SELECT array_agg(             CASE                 WHEN payload ->> k is null and is_condition THEN quote_ident(k) || ' is null'                 ELSE quote_ident(k) || '=' || quote(payload ->> k)             END)             INTO _res             FROM jsonb_object_keys(payload) as k;         RETURN _res;     END;  ;"
, UnknownStatement
    "ALTER FUNCTION public . key_equal_value ( is_condition boolean , payload jsonb ) OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION notify_order_update ( ) RETURNS trigger LANGUAGE plpgsql AS \n    DECLARE\n        stat_id int;\n    BEGIN\n        INSERT INTO order_status_event(\"dok_fazaID\", dok, broj, faza, datum, sat)\n            VALUES (NEW.\"dok_fazaID\", NEW.dok, NEW.broj, NEW.faza, NEW.datum, NEW.sat)\n            RETURNING event_id INTO stat_id;\n        PERFORM pg_notify('order', 'v1 ' || stat_id || ' ' || NEW.faza);\n        RETURN NEW;\n    END;\n ;"
, UnknownStatement
    "ALTER FUNCTION public . notify_order_update ( ) OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION quote ( val anyelement ) RETURNS text LANGUAGE plpgsql AS      BEGIN         IF val is null THEN             RETURN 'null';         ELSE             RETURN quote_literal(val);         END IF;     END;  ;"
, UnknownStatement
    "ALTER FUNCTION public . quote ( val anyelement ) OWNER TO postgres ;"
, UnknownStatement
    "CREATE FUNCTION to_lit ( a anyelement ) RETURNS text LANGUAGE plpgsql AS      BEGIN         RETURN             CASE WHEN pg_typeof(a) = 'text' THEN quote_literal(a)                  ELSE (a :: text)             END;     END;  ;"
, UnknownStatement
    "ALTER FUNCTION public . to_lit ( a anyelement ) OWNER TO postgres ;"
, SetStatement "default_tablespace =  ;"
, SetStatement "default_with_oids = false ;"
, CreateTableStatement
    CreateTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "applied_diffs" }
      , _columns =
          [ CreateTableColumn
              { _name = "diff_id"
              , _sqlType = SqlInteger
              , _constraints =
                  [ NamedConstraint { _name = Nothing , _constraint = NotNull } ]
              }
          , CreateTableColumn
              { _name = "applied_on"
              , _sqlType = Timestamp
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default
                            (Application
                                FunctionName { _schema = Nothing , _functionName = "now" } [])
                      }
                  , NamedConstraint { _name = Nothing , _constraint = NotNull }
                  ]
              }
          ]
      , _tableConstraints = []
      }
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "applied_diffs" }
      , _alteration = AlterOwner "postgres"
      }
, CreateTableStatement
    CreateTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "artikli" }
      , _columns =
          [ CreateTableColumn
              { _name = "artikliID"
              , _sqlType = BigInt
              , _constraints =
                  [ NamedConstraint { _name = Nothing , _constraint = NotNull } ]
              }
          , CreateTableColumn
              { _name = "RECNO"
              , _sqlType = BigInt
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing , _constraint = Default (NumberLiteral 0.0) }
                  , NamedConstraint { _name = Nothing , _constraint = NotNull }
                  ]
              }
          , CreateTableColumn
              { _name = "SIFRA"
              , _sqlType = CharacterVaryingN 20
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "NAZIV"
              , _sqlType = CharacterVaryingN 90
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "STANJE"
              , _sqlType = NumericPS 14 2
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing , _constraint = Default (NumberLiteral 0.0) }
                  ]
              }
          , CreateTableColumn
              { _name = "C1"
              , _sqlType = NumericPS 14 2
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing , _constraint = Default (NumberLiteral 0.0) }
                  ]
              }
          , CreateTableColumn
              { _name = "C2"
              , _sqlType = NumericPS 14 2
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing , _constraint = Default (NumberLiteral 0.0) }
                  ]
              }
          , CreateTableColumn
              { _name = "C3"
              , _sqlType = NumericPS 14 2
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing , _constraint = Default (NumberLiteral 0.0) }
                  ]
              }
          , CreateTableColumn
              { _name = "C4"
              , _sqlType = NumericPS 14 2
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing , _constraint = Default (NumberLiteral 0.0) }
                  ]
              }
          , CreateTableColumn
              { _name = "MPC"
              , _sqlType = NumericPS 14 2
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing , _constraint = Default (NumberLiteral 0.0) }
                  ]
              }
          , CreateTableColumn
              { _name = "VPC"
              , _sqlType = NumericPS 14 2
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing , _constraint = Default (NumberLiteral 0.0) }
                  ]
              }
          , CreateTableColumn
              { _name = "SKLADISTE"
              , _sqlType = CharacterVaryingN 5
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "LOKACIJA"
              , _sqlType = CharacterVaryingN 30
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "SKUPINA"
              , _sqlType = CharacterVaryingN 15
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "NAZIV_SK"
              , _sqlType = CharacterVaryingN 50
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "GRUPA"
              , _sqlType = CharacterVaryingN 15
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "NAZIV_GR"
              , _sqlType = CharacterVaryingN 50
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "P_GRUPA"
              , _sqlType = CharacterVaryingN 15
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "NAZIV_PG"
              , _sqlType = CharacterVaryingN 50
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "BK"
              , _sqlType = CharacterVaryingN 13
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "KB"
              , _sqlType = CharacterVaryingN 20
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "TIP"
              , _sqlType = CharacterVaryingN 15
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral " ") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "P_SKUPINA"
              , _sqlType = CharacterVaryingN 15
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "NAZIV_PS"
              , _sqlType = CharacterVaryingN 50
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "BRAND"
              , _sqlType = CharacterVaryingN 15
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "MJERA"
              , _sqlType = CharacterVaryingN 10
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "POREZ"
              , _sqlType = CharacterVaryingN 3
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "BRAND_NZ"
              , _sqlType = CharacterVaryingN 50
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "WEB_PROD"
              , _sqlType = CharacterVaryingN 1
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "OPIS" , _sqlType = SqlText , _constraints = [] }
          ]
      , _tableConstraints = []
      }
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "artikli" }
      , _alteration = AlterOwner "postgres"
      }
, CreateTableStatement
    CreateTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "atributi" }
      , _columns =
          [ CreateTableColumn
              { _name = "atributiID"
              , _sqlType = BigInt
              , _constraints =
                  [ NamedConstraint { _name = Nothing , _constraint = NotNull } ]
              }
          , CreateTableColumn
              { _name = "ArtikliID"
              , _sqlType = BigInt
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing , _constraint = Default (NumberLiteral 0.0) }
                  ]
              }
          , CreateTableColumn
              { _name = "KRATICA"
              , _sqlType = CharacterVaryingN 25
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "OPIS"
              , _sqlType = CharacterVaryingN 30
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "VRIJEDNOST" , _sqlType = SqlText , _constraints = [] }
          ]
      , _tableConstraints = []
      }
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "atributi" }
      , _alteration = AlterOwner "postgres"
      }
, CreateTableStatement
    CreateTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "dok_faza" }
      , _columns =
          [ CreateTableColumn
              { _name = "dok_fazaID"
              , _sqlType = BigInt
              , _constraints =
                  [ NamedConstraint { _name = Nothing , _constraint = NotNull } ]
              }
          , CreateTableColumn
              { _name = "sklad_id"
              , _sqlType = CharacterVaryingN 5
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "dok"
              , _sqlType = CharacterVaryingN 3
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "broj"
              , _sqlType = SqlInteger
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing , _constraint = Default (NumberLiteral 0.0) }
                  ]
              }
          , CreateTableColumn
              { _name = "faza"
              , _sqlType = CharacterVaryingN 5
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "datum" , _sqlType = Date , _constraints = [] }
          , CreateTableColumn
              { _name = "sat"
              , _sqlType = Time
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint = Default (TypeCast (StringLiteral "00:00:00") Time)
                      }
                  ]
              }
          ]
      , _tableConstraints = []
      }
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "dok_faza" }
      , _alteration = AlterOwner "postgres"
      }
, CreateTableStatement
    CreateTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "log_rada" }
      , _columns =
          [ CreateTableColumn
              { _name = "log_radaID"
              , _sqlType = SqlInteger
              , _constraints =
                  [ NamedConstraint { _name = Nothing , _constraint = NotNull } ]
              }
          , CreateTableColumn
              { _name = "DATUM" , _sqlType = Date , _constraints = [] }
          , CreateTableColumn
              { _name = "VRIJEME"
              , _sqlType = Time
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint = Default (TypeCast (StringLiteral "00:00:00") Time)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "OPIS"
              , _sqlType = CharacterVaryingN 30
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "ARTIKL"
              , _sqlType = CharacterVaryingN 20
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "PODATAK"
              , _sqlType = CharacterVaryingN 60
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          ]
      , _tableConstraints = []
      }
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "log_rada" }
      , _alteration = AlterOwner "postgres"
      }
, CreateTableStatement
    CreateTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "order_status_event" }
      , _columns =
          [ CreateTableColumn
              { _name = "event_id"
              , _sqlType = SqlInteger
              , _constraints =
                  [ NamedConstraint { _name = Nothing , _constraint = NotNull } ]
              }
          , CreateTableColumn
              { _name = "dok_fazaID"
              , _sqlType = BigInt
              , _constraints =
                  [ NamedConstraint { _name = Nothing , _constraint = NotNull } ]
              }
          , CreateTableColumn
              { _name = "dok"
              , _sqlType = CharacterVaryingN 3
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "broj"
              , _sqlType = SqlInteger
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing , _constraint = Default (NumberLiteral 0.0) }
                  ]
              }
          , CreateTableColumn
              { _name = "faza"
              , _sqlType = CharacterVaryingN 5
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default (TypeCast (StringLiteral "") CharacterVarying)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "datum" , _sqlType = Date , _constraints = [] }
          , CreateTableColumn
              { _name = "sat"
              , _sqlType = Time
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint = Default (TypeCast (StringLiteral "00:00:00") Time)
                      }
                  ]
              }
          , CreateTableColumn
              { _name = "update_time"
              , _sqlType = Timestamp
              , _constraints =
                  [ NamedConstraint
                      { _name = Nothing
                      , _constraint =
                          Default
                            (Application
                                FunctionName { _schema = Nothing , _functionName = "now" } [])
                      }
                  ]
              }
          ]
      , _tableConstraints = []
      }
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "order_status_event" }
      , _alteration = AlterOwner "postgres"
      }
, CreateSequence
    Sequence
      { _tableName =
          TableName
            { _schema = Nothing
            , _tableName = "order_status_event_event_id_seq"
            }
      }
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName
            { _schema = Nothing
            , _tableName = "order_status_event_event_id_seq"
            }
      , _alteration = AlterOwner "postgres"
      }
, UnknownStatement
    "ALTER SEQUENCE order_status_event_event_id_seq OWNED BY order_status_event . event_id ;"
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "order_status_event" }
      , _alteration =
          AlterColumn
            "event_id"
            (SetDefault
                (Application
                  FunctionName { _schema = Nothing , _functionName = "nextval" }
                  [ TypeCast
                      (StringLiteral "order_status_event_event_id_seq")
                      (Other "regclass")
                  ]))
      }
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "artikli" }
      , _alteration =
          AddConstraint
            NamedConstraint
              { _name = Just "artikli_pkey"
              , _constraint = TablePrimaryKey ("artikliID" :| [])
              }
      }
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "atributi" }
      , _alteration =
          AddConstraint
            NamedConstraint
              { _name = Just "atributi_pkey"
              , _constraint = TablePrimaryKey ("atributiID" :| [])
              }
      }
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "dok_faza" }
      , _alteration =
          AddConstraint
            NamedConstraint
              { _name = Just "dok_faza_pkey"
              , _constraint = TablePrimaryKey ("dok_fazaID" :| [])
              }
      }
, AlterTableStatement
    AlterTable
      { _tableName =
          TableName { _schema = Nothing , _tableName = "log_rada" }
      , _alteration =
          AddConstraint
            NamedConstraint
              { _name = Just "log_rada_pkey"
              , _constraint = TablePrimaryKey ("log_radaID" :| [])
              }
      }
, UnknownStatement
    "CREATE TRIGGER order_update AFTER INSERT OR UPDATE ON dok_faza FOR EACH ROW EXECUTE PROCEDURE notify_order_update ( ) ;"
, UnknownStatement "GRANT ALL ON SCHEMA public TO PUBLIC ;"
]
, Schema
  { _tables =
      [ Table
          { _id =
              TableId { _schemaName = "public" , _tableName = "log_rada" }
          , _columns =
              [ ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "log_rada" }
                  , _columnName = "log_radaID"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "log_rada" }
                  , _columnName = "DATUM"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "log_rada" }
                  , _columnName = "VRIJEME"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "log_rada" }
                  , _columnName = "OPIS"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "log_rada" }
                  , _columnName = "ARTIKL"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "log_rada" }
                  , _columnName = "PODATAK"
                  }
              ]
          , _constraints =
              [ NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      NotNull
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "log_rada" }
                          , _columnName = "log_radaID"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "log_rada" }
                          , _columnName = "DATUM"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "log_rada" }
                          , _columnName = "VRIJEME"
                          }
                        (TypeCast (StringLiteral "00:00:00") Time)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "log_rada" }
                          , _columnName = "VRIJEME"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "log_rada" }
                          , _columnName = "OPIS"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "log_rada" }
                          , _columnName = "OPIS"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "log_rada" }
                          , _columnName = "ARTIKL"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "log_rada" }
                          , _columnName = "ARTIKL"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "log_rada" }
                          , _columnName = "PODATAK"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "log_rada" }
                          , _columnName = "PODATAK"
                          }
                  }
              , NamedConstraint
                  { _name = Just "log_rada_pkey"
                  , _constraint =
                      PrimaryKey
                        (ColumnId
                            { _tableId =
                                TableId { _schemaName = "public" , _tableName = "log_rada" }
                            , _columnName = "log_radaID"
                            } :|
                            [])
                  }
              ]
          }
      , Table
          { _id =
              TableId { _schemaName = "public" , _tableName = "dok_faza" }
          , _columns =
              [ ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "dok_faza" }
                  , _columnName = "dok_fazaID"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "dok_faza" }
                  , _columnName = "sklad_id"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "dok_faza" }
                  , _columnName = "dok"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "dok_faza" }
                  , _columnName = "broj"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "dok_faza" }
                  , _columnName = "faza"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "dok_faza" }
                  , _columnName = "datum"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "dok_faza" }
                  , _columnName = "sat"
                  }
              ]
          , _constraints =
              [ NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      NotNull
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "dok_fazaID"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "sklad_id"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "sklad_id"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "dok"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "dok"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "broj"
                          }
                        (NumberLiteral 0.0)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "broj"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "faza"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "faza"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "datum"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "sat"
                          }
                        (TypeCast (StringLiteral "00:00:00") Time)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "dok_faza" }
                          , _columnName = "sat"
                          }
                  }
              , NamedConstraint
                  { _name = Just "dok_faza_pkey"
                  , _constraint =
                      PrimaryKey
                        (ColumnId
                            { _tableId =
                                TableId { _schemaName = "public" , _tableName = "dok_faza" }
                            , _columnName = "dok_fazaID"
                            } :|
                            [])
                  }
              ]
          }
      , Table
          { _id =
              TableId { _schemaName = "public" , _tableName = "atributi" }
          , _columns =
              [ ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "atributi" }
                  , _columnName = "atributiID"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "atributi" }
                  , _columnName = "ArtikliID"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "atributi" }
                  , _columnName = "KRATICA"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "atributi" }
                  , _columnName = "OPIS"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "atributi" }
                  , _columnName = "VRIJEDNOST"
                  }
              ]
          , _constraints =
              [ NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      NotNull
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "atributi" }
                          , _columnName = "atributiID"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "atributi" }
                          , _columnName = "ArtikliID"
                          }
                        (NumberLiteral 0.0)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "atributi" }
                          , _columnName = "ArtikliID"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "atributi" }
                          , _columnName = "KRATICA"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "atributi" }
                          , _columnName = "KRATICA"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "atributi" }
                          , _columnName = "OPIS"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "atributi" }
                          , _columnName = "OPIS"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "atributi" }
                          , _columnName = "VRIJEDNOST"
                          }
                  }
              , NamedConstraint
                  { _name = Just "atributi_pkey"
                  , _constraint =
                      PrimaryKey
                        (ColumnId
                            { _tableId =
                                TableId { _schemaName = "public" , _tableName = "atributi" }
                            , _columnName = "atributiID"
                            } :|
                            [])
                  }
              ]
          }
      , Table
          { _id = TableId { _schemaName = "public" , _tableName = "artikli" }
          , _columns =
              [ ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "artikliID"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "RECNO"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "SIFRA"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "NAZIV"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "STANJE"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "C1"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "C2"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "C3"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "C4"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "MPC"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "VPC"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "SKLADISTE"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "LOKACIJA"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "SKUPINA"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "NAZIV_SK"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "GRUPA"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "NAZIV_GR"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "P_GRUPA"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "NAZIV_PG"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "BK"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "KB"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "TIP"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "P_SKUPINA"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "NAZIV_PS"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "BRAND"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "MJERA"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "POREZ"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "BRAND_NZ"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "WEB_PROD"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "artikli" }
                  , _columnName = "OPIS"
                  }
              ]
          , _constraints =
              [ NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      NotNull
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "artikliID"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "RECNO"
                          }
                        (NumberLiteral 0.0)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      NotNull
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "RECNO"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "SIFRA"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "SIFRA"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "NAZIV"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "NAZIV"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "STANJE"
                          }
                        (NumberLiteral 0.0)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "STANJE"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "C1"
                          }
                        (NumberLiteral 0.0)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "C1"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "C2"
                          }
                        (NumberLiteral 0.0)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "C2"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "C3"
                          }
                        (NumberLiteral 0.0)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "C3"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "C4"
                          }
                        (NumberLiteral 0.0)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "C4"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "MPC"
                          }
                        (NumberLiteral 0.0)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "MPC"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "VPC"
                          }
                        (NumberLiteral 0.0)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "VPC"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "SKLADISTE"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "SKLADISTE"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "LOKACIJA"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "LOKACIJA"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "SKUPINA"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "SKUPINA"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "NAZIV_SK"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "NAZIV_SK"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "GRUPA"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "GRUPA"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "NAZIV_GR"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "NAZIV_GR"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "P_GRUPA"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "P_GRUPA"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "NAZIV_PG"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "NAZIV_PG"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "BK"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "BK"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "KB"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "KB"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "TIP"
                          }
                        (TypeCast (StringLiteral " ") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "TIP"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "P_SKUPINA"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "P_SKUPINA"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "NAZIV_PS"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "NAZIV_PS"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "BRAND"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "BRAND"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "MJERA"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "MJERA"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "POREZ"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "POREZ"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "BRAND_NZ"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "BRAND_NZ"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "WEB_PROD"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "WEB_PROD"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "artikli" }
                          , _columnName = "OPIS"
                          }
                  }
              , NamedConstraint
                  { _name = Just "artikli_pkey"
                  , _constraint =
                      PrimaryKey
                        (ColumnId
                            { _tableId =
                                TableId { _schemaName = "public" , _tableName = "artikli" }
                            , _columnName = "artikliID"
                            } :|
                            [])
                  }
              ]
          }
      , Table
          { _id =
              TableId
                { _schemaName = "public" , _tableName = "order_status_event" }
          , _columns =
              [ ColumnId
                  { _tableId =
                      TableId
                        { _schemaName = "public" , _tableName = "order_status_event" }
                  , _columnName = "event_id"
                  }
              , ColumnId
                  { _tableId =
                      TableId
                        { _schemaName = "public" , _tableName = "order_status_event" }
                  , _columnName = "dok_fazaID"
                  }
              , ColumnId
                  { _tableId =
                      TableId
                        { _schemaName = "public" , _tableName = "order_status_event" }
                  , _columnName = "dok"
                  }
              , ColumnId
                  { _tableId =
                      TableId
                        { _schemaName = "public" , _tableName = "order_status_event" }
                  , _columnName = "broj"
                  }
              , ColumnId
                  { _tableId =
                      TableId
                        { _schemaName = "public" , _tableName = "order_status_event" }
                  , _columnName = "faza"
                  }
              , ColumnId
                  { _tableId =
                      TableId
                        { _schemaName = "public" , _tableName = "order_status_event" }
                  , _columnName = "datum"
                  }
              , ColumnId
                  { _tableId =
                      TableId
                        { _schemaName = "public" , _tableName = "order_status_event" }
                  , _columnName = "sat"
                  }
              , ColumnId
                  { _tableId =
                      TableId
                        { _schemaName = "public" , _tableName = "order_status_event" }
                  , _columnName = "update_time"
                  }
              ]
          , _constraints =
              [ NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      NotNull
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "event_id"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      NotNull
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "dok_fazaID"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "dok"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "dok"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "broj"
                          }
                        (NumberLiteral 0.0)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "broj"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "faza"
                          }
                        (TypeCast (StringLiteral "") CharacterVarying)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "faza"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "datum"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "sat"
                          }
                        (TypeCast (StringLiteral "00:00:00") Time)
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "sat"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "update_time"
                          }
                        (Application
                            FunctionName { _schema = Nothing , _functionName = "now" } [])
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Null
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "update_time"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId
                                { _schemaName = "public" , _tableName = "order_status_event" }
                          , _columnName = "event_id"
                          }
                        (Application
                            FunctionName { _schema = Nothing , _functionName = "nextval" }
                            [ TypeCast
                                (StringLiteral "order_status_event_event_id_seq")
                                (Other "regclass")
                            ])
                  }
              ]
          }
      , Table
          { _id =
              TableId
                { _schemaName = "public"
                , _tableName = "order_status_event_event_id_seq"
                }
          , _columns = []
          , _constraints = []
          }
      , Table
          { _id =
              TableId { _schemaName = "public" , _tableName = "applied_diffs" }
          , _columns =
              [ ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "applied_diffs" }
                  , _columnName = "diff_id"
                  }
              , ColumnId
                  { _tableId =
                      TableId { _schemaName = "public" , _tableName = "applied_diffs" }
                  , _columnName = "applied_on"
                  }
              ]
          , _constraints =
              [ NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      NotNull
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "applied_diffs" }
                          , _columnName = "diff_id"
                          }
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      Default
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "applied_diffs" }
                          , _columnName = "applied_on"
                          }
                        (Application
                            FunctionName { _schema = Nothing , _functionName = "now" } [])
                  }
              , NamedConstraint
                  { _name = Nothing
                  , _constraint =
                      NotNull
                        ColumnId
                          { _tableId =
                              TableId { _schemaName = "public" , _tableName = "applied_diffs" }
                          , _columnName = "applied_on"
                          }
                  }
              ]
          }
      ]
  , _columns =
      [ Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "applied_diffs" }
                , _columnName = "diff_id"
                }
          , _sqlType = Integer
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "applied_diffs" }
                , _columnName = "applied_on"
                }
          , _sqlType = Timestamp Nothing False
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "artikliID"
                }
          , _sqlType = Bigint
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "RECNO"
                }
          , _sqlType = Bigint
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "SIFRA"
                }
          , _sqlType = Character True (Just 20)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "NAZIV"
                }
          , _sqlType = Character True (Just 90)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "STANJE"
                }
          , _sqlType = Numeric (Just 14) (Just 2)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "C1"
                }
          , _sqlType = Numeric (Just 14) (Just 2)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "C2"
                }
          , _sqlType = Numeric (Just 14) (Just 2)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "C3"
                }
          , _sqlType = Numeric (Just 14) (Just 2)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "C4"
                }
          , _sqlType = Numeric (Just 14) (Just 2)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "MPC"
                }
          , _sqlType = Numeric (Just 14) (Just 2)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "VPC"
                }
          , _sqlType = Numeric (Just 14) (Just 2)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "SKLADISTE"
                }
          , _sqlType = Character True (Just 5)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "LOKACIJA"
                }
          , _sqlType = Character True (Just 30)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "SKUPINA"
                }
          , _sqlType = Character True (Just 15)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "NAZIV_SK"
                }
          , _sqlType = Character True (Just 50)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "GRUPA"
                }
          , _sqlType = Character True (Just 15)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "NAZIV_GR"
                }
          , _sqlType = Character True (Just 50)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "P_GRUPA"
                }
          , _sqlType = Character True (Just 15)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "NAZIV_PG"
                }
          , _sqlType = Character True (Just 50)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "BK"
                }
          , _sqlType = Character True (Just 13)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "KB"
                }
          , _sqlType = Character True (Just 20)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "TIP"
                }
          , _sqlType = Character True (Just 15)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "P_SKUPINA"
                }
          , _sqlType = Character True (Just 15)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "NAZIV_PS"
                }
          , _sqlType = Character True (Just 50)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "BRAND"
                }
          , _sqlType = Character True (Just 15)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "MJERA"
                }
          , _sqlType = Character True (Just 10)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "POREZ"
                }
          , _sqlType = Character True (Just 3)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "BRAND_NZ"
                }
          , _sqlType = Character True (Just 50)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "WEB_PROD"
                }
          , _sqlType = Character True (Just 1)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "artikli" }
                , _columnName = "OPIS"
                }
          , _sqlType = Text
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "atributi" }
                , _columnName = "atributiID"
                }
          , _sqlType = Bigint
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "atributi" }
                , _columnName = "ArtikliID"
                }
          , _sqlType = Bigint
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "atributi" }
                , _columnName = "KRATICA"
                }
          , _sqlType = Character True (Just 25)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "atributi" }
                , _columnName = "OPIS"
                }
          , _sqlType = Character True (Just 30)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "atributi" }
                , _columnName = "VRIJEDNOST"
                }
          , _sqlType = Text
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "dok_faza" }
                , _columnName = "dok_fazaID"
                }
          , _sqlType = Bigint
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "dok_faza" }
                , _columnName = "sklad_id"
                }
          , _sqlType = Character True (Just 5)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "dok_faza" }
                , _columnName = "dok"
                }
          , _sqlType = Character True (Just 3)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "dok_faza" }
                , _columnName = "broj"
                }
          , _sqlType = Integer
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "dok_faza" }
                , _columnName = "faza"
                }
          , _sqlType = Character True (Just 5)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "dok_faza" }
                , _columnName = "datum"
                }
          , _sqlType = Date
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "dok_faza" }
                , _columnName = "sat"
                }
          , _sqlType = Time Nothing False
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "log_rada" }
                , _columnName = "log_radaID"
                }
          , _sqlType = Integer
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "log_rada" }
                , _columnName = "DATUM"
                }
          , _sqlType = Date
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "log_rada" }
                , _columnName = "VRIJEME"
                }
          , _sqlType = Time Nothing False
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "log_rada" }
                , _columnName = "OPIS"
                }
          , _sqlType = Character True (Just 30)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "log_rada" }
                , _columnName = "ARTIKL"
                }
          , _sqlType = Character True (Just 20)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId { _schemaName = "public" , _tableName = "log_rada" }
                , _columnName = "PODATAK"
                }
          , _sqlType = Character True (Just 60)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId
                      { _schemaName = "public" , _tableName = "order_status_event" }
                , _columnName = "event_id"
                }
          , _sqlType = Integer
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId
                      { _schemaName = "public" , _tableName = "order_status_event" }
                , _columnName = "dok_fazaID"
                }
          , _sqlType = Bigint
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId
                      { _schemaName = "public" , _tableName = "order_status_event" }
                , _columnName = "dok"
                }
          , _sqlType = Character True (Just 3)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId
                      { _schemaName = "public" , _tableName = "order_status_event" }
                , _columnName = "broj"
                }
          , _sqlType = Integer
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId
                      { _schemaName = "public" , _tableName = "order_status_event" }
                , _columnName = "faza"
                }
          , _sqlType = Character True (Just 5)
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId
                      { _schemaName = "public" , _tableName = "order_status_event" }
                , _columnName = "datum"
                }
          , _sqlType = Date
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId
                      { _schemaName = "public" , _tableName = "order_status_event" }
                , _columnName = "sat"
                }
          , _sqlType = Time Nothing False
          }
      , Column
          { _id =
              ColumnId
                { _tableId =
                    TableId
                      { _schemaName = "public" , _tableName = "order_status_event" }
                , _columnName = "update_time"
                }
          , _sqlType = Timestamp Nothing False
          }
      ]
  , _types =
      [ ( TableId
            { _schemaName = "public" , _tableName = "operation_type" }
        , Enumerated [ "insert" , "delete" , "update" ]
        )
      ]
  }
