--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: operation_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE operation_type AS ENUM (
    'insert',
    'delete',
    'update'
);


ALTER TYPE operation_type OWNER TO postgres;

--
-- Name: apply_all_diffs(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION apply_all_diffs() RETURNS void
    LANGUAGE plpgsql
    AS $$     DECLARE         d diff;         q text;     BEGIN         FOR d IN (SELECT * FROM diff)         LOOP             q = apply_diff(d);             RAISE NOTICE 'Applying: %', q;             EXECUTE q;         END LOOP;     END; $$;


ALTER FUNCTION public.apply_all_diffs() OWNER TO postgres;

--
-- Name: apply_diff(integer, text, text, jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION apply_diff(id integer, table_name text, operation text, payload jsonb) RETURNS text
    LANGUAGE plpgsql
    AS $$     BEGIN         IF operation = 'INSERT' THEN             RETURN ('INSERT INTO ' || table_name || col_names(payload) || ' VALUES ' || col_values(payload));         ELSIF operation = 'DELETE' THEN             RETURN ('DELETE FROM ' || table_name || ' WHERE ' || array_to_string(key_equal_value(payload), ' and '));         ELSE             RETURN                 ('UPDATE ' || table_name ||                 ' SET (' || array_to_string(key_equal_value(payload), ', ') || ') WHERE ' ||                 array_to_string(key_equal_value(payload), ' and '));         END IF;     END; $$;


ALTER FUNCTION public.apply_diff(id integer, table_name text, operation text, payload jsonb) OWNER TO postgres;

--
-- Name: apply_diff(integer, text, text, jsonb, jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION apply_diff(id integer, table_name text, operation text, old_row jsonb, payload jsonb) RETURNS text
    LANGUAGE plpgsql
    AS $$     BEGIN         IF operation = 'INSERT' THEN             RETURN ('INSERT INTO ' || table_name || col_names(payload) || ' VALUES ' || col_values(payload));         ELSIF operation = 'DELETE' THEN             RETURN ('DELETE FROM ' || table_name || ' WHERE ' || array_to_string(key_equal_value(true, old_row), ' and '));         ELSE             RETURN                 ('UPDATE ' || table_name ||                 ' SET ' || array_to_string(key_equal_value(false, payload), ', ') || ' WHERE ' ||                 array_to_string(key_equal_value(true, old_row), ' and '));         END IF;     END; $$;


ALTER FUNCTION public.apply_diff(id integer, table_name text, operation text, old_row jsonb, payload jsonb) OWNER TO postgres;

--
-- Name: col_names(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION col_names(payload jsonb) RETURNS text
    LANGUAGE plpgsql
    AS $$     DECLARE         _res text;     BEGIN         SELECT '(' || array_to_string(array_agg(quote_ident(k)), ',') || ')'             INTO _res             FROM jsonb_object_keys(payload) as k;         RETURN _res;     END; $$;


ALTER FUNCTION public.col_names(payload jsonb) OWNER TO postgres;

--
-- Name: col_values(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION col_values(payload jsonb) RETURNS text
    LANGUAGE plpgsql
    AS $$     DECLARE         _res text;     BEGIN         SELECT '(' || array_to_string(array_agg(quote(payload ->> k)), ',') || ')'             INTO _res             FROM jsonb_object_keys(payload) as k;         RETURN _res;     END; $$;


ALTER FUNCTION public.col_values(payload jsonb) OWNER TO postgres;

--
-- Name: diff_trigger(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION diff_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$     BEGIN         INSERT INTO diff(table_name, operation, old_row, payload) VALUES (             TG_TABLE_NAME,             TG_OP,             CASE WHEN TG_OP = 'INSERT' THEN null                 ELSE row_to_json(OLD)             END,             CASE WHEN TG_OP = 'DELETE' THEN null                 ELSE row_to_json(NEW)             END);         RETURN NEW;     END; $$;


ALTER FUNCTION public.diff_trigger() OWNER TO postgres;

--
-- Name: exec_string(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION exec_string(q text) RETURNS void
    LANGUAGE plpgsql
    AS $$     BEGIN         EXECUTE IMMEDIATE q;     END; $$;


ALTER FUNCTION public.exec_string(q text) OWNER TO postgres;

--
-- Name: key_equal_value(jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION key_equal_value(payload jsonb) RETURNS text[]
    LANGUAGE plpgsql
    AS $$     DECLARE         _res text[];     BEGIN         SELECT array_agg(quote_ident(k) || '=' || quote_literal(payload ->> k))             INTO _res             FROM jsonb_object_keys(payload) as k;         RETURN _res;     END; $$;


ALTER FUNCTION public.key_equal_value(payload jsonb) OWNER TO postgres;

--
-- Name: key_equal_value(boolean, jsonb); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION key_equal_value(is_condition boolean, payload jsonb) RETURNS text[]
    LANGUAGE plpgsql
    AS $$     DECLARE         _res text[];     BEGIN         SELECT array_agg(             CASE                 WHEN payload ->> k is null and is_condition THEN quote_ident(k) || ' is null'                 ELSE quote_ident(k) || '=' || quote(payload ->> k)             END)             INTO _res             FROM jsonb_object_keys(payload) as k;         RETURN _res;     END; $$;


ALTER FUNCTION public.key_equal_value(is_condition boolean, payload jsonb) OWNER TO postgres;

--
-- Name: notify_order_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION notify_order_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
        stat_id int;
    BEGIN
        INSERT INTO order_status_event("dok_fazaID", dok, broj, faza, datum, sat)
            VALUES (NEW."dok_fazaID", NEW.dok, NEW.broj, NEW.faza, NEW.datum, NEW.sat)
            RETURNING event_id INTO stat_id;
        PERFORM pg_notify('order', 'v1 ' || stat_id || ' ' || NEW.faza);
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.notify_order_update() OWNER TO postgres;

--
-- Name: quote(anyelement); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION quote(val anyelement) RETURNS text
    LANGUAGE plpgsql
    AS $$     BEGIN         IF val is null THEN             RETURN 'null';         ELSE             RETURN quote_literal(val);         END IF;     END; $$;


ALTER FUNCTION public.quote(val anyelement) OWNER TO postgres;

--
-- Name: to_lit(anyelement); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION to_lit(a anyelement) RETURNS text
    LANGUAGE plpgsql
    AS $$     BEGIN         RETURN             CASE WHEN pg_typeof(a) = 'text' THEN quote_literal(a)                  ELSE (a :: text)             END;     END; $$;


ALTER FUNCTION public.to_lit(a anyelement) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: applied_diffs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE applied_diffs (
    diff_id integer NOT NULL,
    applied_on timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE applied_diffs OWNER TO postgres;

--
-- Name: artikli; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE artikli (
    "artikliID" bigint NOT NULL,
    "RECNO" bigint DEFAULT 0 NOT NULL,
    "SIFRA" character varying(20) DEFAULT ' '::character varying,
    "NAZIV" character varying(90) DEFAULT ' '::character varying,
    "STANJE" numeric(14,2) DEFAULT 0,
    "C1" numeric(14,2) DEFAULT 0,
    "C2" numeric(14,2) DEFAULT 0,
    "C3" numeric(14,2) DEFAULT 0,
    "C4" numeric(14,2) DEFAULT 0,
    "MPC" numeric(14,2) DEFAULT 0,
    "VPC" numeric(14,2) DEFAULT 0,
    "SKLADISTE" character varying(5) DEFAULT ' '::character varying,
    "LOKACIJA" character varying(30) DEFAULT ' '::character varying,
    "SKUPINA" character varying(15) DEFAULT ' '::character varying,
    "NAZIV_SK" character varying(50) DEFAULT ' '::character varying,
    "GRUPA" character varying(15) DEFAULT ' '::character varying,
    "NAZIV_GR" character varying(50) DEFAULT ' '::character varying,
    "P_GRUPA" character varying(15) DEFAULT ' '::character varying,
    "NAZIV_PG" character varying(50) DEFAULT ' '::character varying,
    "BK" character varying(13) DEFAULT ' '::character varying,
    "KB" character varying(20) DEFAULT ' '::character varying,
    "TIP" character varying(15) DEFAULT ' '::character varying,
    "P_SKUPINA" character varying(15) DEFAULT ''::character varying,
    "NAZIV_PS" character varying(50) DEFAULT ''::character varying,
    "BRAND" character varying(15) DEFAULT ''::character varying,
    "MJERA" character varying(10) DEFAULT ''::character varying,
    "POREZ" character varying(3) DEFAULT ''::character varying,
    "BRAND_NZ" character varying(50) DEFAULT ''::character varying,
    "WEB_PROD" character varying(1) DEFAULT ''::character varying,
    "OPIS" text
);


ALTER TABLE artikli OWNER TO postgres;

--
-- Name: atributi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE atributi (
    "atributiID" bigint NOT NULL,
    "ArtikliID" bigint DEFAULT 0,
    "KRATICA" character varying(25) DEFAULT ''::character varying,
    "OPIS" character varying(30) DEFAULT ''::character varying,
    "VRIJEDNOST" text
);


ALTER TABLE atributi OWNER TO postgres;

--
-- Name: dok_faza; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE dok_faza (
    "dok_fazaID" bigint NOT NULL,
    sklad_id character varying(5) DEFAULT ''::character varying,
    dok character varying(3) DEFAULT ''::character varying,
    broj integer DEFAULT 0,
    faza character varying(5) DEFAULT ''::character varying,
    datum date,
    sat time without time zone DEFAULT '00:00:00'::time without time zone
);


ALTER TABLE dok_faza OWNER TO postgres;

--
-- Name: log_rada; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE log_rada (
    "log_radaID" integer NOT NULL,
    "DATUM" date,
    "VRIJEME" time without time zone DEFAULT '00:00:00'::time without time zone,
    "OPIS" character varying(30) DEFAULT ''::character varying,
    "ARTIKL" character varying(20) DEFAULT ''::character varying,
    "PODATAK" character varying(60) DEFAULT ''::character varying
);


ALTER TABLE log_rada OWNER TO postgres;

--
-- Name: order_status_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE order_status_event (
    event_id integer NOT NULL,
    "dok_fazaID" bigint NOT NULL,
    dok character varying(3) DEFAULT ''::character varying,
    broj integer DEFAULT 0,
    faza character varying(5) DEFAULT ''::character varying,
    datum date,
    sat time without time zone DEFAULT '00:00:00'::time without time zone,
    update_time timestamp without time zone DEFAULT now()
);


ALTER TABLE order_status_event OWNER TO postgres;

--
-- Name: order_status_event_event_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_status_event_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_status_event_event_id_seq OWNER TO postgres;

--
-- Name: order_status_event_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_status_event_event_id_seq OWNED BY order_status_event.event_id;


--
-- Name: order_status_event event_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_status_event ALTER COLUMN event_id SET DEFAULT nextval('order_status_event_event_id_seq'::regclass);


--
-- Name: artikli artikli_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY artikli
    ADD CONSTRAINT artikli_pkey PRIMARY KEY ("artikliID");


--
-- Name: atributi atributi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY atributi
    ADD CONSTRAINT atributi_pkey PRIMARY KEY ("atributiID");


--
-- Name: dok_faza dok_faza_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dok_faza
    ADD CONSTRAINT dok_faza_pkey PRIMARY KEY ("dok_fazaID");


--
-- Name: log_rada log_rada_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_rada
    ADD CONSTRAINT log_rada_pkey PRIMARY KEY ("log_radaID");


--
-- Name: dok_faza order_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER order_update AFTER INSERT OR UPDATE ON dok_faza FOR EACH ROW EXECUTE PROCEDURE notify_order_update();


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
