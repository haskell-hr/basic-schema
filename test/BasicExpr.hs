{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
module BasicExpr where

import Internal.Interlude hiding (id)
import Data.Schema.Query
import Data.Schema.Types.Schema as Semantic
import Data.Schema.File
import qualified Data.Schema.Types.AST as AST
import Data.Schema.RenderSQL

basicExprs :: [Text]
basicExprs =
    [ "SELECT  bla;"
    , "SELECT  1  + 1;"
    , "SELECT  1.1;"
    , "SELECT  (-1 * 1.1);"
    , "SELECT  (1 << 2);"
    , "SELECT  (1 # 2);"
    , "SELECT  (1 + 2 + 4);"
    , "SELECT  (1 + 3 * 4);"
    , "SELECT ((1 + 2) * 3);"
    ]


basicExprTests = do
  res <- mapM parseStatements basicExprs
  print (renderToSQL <$> (concat $ rights res))
