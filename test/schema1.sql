-- General tables

CREATE TABLE db_version (
    version integer not null,
    create_date timestamptz not null,
    primary key (version)
);

CREATE TABLE consent_type (
    id UUID not null,
    name TEXT NOT NULL,
    intent TEXT NOT NULL,
    created timestamptz NOT NULL,
    expired timestamptz,
    PRIMARY KEY (id)
);

CREATE TABLE consent (
    user_id UUID NOT NULL,
    action_id UUID NOT NULL,
    created timestamptz NOT NULL,
    valid_to TEXT,
    PRIMARY KEY (user_id, action_id)
);

CREATE TABLE "user" (
    id UUID NOT NULL,
    created timestamptz NOT NULL,
    role text NOT NULL,
    organization_id UUID,
    PRIMARY KEY (id)
);

CREATE TABLE password_login (
    id UUID NOT NULL,
    user_info_id UUID NOT NULL,
    email text NOT NULL,
    password_hash bytea NOT NULL,
    created timestamptz NOT NULL,
    expired timestamptz,
    verification_code UUID NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE auth_user (
    token text NOT NULL,
    provider text NOT NULL,
    user_id UUID NOT NULL,
    created timestamptz NOT NULL,
    expiry timestamptz NOT NULL,
    refresh_token text NOT NULL
);

CREATE TABLE notification (
    id UUID NOT NULL,
    content json NOT NULL,
    user_id UUID NOT NULL,
    created timestamptz NOT NULL,
    viewed timestamptz,
    PRIMARY KEY (id)
);


CREATE TABLE "session" (
    id UUID NOT NULL,
    user_id UUID NOT NULL,
    created timestamptz NOT NULL,
    expires timestamptz NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE oauth_request (
    id UUID NOT NULL,
    session_id UUID NOT NULL,
    created timestamptz NOT NULL,
    resolved timestamptz,
    PRIMARY KEY (id)
);

CREATE TABLE password_reset (
    id UUID NOT NULL,
    code UUID NOT NULL,
    expires timestamptz NOT NULL,
    used timestamptz,
    password_login_id UUID NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE organization (
    id UUID NOT NULL,
    name TEXT NOT NULL,
    invoice_name TEXT NOT NULL,
    address TEXT NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);


-- Product specific tables

CREATE TABLE user_info (
    id UUID NOT NULL,
    user_id UUID NOT NULL,
    name text NOT NULL,
--    phone_number text NOT NULL,
    email text NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE phone_numer (
    phone_number TEXT NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (phone_number)
);

CREATE TABLE call (
    id UUID NOT NULL,
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL,
    phone_number TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE call_record (
    id UUID NOT NULL,
    duration TIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE customer (
    id UUID NOT NULL,
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL,
    name TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE customer_address (
    customer_id UUID NOT NULL,
    created timestamptz NOT NULL,
    adress TEXT NOT NULL
);

CREATE TABLE customer_phone_number (
    customer_id UUID NOT NULL,
    phone_number UUID NOT NULL, -- two customers can have same phone number (e.g. office phone)
    PRIMARY KEY (customer_id, phone_number)
);


-- Product stuff

CREATE TABLE tag (
    id UUID NOT NULL,
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL,
    parent_id UUID NOT NULL,
    name TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE product_tag (
    product_id UUID NOT NULL,
    tag_id UUID NOT NULL,
    created timestamptz NOT NULL,
    PRIMARY KEY (product_id, tag_id)
);

CREATE TABLE product (
    id UUID NOT NULL,
    organization_id UUID NOT NULL,
    name TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE product_price (
    product_id UUID NOT NULL,
    created timestamptz NOT NULL,
    valid_to timestamptz,
    price INT NOT NULL
);

CREATE TABLE food_addition (
    id UUID NOT NULL,
    organization_id UUID NOT NULL,
    name TEXT NOT NULL,
    created timestamptz,
    PRIMARY KEY (id)
);

CREATE TABLE food_addition_applied_tag (
    food_addition_id UUID NOT NULL,
    tag_id UUID NOT NULL,
    created timestamptz NOT NULL,
    PRIMARY KEY (food_addition_id, tag_id)
);

CREATE TABLE order (
    id UUID NOT NULL,
    organization_id UUID NOT NULL,
    daily_cnt INT NOT NULL,
    yearly_cnt INT NOT NULL,
    created timestamptz NOT NULL,
    relative_discount INT NOT NULL,
    absolute_discount INT NOT NULL,
    timed timestamptz NOT NULL,
    note TEXT,
    customer_address UUID NOT NULL,
    call_id UUID,
    PRIMARY KEY (id)
);

CREATE TABLE order_product (
    product_id UUID NOT NULL,
    order_id UUID NOT NULL,
    quantity INT NOT NULL,
    -- dodaci proizvodu
    relative_discount INT NOT NULL,
    absolute_discount INT NOT NULL,
    note TEXT,
    PRIMARY KEY (product_id, order_id)
);

CREATE TABLE delivery (
    id UUID NOT NULL,
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL,
    estimated_time TIME,
    real_time TIME,
    estimated_distance TIME,
    real_distance TIME,
    user_id UUID NOT NULL,
    confirmed timestamptz,
    PRIMARY KEY (id)
);

CREATE TABLE delivery_order (
    order_id UUID NOT NULL,
    delivery_id UUID NOT NULL,
    estimated_time TIME,
    real_time TIME,
    estimated_distance INT,
    real_distance INT,
    PRIMARY KEY (order_id, delivery_id)
);

-- CREATE TABLE driver_path (
-- );

CREATE TABLE supply_location (
    id UUID NOT NULL,
    organization_id UUID NOT NULL,
    location_name text NOT NULL,
    location POINT NOT NULL,
    address TEXT NOT NULL, -- unositi adresu i kucni broj zasebno
    city TEXT NOT NULL,
    created timestamptz NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE driver_info (
    user_id UUID NOT NULL,
    base UUID NOT NULL,
    created timestamptz NOT NULL,
    blocked boolean,
    PRIMARY KEY (user_id)
);

CREATE TABLE invoice (
    id UUID NOT NULL,
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL,
    amount INT NOT NULL,
    status TEXT NOT NULL,
    PRIMARY KEY (id)
);

-- gledati koliko vozacu treba da dodje ponovno u restoran

CREATE VIEW product_details AS
    SELECT *
    FROM product, product_price
    WHERE
        product.id = product_price.product_id
        AND product_price.valid_to IS NULL;
