{-# OPTIONS_GHC -fno-warn-type-defaults #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Internal.Interlude hiding (id)
import Data.Schema.Query
import Data.Schema.Types.Schema as Semantic
import Data.Schema.File
import Data.Schema.SemanticError
import qualified Data.Schema.Types.AST as AST

import Control.Lens hiding (Context(..))
import Data.Schema.Types.Lens
import Test.Hspec
import qualified Data.Text as T
import Control.Monad.IO.Class
import Text.Show.Pretty
import System.IO.Temp
import System.Process
import System.IO
import System.Exit
import BasicExpr

tableIdentifierBasic :: Text
tableIdentifierBasic = "CREATE TABLE tiBasic (id INT NOT NULL);"

primaryKeyBasic :: Text
primaryKeyBasic = "CREATE TABLE pkBasic (id INT NOT NULL PRIMARY KEY);"

primaryKeySerial :: Text
primaryKeySerial = "CREATE TABLE pkSerial (id SERIAL);"

tableIdentifierQuoted :: Text
tableIdentifierQuoted = "CREATE TABLE \"tiQuoted\" (id INT NOT NULL);"

tableIdentifierSchema :: Text
tableIdentifierSchema = "CREATE TABLE myschema.tiSchema (id INT NOT NULL);"

tableIdentifierSchemaQuoted :: Text
tableIdentifierSchemaQuoted = "CREATE TABLE \"myschema\".\"tiSchemaQuoted\" (id INT NOT NULL);"

duplicateTables :: Text
duplicateTables = "CREATE TABLE magic (id SERIAL); CREATE TABLE magic (id SERIAL);"

duplicateColumns :: Text
duplicateColumns = "CREATE TABLE magic (name text, name text);"

quotedColumns :: Text
quotedColumns = "CREATE TABLE \"myschema\".\"quotedColumns\" (id INT NOT NULL, \"myTestField\" text);"

basicEnum :: Text
basicEnum = "CREATE TYPE public.myenum AS ENUM ('CustomType1', 'customtype2', 'analysis');";

enumInTable :: Text
enumInTable = "CREATE TYPE public.myenum AS ENUM ('CustomType1', 'customtype2', 'analysis'); CREATE TABLE mytable (myfield myenum NOT NULL);";

optionalField :: Text
optionalField = "CREATE TABLE opt1 (id INT NOT NULL, name text);"

defaultFieldNull :: Text
defaultFieldNull = "CREATE TABLE opt2 (id INT NOT NULL, name text DEFAULT 'John');"

defaultFieldNotNull :: Text
defaultFieldNotNull = "CREATE TABLE opt3 (id INT NOT NULL, name text NOT NULL DEFAULT 'John');"


-- Alter table test cases

afterPrimaryKey :: Text
afterPrimaryKey = "CREATE TABLE afk (id INT NOT NULL); ALTER TABLE afk ADD CONSTRAINT pkey PRIMARY KEY (id);"

alterTableOnly :: Text
alterTableOnly = "CREATE TABLE afk2 (id INT NOT NULL); ALTER TABLE ONLY afk2 ADD CONSTRAINT pkey PRIMARY KEY (id);"


-- Foreign key test
foreignKey :: Text
foreignKey = "CREATE TABLE author (id SERIAL, PRIMARY KEY (id)); CREATE TABLE post (id SERIAL, author_id INT, PRIMARY KEY (id));ALTER TABLE post ADD CONSTRAINT fkey FOREIGN KEY (author_id) REFERENCES author(\"id\");"

handleParseFail :: MonadIO m => Either SemanticParseError t -> (t -> m a2) -> m a2
handleParseFail res cb =
    case res of
       Left (ParseError t) -> do
           liftIO $ putStrLn (toS t)
           fail "failed parse"
       Left (SemanticError t) -> do
           liftIO $ print t
           fail "failed compile"
       Right k -> cb k

shouldHaveColumn :: Text -> (Text, Text) -> Text -> IO Column
shouldHaveColumn sample (expectedSchema, expectedTable) expectedColumn = do
  res <- semanticParse sample
  handleParseFail res (\(_, schema') ->
      case getTable schema' expectedSchema expectedTable of
          Nothing -> fail $ "Table mytable not found in schema. Schema: " <> show schema'
          Just table' -> do
              (table' ^. id . schemaName) `shouldBe` expectedSchema
              (table' ^. name) `shouldBe` expectedTable
              let columnId = ColumnId (table' ^. id) expectedColumn
              case getColumnById schema' columnId of
                  Nothing -> fail $ "Column " <> toS expectedColumn <> " not found"
                  Just c -> do
                      (c ^. name) `shouldBe` expectedColumn
                      return c
                        )

shouldHaveTable :: Text -> (Text, Text) -> IO (Schema, Table)
shouldHaveTable sample (expectedSchema, expectedTable) = do
  res <- semanticParse sample
  handleParseFail res (\(_, dbModel) ->
      case getTable dbModel expectedSchema expectedTable of
          Nothing -> fail $ "Table mytable not found in schema. Schema: " <> show dbModel
          Just table' -> do
              (table' ^. id . schemaName) `shouldBe` expectedSchema
              (table' ^. name) `shouldBe` expectedTable
              return (dbModel, table'))

main :: IO ()
main = hspec $ do
    describe "Table identifiers" $ do

        it "should parse classic identifiers" $
            void $ tableIdentifierBasic `shouldHaveTable` ("public", "tiBasic")

        it "should parse quoted identifiers" $
            void $ tableIdentifierQuoted `shouldHaveTable` ("public", "tiQuoted")

        it "should parse qualified identifiers" $
            void $ tableIdentifierSchema `shouldHaveTable` ("myschema", "tiSchema")

        it "should parse quoted qualified identifiers" $
            void $ tableIdentifierSchemaQuoted `shouldHaveTable` ("myschema", "tiSchemaQuoted")


    describe "Table columns" $
        it "should parse quoted columns" $
            void $ shouldHaveColumn quotedColumns ("myschema", "quotedColumns") "myTestField"


    describe "enums " $ do
        it "should be parsed" $ do
            res <- semanticParse basicEnum
            handleParseFail res (\(_, dbModel) -> do
                let mRes = getEnumType  dbModel (TableId "public" "myenum")
                case mRes of
                    Nothing -> fail "Enum cannot be found"
                    Just customType ->
                        case customType of
                            (AST.Enumerated _) -> do
                                isEnumeratedType customType `shouldBe` True
                            o -> fail $ "Enum is not parsed correctly. Expected to be parsed as Enumerated, got something else: " <> show o
                                  )
        it "should be usable in a table" $ do
            res <- semanticParse enumInTable
            handleParseFail res (\(_, schema') ->
                case getTable schema' "public" "mytable" of
                  Nothing -> fail $ "Table mytable not found in schema. Schema: " <> show schema'
                  Just table' ->
                      case getColumnType schema' table' "myfield" of
                        Nothing -> fail $ "Enum column could not be found in: " <> show schema'
                        Just sqlType' -> return ()) -- @TODO make custom/complex types supported in the semantic SqlType -- sqlType' `shouldBe` AST.Other "myenum"

    describe "Fields" $ do
        it "should be optional when NOT NULL isn't used" $ do
            (_, t) <- optionalField `shouldHaveTable` ("public", "opt1")
            c <- shouldHaveColumn optionalField ("public", "opt1") "name"
            (t, c) `shouldSatisfy` uncurry isColumnOptional
            (t, c) `shouldNotSatisfy` uncurry isColumnRequired
            (t, c) `shouldSatisfy` uncurry requiresMaybeWrapper

        it "should not be optional if default and not null constraints are present" $ do
            (_, t) <- defaultFieldNotNull `shouldHaveTable` ("public", "opt3")
            c <- shouldHaveColumn defaultFieldNotNull ("public", "opt3") "name"
            (t, c) `shouldSatisfy` uncurry isColumnOptional
            (t, c) `shouldNotSatisfy` uncurry isColumnRequired
            (t, c) `shouldNotSatisfy` uncurry requiresMaybeWrapper

        it "should be optional if default and null constraints are present" $ do
            let input = defaultFieldNull
            (_, t) <- input `shouldHaveTable` ("public", "opt2")
            c <- shouldHaveColumn input ("public", "opt2") "name"
            (t, c) `shouldSatisfy` uncurry isColumnOptional
            (t, c) `shouldNotSatisfy` uncurry isColumnRequired
            (t, c) `shouldSatisfy` uncurry requiresMaybeWrapper

    describe "Primary keys" $ do
        it "should parse column constraint syntax" $ do
            (dbModel, t) <- primaryKeyBasic `shouldHaveTable` ("public", "pkBasic")
            hasPrimaryKey dbModel t `shouldBe` True

        it "should be added if used with alter table" $ do
            (dbModel, t) <- afterPrimaryKey `shouldHaveTable` ("public", "afk")
            hasPrimaryKey dbModel t `shouldBe` True

        it "should be added if used with alter table, ONLY" $ do
            (dbModel, t) <- alterTableOnly `shouldHaveTable` ("public", "afk2")
            hasPrimaryKey dbModel t `shouldBe` True

    describe "Foreign keys" $
        it "should parse alter table syntax" $ do
            (dbModel, author) <- foreignKey `shouldHaveTable` ("public", "author")
            (_, post) <- foreignKey `shouldHaveTable` ("public", "post")
            hasPrimaryKey dbModel author `shouldBe` True
            hasPrimaryKey dbModel post `shouldBe` True

--    describe "Schema parsing" $
--        it "should successfully parse and compile the erp schema" $ do
--            res <- liftIO $ semanticParseFile "test/erp.sql"
--            sch <- readFile "test/schema.hs"
--            handleParseFail res $ \sch' ->
--                withSystemTempFile "diffa" $ \fpa ha ->
--                withSystemTempFile "diffb" $ \fpb hb -> do
--                    hPutStr ha sch
--                    hPutStr hb (ppShow (AST.statement <$> fst sch'))
--                    (ec, out, _) <- readProcessWithExitCode "git"
--                        ["-c", "color.ui=always", "diff", "--no-index", fpa, fpb] ""
--                    when (ec /= ExitSuccess) $ do
--                        putStrLn out
--                        "something else" `shouldBe` "correct schema"

    describe "Error checking" $ do
        it "should detect duplicate table names" $ do
            res <- liftIO $ parseStatements duplicateTables
            case res of
                Left (SemanticError (SemanticErrorContext (TableAlreadyExists _) _)) -> return ()
                Left err -> do
                    print err
                    fail "Couldn't detect duplicate table names"
                Right res -> fail "Couldn't detect duplicate table names"
        it "should detect duplicate column names" $ do
            res <- liftIO $ parseStatements duplicateColumns
            print res
    describe "Dentum view checking" $
        it "should complain about duplicate column names" $ do
            res <- liftIO $ semanticParseFile "test/dentum.sql"
            case res of
                Left (SemanticError (SemanticErrorContext (ViewColumnNamesDuplicate _) _)) -> return ()
                Left (ParseError err) -> do
                    putStrLn $ toS err
                    fail "failed parse1"
                Left (SemanticError err) -> do
                    print err
                    fail "failed parse2"
                Right _ -> fail "Typechecking the view somehow succeeded despite the ambiguous column names"

    describe "Chop drop checking" $
        it "parse and typecheck the schema" $ do
            res <- semanticParseFile "test/chopdrop.sql"
            handleParseFail res $ \_ -> return ()

    describe "pg_dump checking" $
        it "parse and typecheck the schema dumped from the db" $ do
            res <- semanticParseFile "test/chopdrop2.sql"
            handleParseFail res $ \_ -> return ()
