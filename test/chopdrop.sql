-- General tables

CREATE TABLE db_version (
    version int not null,
    create_date timestamptz not null,
    migration_name text not null,
    primary key (version)
);

CREATE TABLE consent_type (
    id UUID not null DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL,
    intent TEXT NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    expired timestamptz,
    PRIMARY KEY (id)
);

CREATE TABLE consent (
    user_id UUID NOT NULL,
    action_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    valid_to TEXT,
    PRIMARY KEY (user_id, action_id)
);

CREATE TABLE "user" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    created timestamptz NOT NULL DEFAULT NOW(),
    role text NOT NULL,
    organization_id UUID,
    PRIMARY KEY (id)
);

CREATE TABLE password_login (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    email text NOT NULL,
    password_hash bytea NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    expired timestamptz,
    verification_code UUID NOT NULL DEFAULT uuid_generate_v4(),
    PRIMARY KEY (id)
);

CREATE TABLE auth_user (
    token text NOT NULL,
    provider text NOT NULL,
    user_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    expiry timestamptz NOT NULL,
    refresh_token text NOT NULL
);

CREATE TABLE notification (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    content json NOT NULL,
    user_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    viewed timestamptz,
    PRIMARY KEY (id)
);


CREATE TABLE "session" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    expires timestamptz NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE oauth_request (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    session_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    resolved timestamptz,
    PRIMARY KEY (id)
);

CREATE TABLE password_reset (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    code UUID NOT NULL,
    expires timestamptz NOT NULL,
    used timestamptz,
    password_login_id UUID NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE organization (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    name TEXT NOT NULL,
    invoice_name TEXT NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);


-- user specific tables

CREATE TABLE user_info (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    name text NOT NULL,
    email text NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE call (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    phone_number TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE customer (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    name TEXT,
    PRIMARY KEY (id)
);

CREATE TABLE address (
    id UUID NOT NULL default uuid_generate_v4(),
    created timestamptz NOT NULL DEFAULT NOW(),
    geo point NOT NULL,
    address TEXT NOT NULL
);

-- Product stuff

CREATE TABLE tag (
    id UUID NOT NULL default uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL default NOW(),
    parent_id UUID,
    name TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE product_tag (
    product_id UUID NOT NULL,
    tag_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (product_id, tag_id)
);

CREATE TABLE product (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    name TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE product_price (
    product_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    valid_to timestamptz,
    price INT NOT NULL
);

CREATE TABLE food_addition (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    name TEXT NOT NULL,
    created timestamptz DEFAULT NOW(),
    PRIMARY KEY (id)
);

CREATE TABLE food_addition_price (
    food_addition_id UUID NOT NULL REFERENCES food_addition(id),
    created timestamptz NOT NULL,
    valid_to timestamptz,
    price int NOT NULL
);

CREATE TABLE food_addition_applied_tag (
    food_addition_id UUID NOT NULL,
    tag_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (food_addition_id, tag_id)
);

CREATE VIEW food_addition_details AS
SELECT fa.id, fa.organization_id, fa.name, prices.price, tags.tags as categories
FROM
    food_addition as fa,
    (
        SELECT food_addition_price.food_addition_id, food_addition_price.price
        FROM food_addition_price
        WHERE valid_to IS NULL
    ) as prices,
    (
        SELECT food_addition_applied_tag.food_addition_id, array_agg(tag.name) as tags
        FROM food_addition_applied_tag, tag
        WHERE food_addition_applied_tag.tag_id = tag.id
        GROUP by food_addition_applied_tag.food_addition_id
    ) as tags
WHERE
    prices.food_addition_id = fa.id AND tags.food_addition_id = fa.id;

CREATE TABLE "order" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    relative_discount INT NOT NULL,
    absolute_discount INT NOT NULL,
    estimate INT,
    note TEXT,
    address_id UUID NOT NULL,
    call_id UUID,
    delivery_id UUID,
    status TEXT NOT NULL default 'Unassigned',
    deleted timestamptz,
    PRIMARY KEY (id)
);

CREATE TABLE food_addition_with_quantity
(
    food_addition food_addition_details not null,
    quantity int not null
);

CREATE TABLE order_product (
    product_id UUID NOT NULL,
    order_id UUID NOT NULL,
    quantity INT NOT NULL,
    additions food_addition_with_quantity[] NOT NULL,
    relative_discount INT NOT NULL,
    absolute_discount INT NOT NULL,
    note TEXT,
    PRIMARY KEY (product_id, order_id)
);

CREATE TABLE delivery (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    estimated_time INT NOT NULL,
    real_time INT,
    estimated_distance INT NOT NULL,
    real_distance INT,
    driver_id UUID,
    confirmed timestamptz,
    suggested_route POINT[] NOT NULL,
    accepted timestamptz,
    on_route timestamptz,
    completed timestamptz,
    PRIMARY KEY (id)
);

-- CREATE TABLE driver_path (
-- );

CREATE TABLE driver_location (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    driver_id UUID NOT NULL,
    location point NOT NULL,
    send timestamptz NOT NULL DEFAULT now(),
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);

CREATE TABLE supply_location (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    location_name text NOT NULL,
    address TEXT NOT NULL,
    geo POINT NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);

CREATE TABLE driver_info (
    user_id UUID NOT NULL,
    base UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    blocked boolean,
    PRIMARY KEY (user_id)
);

CREATE TABLE invoice (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    organization_id UUID NOT NULL,
    created timestamptz NOT NULL DEFAULT NOW(),
    due timestamptz NOT NULL DEFAULT NOW(),
    paid timestamptz,
    amount INT NOT NULL,
    status TEXT NOT NULL,
    PRIMARY KEY (id)
);


CREATE VIEW product_additions AS
    SELECT
        product.id as product_id,
        CASE
            WHEN addition_ids.prod_id IS NULL THEN '{}' :: food_addition_details[]
            ELSE array_agg(food_addition_details.* :: food_addition_details)
        END AS additions
    FROM
        product
        LEFT JOIN (
            SELECT
                product.id as prod_id, array_agg(food_addition_applied_tag.food_addition_id) as ids
            FROM
                product
                INNER JOIN product_tag ON (product_tag.product_id = product.id)
                INNER JOIN food_addition_applied_tag
                    ON (food_addition_applied_tag.tag_id = product_tag.tag_id)
            GROUP BY product.id
        ) AS addition_ids ON (product.id = addition_ids.prod_id)
        LEFT JOIN food_addition_details ON (food_addition_details.id = ANY(addition_ids.ids))
    GROUP BY product.id, addition_ids.prod_id;

CREATE VIEW product_details (product, price, tags, additions) AS
    SELECT row(product.*) :: product, product_price.price, tags.tag_names, product_additions.additions
    FROM
        product,
        product_price,
        (
            select product_tag.product_id, array_agg(tag.name) as tag_names
            from product_tag, tag
            where product_tag.tag_id = tag.id
            group by product_tag.product_id
        ) as tags,
        product_additions
    WHERE
        product.id = product_price.product_id AND
        product_price.valid_to IS NULL AND
        product.id = tags.product_id AND
        product.id = product_additions.product_id;

-- gledati koliko vozacu treba da dodje ponovno u restoran

CREATE TABLE "activity_log" (
    id UUID NOT NULL DEFAULT uuid_generate_v4(),
    user_id UUID NOT NULL,
    session_id UUID NOT NULL,
    msg text NOT NULL,
    content json NOT NULL,
    lvl integer NOT NULL,
    created timestamptz NOT NULL default NOW(),
    PRIMARY KEY (id)
);
