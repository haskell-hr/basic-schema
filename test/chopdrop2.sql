--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.0
-- Dumped by pg_dump version 9.6.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = public, pg_catalog;

--
-- Name: get_new_id(uuid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_new_id(uuid) RETURNS uuid
    LANGUAGE sql
    AS $_$(select new_id from new_ids where old_id = $1)$_$;


ALTER FUNCTION public.get_new_id(uuid) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: activity_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE activity_log (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_id uuid,
    session_id uuid,
    msg text NOT NULL,
    content json NOT NULL,
    lvl integer NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE activity_log OWNER TO postgres;

--
-- Name: address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE address (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    geo point NOT NULL,
    street text,
    number text,
    locality text,
    country text,
    post_code text,
    formatted_address text NOT NULL,
    original_address text
);


ALTER TABLE address OWNER TO postgres;

--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user (
    token text NOT NULL,
    provider text NOT NULL,
    user_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    expiry timestamp with time zone NOT NULL,
    refresh_token text NOT NULL
);


ALTER TABLE auth_user OWNER TO postgres;

--
-- Name: call; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE call (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    organization_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    phone_number text NOT NULL,
    answered timestamp with time zone,
    answered_by uuid
);


ALTER TABLE call OWNER TO postgres;

--
-- Name: consent; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE consent (
    user_id uuid NOT NULL,
    action_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    valid_to text
);


ALTER TABLE consent OWNER TO postgres;

--
-- Name: consent_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE consent_type (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    intent text NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    expired timestamp with time zone
);


ALTER TABLE consent_type OWNER TO postgres;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    organization_id uuid NOT NULL,
    name text,
    email text,
    created timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE customer OWNER TO postgres;

--
-- Name: customer_address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer_address (
    customer_id uuid NOT NULL,
    address_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE customer_address OWNER TO postgres;

--
-- Name: customer_phone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer_phone (
    customer_id uuid NOT NULL,
    phone_number text NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE customer_phone OWNER TO postgres;

--
-- Name: customer_detail; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW customer_detail AS
 SELECT customer.*::customer AS customer,
    addrs.addrs AS addresses,
    phones.phones
   FROM customer,
    ( SELECT customer_address.customer_id,
            array_agg(address.*) AS addrs
           FROM customer_address,
            address
          WHERE (customer_address.address_id = address.id)
          GROUP BY customer_address.customer_id) addrs,
    ( SELECT customer_phone.customer_id,
            array_agg(customer_phone.phone_number) AS phones
           FROM customer_phone
          GROUP BY customer_phone.customer_id) phones
  WHERE ((customer.id = addrs.customer_id) AND (customer.id = phones.customer_id));


ALTER TABLE customer_detail OWNER TO postgres;

--
-- Name: db_version; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE db_version (
    version integer NOT NULL,
    create_date timestamp with time zone NOT NULL,
    migration_name text NOT NULL
);


ALTER TABLE db_version OWNER TO postgres;

--
-- Name: delivery; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE delivery (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    organization_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    estimated_time integer NOT NULL,
    real_time integer,
    estimated_distance integer NOT NULL,
    real_distance integer,
    driver_id uuid,
    confirmed timestamp with time zone,
    suggested_route point[] NOT NULL,
    accepted timestamp with time zone,
    on_route timestamp with time zone,
    completed timestamp with time zone,
    label text NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE delivery OWNER TO postgres;

--
-- Name: driver_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE driver_info (
    user_id uuid NOT NULL,
    base uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    blocked boolean
);


ALTER TABLE driver_info OWNER TO postgres;

--
-- Name: driver_location; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE driver_location (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    driver_id uuid NOT NULL,
    location point NOT NULL,
    send timestamp with time zone DEFAULT now() NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE driver_location OWNER TO postgres;

--
-- Name: enabled_addition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE enabled_addition (
    product_id uuid NOT NULL,
    food_addition_id uuid NOT NULL
);


ALTER TABLE enabled_addition OWNER TO postgres;

--
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    organization_id uuid NOT NULL,
    name text NOT NULL,
    deleted timestamp with time zone
);


ALTER TABLE product OWNER TO postgres;

--
-- Name: enabled_additions_for_product; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW enabled_additions_for_product AS
 SELECT product.id AS product_id,
        CASE
            WHEN (enabled_addition.product_id IS NULL) THEN '{}'::uuid[]
            ELSE array_agg(enabled_addition.food_addition_id)
        END AS additions
   FROM (product
     LEFT JOIN enabled_addition ON ((enabled_addition.product_id = product.id)))
  GROUP BY product.id, enabled_addition.product_id;


ALTER TABLE enabled_additions_for_product OWNER TO postgres;

--
-- Name: failed_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE failed_request (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    url text NOT NULL,
    request_made timestamp with time zone NOT NULL,
    request_received timestamp with time zone NOT NULL,
    user_id uuid NOT NULL
);


ALTER TABLE failed_request OWNER TO postgres;

--
-- Name: fcm_registration_id; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE fcm_registration_id (
    id text NOT NULL,
    session_id uuid NOT NULL,
    created timestamp with time zone NOT NULL,
    expires timestamp with time zone
);


ALTER TABLE fcm_registration_id OWNER TO postgres;

--
-- Name: food_addition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE food_addition (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    organization_id uuid NOT NULL,
    name text NOT NULL,
    created timestamp with time zone DEFAULT now()
);


ALTER TABLE food_addition OWNER TO postgres;

--
-- Name: food_addition_applied_tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE food_addition_applied_tag (
    food_addition_id uuid NOT NULL,
    tag_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE food_addition_applied_tag OWNER TO postgres;

--
-- Name: food_addition_price; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE food_addition_price (
    food_addition_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    valid_to timestamp with time zone,
    price integer NOT NULL
);


ALTER TABLE food_addition_price OWNER TO postgres;

--
-- Name: tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tag (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    organization_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    parent_id uuid,
    name text NOT NULL
);


ALTER TABLE tag OWNER TO postgres;

--
-- Name: food_addition_details; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW food_addition_details AS
 SELECT fa.id,
    fa.organization_id,
    fa.name,
    prices.price,
        CASE
            WHEN (tags.tags IS NULL) THEN '{}'::tag[]
            ELSE tags.tags
        END AS categories
   FROM ((food_addition fa
     JOIN ( SELECT food_addition_price.food_addition_id,
            food_addition_price.price
           FROM food_addition_price
          WHERE (food_addition_price.valid_to IS NULL)) prices ON ((prices.food_addition_id = fa.id)))
     LEFT JOIN ( SELECT food_addition_applied_tag.food_addition_id,
            array_agg(tag.*) AS tags
           FROM (tag
             LEFT JOIN food_addition_applied_tag ON ((food_addition_applied_tag.tag_id = tag.id)))
          GROUP BY food_addition_applied_tag.food_addition_id) tags ON ((tags.food_addition_id = fa.id)));


ALTER TABLE food_addition_details OWNER TO postgres;

--
-- Name: food_addition_with_quantity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE food_addition_with_quantity (
    food_addition food_addition_details NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE food_addition_with_quantity OWNER TO postgres;

--
-- Name: gf_addition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gf_addition (
    addition_id uuid NOT NULL,
    gf_id integer NOT NULL
);


ALTER TABLE gf_addition OWNER TO postgres;

--
-- Name: gf_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gf_category (
    tag_id uuid NOT NULL,
    gf_id integer NOT NULL
);


ALTER TABLE gf_category OWNER TO postgres;

--
-- Name: gf_key; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gf_key (
    sync_key text NOT NULL,
    order_key text NOT NULL,
    organization_id uuid NOT NULL
);


ALTER TABLE gf_key OWNER TO postgres;

--
-- Name: gf_order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gf_order (
    gf_order_id integer NOT NULL,
    order_id uuid NOT NULL
);


ALTER TABLE gf_order OWNER TO postgres;

--
-- Name: gf_product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gf_product (
    product_id uuid NOT NULL,
    gf_size_name text,
    gf_id integer NOT NULL
);


ALTER TABLE gf_product OWNER TO postgres;

--
-- Name: gf_size; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gf_size (
    gf_category_id integer NOT NULL,
    gf_size_name text NOT NULL,
    tag_id uuid NOT NULL
);


ALTER TABLE gf_size OWNER TO postgres;

--
-- Name: invoice; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE invoice (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    organization_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    due timestamp with time zone DEFAULT now() NOT NULL,
    paid timestamp with time zone,
    amount integer NOT NULL,
    status text NOT NULL
);


ALTER TABLE invoice OWNER TO postgres;

--
-- Name: notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE notification (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    content json NOT NULL,
    user_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    viewed timestamp with time zone
);


ALTER TABLE notification OWNER TO postgres;

--
-- Name: oauth_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE oauth_request (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    session_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    resolved timestamp with time zone
);


ALTER TABLE oauth_request OWNER TO postgres;

--
-- Name: order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "order" (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    organization_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    relative_discount integer NOT NULL,
    absolute_discount integer NOT NULL,
    estimate integer,
    driver_estimate integer NOT NULL,
    note text,
    address_id uuid NOT NULL,
    call_id uuid,
    phone_number text,
    delivery_id uuid,
    status text DEFAULT 'Unassigned'::text NOT NULL,
    deleted timestamp with time zone,
    completed timestamp with time zone,
    index_in_delivery integer NOT NULL,
    customer_id uuid NOT NULL
);


ALTER TABLE "order" OWNER TO postgres;

--
-- Name: order_product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE order_product (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    product_id uuid NOT NULL,
    order_id uuid NOT NULL,
    quantity integer NOT NULL,
    additions food_addition_with_quantity[] NOT NULL,
    relative_discount integer NOT NULL,
    absolute_discount integer NOT NULL,
    note text
);


ALTER TABLE order_product OWNER TO postgres;

--
-- Name: organization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE organization (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    invoice_name text NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE organization OWNER TO postgres;

--
-- Name: password_login; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE password_login (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    email text NOT NULL,
    password_hash bytea NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    expired timestamp with time zone,
    verification_code uuid DEFAULT uuid_generate_v4() NOT NULL
);


ALTER TABLE password_login OWNER TO postgres;

--
-- Name: password_reset; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE password_reset (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    code uuid NOT NULL,
    expires timestamp with time zone NOT NULL,
    used timestamp with time zone,
    password_login_id uuid NOT NULL
);


ALTER TABLE password_reset OWNER TO postgres;

--
-- Name: phone_number_address; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW phone_number_address AS
 SELECT call.created,
    call.phone_number,
    ROW(address.id, address.created, address.geo, address.street, address.number, address.locality, address.country, address.post_code, address.formatted_address, NULL::text)::address AS address
   FROM "order",
    call,
    address
  WHERE (("order".call_id = call.id) AND ("order".address_id = address.id));


ALTER TABLE phone_number_address OWNER TO postgres;

--
-- Name: product_tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_tag (
    product_id uuid NOT NULL,
    tag_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE product_tag OWNER TO postgres;

--
-- Name: product_additions; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW product_additions AS
 SELECT product.id AS product_id,
        CASE
            WHEN (addition_ids.prod_id IS NULL) THEN '{}'::food_addition_details[]
            ELSE array_agg(food_addition_details.*)
        END AS additions
   FROM ((product
     LEFT JOIN ( SELECT product_1.id AS prod_id,
            array_agg(food_addition_applied_tag.food_addition_id) AS ids
           FROM ((product product_1
             JOIN product_tag ON ((product_tag.product_id = product_1.id)))
             JOIN food_addition_applied_tag ON ((food_addition_applied_tag.tag_id = product_tag.tag_id)))
          GROUP BY product_1.id) addition_ids ON ((product.id = addition_ids.prod_id)))
     LEFT JOIN food_addition_details ON ((food_addition_details.id = ANY (addition_ids.ids))))
  GROUP BY product.id, addition_ids.prod_id;


ALTER TABLE product_additions OWNER TO postgres;

--
-- Name: product_price; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE product_price (
    product_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    valid_to timestamp with time zone,
    price integer NOT NULL
);


ALTER TABLE product_price OWNER TO postgres;

--
-- Name: product_details; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW product_details AS
 SELECT ROW(product.id, product.organization_id, product.name, product.deleted)::product AS product,
    product_price.price,
    tags.tags,
    product_additions.additions,
    enabled_additions_for_product.additions AS enabled_additions
   FROM product,
    product_price,
    ( SELECT product_tag.product_id,
            array_agg(tag.*) AS tags
           FROM product_tag,
            tag
          WHERE (product_tag.tag_id = tag.id)
          GROUP BY product_tag.product_id) tags,
    product_additions,
    enabled_additions_for_product
  WHERE ((product.id = product_price.product_id) AND (product_price.valid_to IS NULL) AND (product.deleted IS NULL) AND (product.id = tags.product_id) AND (product.id = product_additions.product_id) AND (product.id = enabled_additions_for_product.product_id));


ALTER TABLE product_details OWNER TO postgres;

--
-- Name: sent_notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE sent_notification (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    notification_payload jsonb NOT NULL,
    user_id uuid NOT NULL,
    sent timestamp with time zone
);


ALTER TABLE sent_notification OWNER TO postgres;

--
-- Name: session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE session (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    expires timestamp with time zone NOT NULL
);


ALTER TABLE session OWNER TO postgres;

--
-- Name: supply_location; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE supply_location (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    organization_id uuid NOT NULL,
    location_name text NOT NULL,
    address text NOT NULL,
    geo point NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE supply_location OWNER TO postgres;

--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "user" (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    created timestamp with time zone DEFAULT now() NOT NULL,
    role text NOT NULL,
    organization_id uuid,
    deleted timestamp with time zone
);


ALTER TABLE "user" OWNER TO postgres;

--
-- Name: user_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE user_info (
    id uuid DEFAULT uuid_generate_v4() NOT NULL,
    user_id uuid NOT NULL,
    name text NOT NULL,
    email text NOT NULL
);


ALTER TABLE user_info OWNER TO postgres;

--
-- Name: activity_log activity_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY activity_log
    ADD CONSTRAINT activity_log_pkey PRIMARY KEY (id);


--
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- Name: call call_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY call
    ADD CONSTRAINT call_pkey PRIMARY KEY (id);


--
-- Name: consent consent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY consent
    ADD CONSTRAINT consent_pkey PRIMARY KEY (user_id, action_id);


--
-- Name: consent_type consent_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY consent_type
    ADD CONSTRAINT consent_type_pkey PRIMARY KEY (id);


--
-- Name: customer_address customer_address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_address
    ADD CONSTRAINT customer_address_pkey PRIMARY KEY (customer_id, address_id);


--
-- Name: customer_phone customer_phone_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_phone
    ADD CONSTRAINT customer_phone_pkey PRIMARY KEY (customer_id, phone_number);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: db_version db_version_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY db_version
    ADD CONSTRAINT db_version_pkey PRIMARY KEY (version);


--
-- Name: delivery delivery_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY delivery
    ADD CONSTRAINT delivery_pkey PRIMARY KEY (id);


--
-- Name: driver_info driver_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY driver_info
    ADD CONSTRAINT driver_info_pkey PRIMARY KEY (user_id);


--
-- Name: driver_location driver_location_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY driver_location
    ADD CONSTRAINT driver_location_pkey PRIMARY KEY (id);


--
-- Name: enabled_addition enabled_addition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY enabled_addition
    ADD CONSTRAINT enabled_addition_pkey PRIMARY KEY (product_id, food_addition_id);


--
-- Name: failed_request failed_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY failed_request
    ADD CONSTRAINT failed_request_pkey PRIMARY KEY (id);


--
-- Name: fcm_registration_id fcm_registration_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fcm_registration_id
    ADD CONSTRAINT fcm_registration_id_pkey PRIMARY KEY (id);


--
-- Name: food_addition_applied_tag food_addition_applied_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY food_addition_applied_tag
    ADD CONSTRAINT food_addition_applied_tag_pkey PRIMARY KEY (food_addition_id, tag_id);


--
-- Name: food_addition food_addition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY food_addition
    ADD CONSTRAINT food_addition_pkey PRIMARY KEY (id);


--
-- Name: food_addition_price food_addition_price_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY food_addition_price
    ADD CONSTRAINT food_addition_price_pkey PRIMARY KEY (food_addition_id, created);


--
-- Name: gf_addition gf_addition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gf_addition
    ADD CONSTRAINT gf_addition_pkey PRIMARY KEY (addition_id, gf_id);


--
-- Name: gf_category gf_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gf_category
    ADD CONSTRAINT gf_category_pkey PRIMARY KEY (gf_id, tag_id);


--
-- Name: gf_key gf_key_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gf_key
    ADD CONSTRAINT gf_key_pkey PRIMARY KEY (sync_key, order_key);


--
-- Name: gf_order gf_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gf_order
    ADD CONSTRAINT gf_order_pkey PRIMARY KEY (gf_order_id, order_id);


--
-- Name: gf_product gf_product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gf_product
    ADD CONSTRAINT gf_product_pkey PRIMARY KEY (product_id, gf_id);


--
-- Name: gf_size gf_size_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gf_size
    ADD CONSTRAINT gf_size_pkey PRIMARY KEY (gf_category_id, gf_size_name);


--
-- Name: invoice invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (id);


--
-- Name: notification notification_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);


--
-- Name: oauth_request oauth_request_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oauth_request
    ADD CONSTRAINT oauth_request_pkey PRIMARY KEY (id);


--
-- Name: order order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);


--
-- Name: order_product order_product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_product
    ADD CONSTRAINT order_product_pkey PRIMARY KEY (id);


--
-- Name: organization organization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY organization
    ADD CONSTRAINT organization_pkey PRIMARY KEY (id);


--
-- Name: password_login password_login_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY password_login
    ADD CONSTRAINT password_login_pkey PRIMARY KEY (id);


--
-- Name: password_reset password_reset_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY password_reset
    ADD CONSTRAINT password_reset_pkey PRIMARY KEY (id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: product_tag product_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_tag
    ADD CONSTRAINT product_tag_pkey PRIMARY KEY (product_id, tag_id);


--
-- Name: sent_notification sent_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sent_notification
    ADD CONSTRAINT sent_notification_pkey PRIMARY KEY (id);


--
-- Name: session session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY session
    ADD CONSTRAINT session_pkey PRIMARY KEY (id);


--
-- Name: supply_location supply_location_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY supply_location
    ADD CONSTRAINT supply_location_pkey PRIMARY KEY (id);


--
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (id);


--
-- Name: user_info user_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_info
    ADD CONSTRAINT user_info_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: activity_log activity_log_session_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY activity_log
    ADD CONSTRAINT activity_log_session_id_fkey FOREIGN KEY (session_id) REFERENCES session(id);


--
-- Name: activity_log activity_log_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY activity_log
    ADD CONSTRAINT activity_log_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: auth_user auth_user_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: call call_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY call
    ADD CONSTRAINT call_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- Name: customer_address customer_address_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_address
    ADD CONSTRAINT customer_address_address_id_fkey FOREIGN KEY (address_id) REFERENCES address(id);


--
-- Name: customer_address customer_address_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_address
    ADD CONSTRAINT customer_address_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: customer customer_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- Name: customer_phone customer_phone_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer_phone
    ADD CONSTRAINT customer_phone_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: delivery delivery_driver_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY delivery
    ADD CONSTRAINT delivery_driver_id_fkey FOREIGN KEY (driver_id) REFERENCES "user"(id);


--
-- Name: delivery delivery_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY delivery
    ADD CONSTRAINT delivery_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- Name: driver_info driver_info_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY driver_info
    ADD CONSTRAINT driver_info_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: driver_location driver_location_driver_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY driver_location
    ADD CONSTRAINT driver_location_driver_id_fkey FOREIGN KEY (driver_id) REFERENCES "user"(id);


--
-- Name: fcm_registration_id fcm_registration_id_session_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fcm_registration_id
    ADD CONSTRAINT fcm_registration_id_session_id_fkey FOREIGN KEY (session_id) REFERENCES session(id);


--
-- Name: food_addition_applied_tag food_addition_applied_tag_food_addition_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY food_addition_applied_tag
    ADD CONSTRAINT food_addition_applied_tag_food_addition_id_fkey FOREIGN KEY (food_addition_id) REFERENCES food_addition(id);


--
-- Name: food_addition_applied_tag food_addition_applied_tag_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY food_addition_applied_tag
    ADD CONSTRAINT food_addition_applied_tag_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES tag(id);


--
-- Name: food_addition food_addition_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY food_addition
    ADD CONSTRAINT food_addition_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- Name: food_addition_price food_addition_price_food_addition_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY food_addition_price
    ADD CONSTRAINT food_addition_price_food_addition_id_fkey FOREIGN KEY (food_addition_id) REFERENCES food_addition(id);


--
-- Name: gf_addition gf_addition_addition_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gf_addition
    ADD CONSTRAINT gf_addition_addition_id_fkey FOREIGN KEY (addition_id) REFERENCES food_addition(id);


--
-- Name: gf_category gf_category_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gf_category
    ADD CONSTRAINT gf_category_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES tag(id);


--
-- Name: gf_order gf_order_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gf_order
    ADD CONSTRAINT gf_order_order_id_fkey FOREIGN KEY (order_id) REFERENCES "order"(id);


--
-- Name: gf_product gf_product_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gf_product
    ADD CONSTRAINT gf_product_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: invoice invoice_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY invoice
    ADD CONSTRAINT invoice_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- Name: notification notification_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT notification_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: oauth_request oauth_request_session_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oauth_request
    ADD CONSTRAINT oauth_request_session_id_fkey FOREIGN KEY (session_id) REFERENCES session(id);


--
-- Name: order order_address_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_address_id_fkey FOREIGN KEY (address_id) REFERENCES address(id);


--
-- Name: order order_call_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_call_id_fkey FOREIGN KEY (call_id) REFERENCES call(id);


--
-- Name: order order_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES customer(id);


--
-- Name: order order_delivery_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_delivery_id_fkey FOREIGN KEY (delivery_id) REFERENCES delivery(id);


--
-- Name: order order_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- Name: order_product order_product_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_product
    ADD CONSTRAINT order_product_order_id_fkey FOREIGN KEY (order_id) REFERENCES "order"(id);


--
-- Name: order_product order_product_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_product
    ADD CONSTRAINT order_product_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: password_login password_login_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY password_login
    ADD CONSTRAINT password_login_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: password_reset password_reset_password_login_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY password_reset
    ADD CONSTRAINT password_reset_password_login_id_fkey FOREIGN KEY (password_login_id) REFERENCES password_login(id);


--
-- Name: product product_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- Name: product_price product_price_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_price
    ADD CONSTRAINT product_price_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_tag product_tag_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_tag
    ADD CONSTRAINT product_tag_product_id_fkey FOREIGN KEY (product_id) REFERENCES product(id);


--
-- Name: product_tag product_tag_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_tag
    ADD CONSTRAINT product_tag_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES tag(id);


--
-- Name: sent_notification sent_notification_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sent_notification
    ADD CONSTRAINT sent_notification_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: session session_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY session
    ADD CONSTRAINT session_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: supply_location supply_location_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY supply_location
    ADD CONSTRAINT supply_location_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- Name: tag tag_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- Name: tag tag_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES tag(id);


--
-- Name: user_info user_info_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_info
    ADD CONSTRAINT user_info_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: user user_organization_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES organization(id);


--
-- PostgreSQL database dump complete
--
