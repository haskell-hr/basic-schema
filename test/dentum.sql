CREATE TABLE client (
    id serial NOT NULL,
    name text NOT NULL,
    email text NOT NULL,
    infusion_id integer NOT NULL,
    creation_date timestamp NOT NULL,
    organization_id integer NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE offer (
    id serial NOT NULL,
    client_id integer NOT NULL,
    email_body text NOT NULL,
    email_subject text NOT NULL,
    language text NOT NULL,
    send_date timestamp,
    status text NOT NULL,
    created_date timestamp NOT NULL,
    alternative_hr boolean NOT NULL,
    last_update timestamp NOT NULL,
    yearly_id integer,  -- this is not really 1, but basic doesn't support nextval('yearly_id)
    currency text NOT NULL,
    revision integer NOT NULL,
    advance_adjustment_offset double precision,
    PRIMARY KEY (id)
);

ALTER TABLE offer
    ADD CONSTRAINT FK_offer__client_id FOREIGN KEY (client_id) REFERENCES client(id);

CREATE TABLE the_option (
    id serial NOT NULL,
    offer_id integer NOT NULL,
    discount double precision NOT NULL,
    accommodation integer NOT NULL,
    discount_card double precision NOT NULL,
    is_enabled boolean NOT NULL,
    voucher integer NOT NULL,
    special_discount double precision NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE the_option
    ADD CONSTRAINT FK_the_option__offer_id FOREIGN KEY (offer_id) REFERENCES offer(id);

CREATE TABLE section (
    id serial NOT NULL,
    the_option_id integer NOT NULL,
    name text NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE section
    ADD CONSTRAINT FK_section__the_option_id FOREIGN KEY (the_option_id) REFERENCES the_option(id);

CREATE TABLE service (
    id serial NOT NULL,
    name_hr text NOT NULL,
    name_en text NOT NULL,
    name_it text NOT NULL,
    name_de text NOT NULL,
    name_si text NOT NULL,
    price_gbp double precision NOT NULL,
    price_hrk double precision NOT NULL,
    price_eur double precision NOT NULL,
    organization_id integer NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE the_option_visit (
    the_option_id integer NOT NULL,
    visit integer NOT NULL,
    description text NOT NULL,
    PRIMARY KEY (the_option_id, visit)
);

ALTER TABLE the_option_visit
    ADD CONSTRAINT FK_the_option_visit__the_option_id FOREIGN KEY (the_option_id) REFERENCES the_option(id);


CREATE TABLE treatment (
    id serial NOT NULL,
    section_id integer NOT NULL,
    service_id integer NOT NULL,
    specification text NOT NULL,
    index_in_section integer NOT NULL,
    quantity integer NOT NULL,
    discount double precision NOT NULL,
    the_option_id integer NOT NULL,
    visit integer NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE treatment
    ADD CONSTRAINT FK_treatment__section_id FOREIGN KEY (section_id) REFERENCES section(id);
ALTER TABLE treatment
    ADD CONSTRAINT FK_treatment__service_id FOREIGN KEY (service_id) REFERENCES service(id);

CREATE TABLE package (
    id serial NOT NULL,
    name text NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE package_service (
    package_id integer NOT NULL,
    service_id integer NOT NULL,
    quantity integer NOT NULL,
    PRIMARY KEY (package_id, service_id)
);

ALTER TABLE package_service
    ADD CONSTRAINT FK_package_service__package_id FOREIGN KEY (package_id) REFERENCES package(id);
ALTER TABLE package_service
    ADD CONSTRAINT FK_package_service__service_id FOREIGN KEY (service_id) REFERENCES service(id);


CREATE TABLE dentum_user (
    id serial NOT NULL,
    name text NOT NULL,
    created timestamp NOT NULL,
    email text NOT NULL,
    password text NOT NULL,
    access_token text,
    organization_id integer NOT NULL,
    role text NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE session (
    id text NOT NULL,
    dentum_user_id integer NOT NULL,
    expires timestamp NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE session
    ADD CONSTRAINT FK_session__dentum_user_id FOREIGN KEY (dentum_user_id) REFERENCES dentum_user(id);

CREATE TABLE infusion_token (
    id serial NOT NULL,
    expires timestamp NOT NULL,
    access_token text NOT NULL,
    refresh_token text NOT NULL,
    dentum_user_id integer NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE infusion_token
    ADD CONSTRAINT FK_infusion_token__dentum_user_id FOREIGN KEY (dentum_user_id) REFERENCES dentum_user(id);

CREATE TABLE testimonial (
    id serial NOT NULL,
    language text NOT NULL,
    path text NOT NULL,
    upload_time timestamp NOT NULL,
    delete_time timestamp,
    approved_time timestamp NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE payment (
    id serial NOT NULL,
    client_id integer NOT NULL,
    offer_id integer NOT NULL,
    created timestamp NOT NULL,
    received timestamp NOT NULL,
    deleted timestamp,
    payment_type text NOT NULL,
    amount double precision NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE payment
    ADD CONSTRAINT FK_payment__client_id FOREIGN KEY (client_id) REFERENCES client(id);
ALTER TABLE payment
    ADD CONSTRAINT FK_payment__offer_id FOREIGN KEY (offer_id) REFERENCES offer(id);

CREATE TABLE offer_pdf (
    offer_id integer NOT NULL,
    created timestamp NOT NULL,
    revision integer NOT NULL,
    edit_number integer NOT NULL
);

ALTER TABLE offer_pdf
    ADD CONSTRAINT FK_offer_pdf__offer_id FOREIGN KEY (offer_id) REFERENCES offer(id);

CREATE TABLE client_visit (
    client_id integer NOT NULL,
    offer_id integer NOT NULL,
    visit_time timestamp NOT NULL
);

ALTER TABLE client_visit
    ADD CONSTRAINT fk_client_visit__client FOREIGN KEY (client_id) REFERENCES client(id);
ALTER TABLE client_visit
    ADD CONSTRAINT fk_client_visit__offer FOREIGN KEY (offer_id) REFERENCES offer(id);

CREATE TABLE organization (
    id serial,
    name text NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE client
    ADD CONSTRAINT fk_client__organization FOREIGN KEY (organization_id) REFERENCES organization(id);
ALTER TABLE service
    ADD CONSTRAINT fk_service__organization FOREIGN KEY (organization_id) REFERENCES organization(id);
ALTER TABLE dentum_user
    ADD CONSTRAINT fk_dentum_user__organization FOREIGN KEY (organization_id) REFERENCES organization(id);


CREATE TABLE applied_migrations (
    name text NOT NULL,
    applied_time timestamp NOT NULL -- DEFAULT now()
);

CREATE TABLE client_user (
    client_id integer NOT NULL,
    user_id integer NOT NULL,
    valid_from timestamp NOT NULL,
    valid_to timestamp
);

ALTER TABLE client_user
    ADD CONSTRAINT fk_client_user__client FOREIGN KEY (client_id) REFERENCES client(id);
ALTER TABLE client_user
    ADD CONSTRAINT fk_client_user__dentum_user FOREIGN KEY (user_id) REFERENCES dentum_user(id);

CREATE TABLE advance_change (
    id serial,
    change_type text not null,
    offer_id integer not null,
    amount double precision not null,
    change_time timestamp not null,
    primary key (id)
);

ALTER TABLE advance_change
    ADD CONSTRAINT fk_advance_change__offer FOREIGN KEY (offer_id) REFERENCES offer(id);

CREATE TABLE transport (
    id serial,
    name text not null,
    primary key (id)
);

CREATE TABLE accommodation (
    id serial,
    name text not null,
    address text not null,
    note text not null,
    code text not null,
    primary key (id)
);

CREATE TABLE "group" (
    id serial,
    note text not null,
    PRIMARY KEY (id)
);

CREATE TABLE client_trip (
    client_id integer not null,
    offer_id integer not null,
    arrival timestamp not null,
    departure timestamp not null,
    arrival_transport integer not null,
    departure_transport integer not null,
    accommodation integer not null,
    responsible_person integer not null,
    group_id integer not null,
    confirmed boolean not null,
    id serial,
    arrival_note text not null,
    departure_note text not null,
    last_update timestamp not null,
    PRIMARY KEY (id)
);

ALTER TABLE client_trip
    ADD CONSTRAINT fk_client_trip__client FOREIGN KEY (client_id) REFERENCES client(id);
ALTER TABLE client_trip
    ADD CONSTRAINT fk_client_trip__offer FOREIGN KEY (offer_id) REFERENCES offer(id);
ALTER TABLE client_trip
    ADD CONSTRAINT fk_client_trip__arrival_trans FOREIGN KEY (arrival_transport) REFERENCES transport(id);
ALTER TABLE client_trip
    ADD CONSTRAINT fk_client_trip__departure_trans FOREIGN KEY (departure_transport) REFERENCES transport(id);
ALTER TABLE client_trip
    ADD CONSTRAINT fk_client_trip__accommodation FOREIGN KEY (accommodation) REFERENCES accommodation(id);
ALTER TABLE client_trip
    ADD CONSTRAINT fk_client_trip__dentum_user FOREIGN KEY (responsible_person) REFERENCES dentum_user(id);
ALTER TABLE client_trip
    ADD CONSTRAINT fk_client_trip__group FOREIGN KEY (group_id) REFERENCES "group"(id);

CREATE TABLE activity_log (
    user_id integer not null,
    session_id text not null,
    activity_time timestamp not null,
    message text not null,
    log_data jsonb,
    id serial,
    committed boolean not null,
    primary key (id)
);


CREATE VIEW trip_summary AS
    select
        ct.*,
        du.name,
        ofr_vis_total.total_visits,
        ofr_vis_done.done_visits,
        cl.name,
        group_count.note,
        group_count.in_group
    from
        client_trip as ct,
        client as cl,
        (
            select
                offer.id as id,
                (
                    select
                        count(the_option_visit.*)
                    from
                        the_option_visit
                    where
                        the_option_visit.the_option_id = min(the_option.id)
                ) as total_visits
            from
                offer,
                the_option
            where
                the_option.offer_id = offer.id
            group by
                offer.id
        ) as ofr_vis_total,
        (
            select
                offer.id as id,
                count(client_visit.*) as done_visits
            from
                offer left join client_visit on client_visit.offer_id = offer.id
            group by
                offer.id
        ) as ofr_vis_done,
        dentum_user as du,
        (
            select
                gr.id as id,
                gr.note as note,
                count(ct2.*) as in_group,
                min(ct2.arrival) as earliest_arrival,
                max(ct2.departure) as latest_departure
            from
                "group" as gr,
                client_trip as ct2
            where
                ct2.group_id = gr.id
            group by
                gr.id
        ) as group_count
    where
        ct.client_id = cl.id and
        ofr_vis_total.id = ct.offer_id and
        ofr_vis_done.id = ct.offer_id and
        du.id = ct.responsible_person and
        group_count.id = ct.group_id;
